const async = require("async");

var config = require('../source/configure');
var { initDB } = require('../source/db.mongo');

var department_controller = require('../controllers/departmentController');
var budget_controller = require('../controllers/budgetController');
var economic_controller = require('../controllers/economicController');
var functional_controller = require('../controllers/functionalController');
var program_controller = require('../controllers/programController');
var bo_controller = require('../controllers/boController');

(async function () {
    initDB(config.database.mongo);
    var qparams = { finYear: 2019 };

    async.parallel({
        departments: function (callback) {
            department_controller.loadFromDB(qparams, callback);
        }, 
        functionals: function (callback) {
            functional_controller.loadFromDB(qparams, callback);
        }, 
        programs: function (callback) {
            program_controller.loadFromDB(qparams, callback);
        },
        economics: function (callback) {
            economic_controller.loadFromDB(qparams, callback);
        },
        budgets: function (callback) {
            budget_controller.loadFromDB(qparams, callback);
        },
        bos: function (callback) {
            bo_controller.loadFromDB(qparams, callback);
        }, 
    },
        // Optional callback
        function (err, results) {
            if (err) {
                console.log('FINAL ERR: ' + err);
            }
            else {
                console.log(results);
            }

            //mongoose.connection.close();
        });

}());