const async = require("async");

var config = require('../source/configure');
var { initDB } = require('../source/db.mongo');

var department_controller = require('../controllers/departmentController');
var budget_controller = require('../controllers/budgetController');
var economic_controller = require('../controllers/economicController');
var functional_controller = require('../controllers/functionalController');
var program_controller = require('../controllers/programController');
var bo_controller = require('../controllers/boController');
var contract_controller = require('../controllers/contractController');

(async function () {
    initDB(config.database.mongo);

    async.parallel({       
        departments: function (callback) {
            department_controller.loadFromFile(null, callback);
        },
        economics: function (callback) {
            economic_controller.loadFromFile(null, callback);
        },
        functionals: function (callback) {
            functional_controller.loadFromFile(null, callback);
        },
        programs: function (callback) {
            program_controller.loadFromFile(null, callback);
        },
        budgets: function (callback) {
            budget_controller.loadFromFile(null, callback);
        },
        bos: function (callback) {
            bo_controller.loadFromFile(null, callback);
        },
/*     
        contracts: function (callback) {
            contract_controller.loadFromFile(callback);
        },
*/
    },
        // Optional callback
        function (err, results) {
            if (err) {
                console.log('FINAL ERR: ' + err);
            }
            else {
                console.log(results);
            }

            //mongoose.connection.close();
        });

}());