var express = require('express'),
    router = express.Router();

var bo_controller = require('../controllers/boController'),
    budget_controller = require('../controllers/budgetController'),
    program_controller = require('../controllers/programController'),
    economic_controller = require('../controllers/economicController'),
    functional_controller = require('../controllers/functionalController'),
    department_controller = require('../controllers/departmentController');
    
// GET request for Department class.
router.get('/department_classifications', department_controller.api_department_import);
router.get('/program_classifications', program_controller.api_program_import);
router.get('/economic_classifications', economic_controller.api_economic_import);
router.get('/functional_classifications', functional_controller.api_functional_import);
router.get('/organizations', bo_controller.api_bo_import);
router.get('/budgets', budget_controller.api_budget_import);

module.exports = router;
