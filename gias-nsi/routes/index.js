var express = require('express');
var router = express.Router();

router.use('/api/import', require('./import'));

// GET home page.
router.get('/', function (req, res) {
    res.redirect('/');
});

module.exports = router;
