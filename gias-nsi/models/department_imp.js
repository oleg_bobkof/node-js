'use strict';

const mongoose = require('mongoose');
const { extendSchema } = require('../utils/common');
const DepartmentSchema = require('./department').schema;

var DepartmentImpSchema = extendSchema(DepartmentSchema, {});

// Index
DepartmentImpSchema
  .index({ finYear: 1, chp: 1 }, { unique: true })

// Statics methods
DepartmentImpSchema.statics
  .queryCreateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'departments',
          let: {
            chp_imp: '$chp',
            finYear_imp: '$finYear'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$chp', '$$chp_imp'] },
                    { $eq: ['$finYear', '$$finYear_imp'] }
                  ]
                }
              }
            },
            {
              $project: { department: 1 }
            }
          ],
          as: 'department'
        }
      },
      {
        $match: { 'department': { $eq: [] } }
      }
      ])
      .exec(callback)
  };

DepartmentImpSchema.statics
  .queryUpdateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'departments',
          let: {
            chp_imp: '$chp',
            finYear_imp: '$finYear',
            name_imp: '$name'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$chp', '$$chp_imp'] },
                    { $eq: ['$finYear', '$$finYear_imp'] },
                    { $ne: ['$name', '$$name_imp'] }
                  ]
                }
              }
            },
            { $project: { department: 1 } }
          ],
          as: 'department'
        }
      },
      { $match: { 'department': { $ne: [] } } }
      ])
      // .limit(10)
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('department_imp', DepartmentImpSchema);