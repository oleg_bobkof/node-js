'use strict';

const mongoose = require('mongoose');
// const MUUID = require('uuid-mongodb');

var Schema = mongoose.Schema;

//-- ведомственная классификация
var DepartmentSchema = new Schema(
  {
    //classId: { type: String, default: MUUID.v4 },
    finYear: { type: Number, required: true, min: 2000, max: 2100 },
    chp: { type: Number, required: true, min: 0 },
    name: { type: String, required: true, max: 255 },
    inputDateTime: { type: Date, default: Date.now },
    updStatus: { type: Number, required: true, min: 0, max: 3, default: 1 }, 
  }
);

// Index
DepartmentSchema
  .index({ finYear: 1, chp: 1 }, { unique: true })
  //.index({ finYear: 1, updStatus: 1 });

// Query Helpers
DepartmentSchema.query
  .byStatus = function (year, status) {
    return this.where({ finYear: year, updStatus: status }).select('_id');
  };

// Statics methods
DepartmentSchema.statics
  .queryDeleteItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'department_imps',
          let: {
            chp_imp: '$chp',
            finYear_imp: '$finYear'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$chp', '$$chp_imp'] },
                    { $eq: ['$finYear', '$$finYear_imp'] }
                  ]
                }
              }
            },
            {
              $project: { department: 1 }
            }
          ],
          as: 'department'
        }
      },
      {
        $match: { 'department': { $eq: [] } }
      }
      ])
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('Department', DepartmentSchema);