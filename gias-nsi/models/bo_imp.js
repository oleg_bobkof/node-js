'use strict';

const mongoose = require('mongoose');
const { extendSchema } = require('../utils/common');
const BOSchema = require('./bo').schema;

var BOImpSchema = extendSchema(BOSchema, {});

// Index
BOImpSchema
  .index({ finYear: 1, finId: 1, unk: 1, unp: 1, treasuryCode: 1 }, { unique: false })

// Statics methods
BOImpSchema.statics
  .queryCreateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'bos',
          let: {
            finYear_imp: '$finYear',
            finId_imp: '$finId',
            unk_imp: '$unk',
            unp_imp: '$unp',
            treasuryCode_imp: '$treasuryCode'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$finId_imp', '$finId'] },
                    { $eq: ['$$unk_imp', '$unk'] },
                    { $eq: ['$$unp_imp', '$unp'] },
                    { $eq: ['$$treasuryCode_imp', '$treasuryCode'] }
                  ]
                }
              }
            },
            {
              $project: { bo: 1 }
            }
          ],
          as: 'bo'
        }
      },
      {
        $match: { 'bo': { $eq: [] } }
      }
      ])
      .exec(callback)
  };

BOImpSchema.statics
  .queryUpdateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'bos',
          let: {
            finYear_imp: '$finYear',
            finId_imp: '$finId',
            unk_imp: '$unk',
            unp_imp: '$unp',
            treasuryCode_imp: '$treasuryCode',
            name_imp: '$name'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$finId_imp', '$finId'] },
                    { $eq: ['$$unk_imp', '$unk'] },
                    { $eq: ['$$unp_imp', '$unp'] },
                    { $eq: ['$$treasuryCode_imp', '$treasuryCode'] },
                    { $ne: ['$$name_imp', '$name'] }
                  ]
                }
              }
            },
            {
              $project: { bo: 1 }
            }
          ],
          as: 'bo'
        }
      },
      {
        $match: { 'bo': { $ne: [] } }
      }
      ])
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('bo_imp', BOImpSchema);