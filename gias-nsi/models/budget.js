'use strict';

const mongoose = require('mongoose');

var Schema = mongoose.Schema;

// справочник НСИ - Бюджеты
var BudgetSchema = new Schema({
    finYear: { type: Number, required: true, min: 2000, max: 2100 },
    finId: { type: Number, required: true, min: 0 },
    finIdParent: { type: Number, min: 0 },
    nameBudget: { type: String, max: 255 },
    inputDateTime: { type: Date, default: Date.now },
    updStatus: { type: Number, required: true, min: 0, max: 3, default: 1 }
});

// Index
BudgetSchema
  .index({ finYear: 1, finId: 1, finIdParent: 1 }, { unique: true })
  //.index({ finYear: 1, updStatus: 1 });

// Query Helpers
BudgetSchema.query
  .byStatus = function (year, status) {
    return this.where({ finYear: year, updStatus: status }).select('_id');
  };

// Statics methods
BudgetSchema.statics
  .queryDeleteItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'budget_imps',
          let: {
            finYear_imp: '$finYear',
            finId_imp: '$finId',
            finIdParent_imp: '$finIdParent'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$finId_imp', '$finId'] },
                    { $eq: ['$$finIdParent_imp', '$finIdParent'] }
                  ]
                }
              }
            },
            { $project: { budget: 1 } }
          ],
          as: 'budget'
        }
      },
      { $match: { 'budget': { $eq: [] } } }
      ])
      .exec(callback)
  };


// Export model.
module.exports = mongoose.model('Budget', BudgetSchema);