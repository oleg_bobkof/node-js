'use strict';

const mongoose = require('mongoose');
const { extendSchema } = require('../utils/common');
const BudgetSchema = require('./budget').schema;

var BudgetImpSchema = extendSchema(BudgetSchema, {});

// Index
BudgetImpSchema
  .index({ finYear: 1, finId: 1, finIdParent: 1 }, { unique: true })
  //.index({ finYear: 1, updStatus: 1 });

// Statics methods
BudgetImpSchema.statics
  .queryCreateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'budgets',
          let: {
            finYear_imp: '$finYear',
            finId_imp: '$finId',
            finIdParent_imp: '$finIdParent'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$finId_imp', '$finId'] },
                    { $eq: ['$$finIdParent_imp', '$finIdParent'] }
                  ]
                }
              }
            },
            { $project: { budget: 1 } }
          ],
          as: 'budget'
        }
      },
      { $match: { 'budget': { $eq: [] } } }
      ])
      .exec(callback)
  };

BudgetImpSchema.statics
  .queryUpdateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'budgets',
          let: {
            finYear_imp: '$finYear',
            finId_imp: '$finId',
            finIdParent_imp: '$finIdParent',
            nameBudget_imp: '$nameBudget'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$finId_imp', '$finId'] },
                    { $eq: ['$$finIdParent_imp', '$finIdParent'] },
                    { $ne: ['$$nameBudget_imp', '$nameBudget'] }
                  ]
                }
              }
            },
            { $project: { budget: 1 } }
          ],
          as: 'budget'
        }
      },
      { $match: { 'budget': { $ne: [] } } }
      ])
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('budget_imp', BudgetImpSchema);