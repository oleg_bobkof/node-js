'use strict';

const mongoose = require('mongoose');
const { extendSchema } = require('../utils/common');
const EconomicSchema = require('./economic').schema;

var EconomicImpSchema = extendSchema(EconomicSchema, {});

// Index
EconomicImpSchema
  .index({ finYear: 1, dCtg: 1, dArt: 1, dSArt: 1, dItm: 1 }, { unique: true })
//.index({ finYear: 1, updStatus: 1 });

// Statics methods
EconomicImpSchema.statics
  .queryCreateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'economics',
          let: {
            finYear_imp: '$finYear',
            dCtg_imp: '$dCtg',
            dArt_imp: '$dArt',
            dSArt_imp: '$dSArt',
            dItm_imp: '$dItm'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$dCtg_imp', '$dCtg'] },
                    { $eq: ['$$dArt_imp', '$dArt'] },
                    { $eq: ['$$dSArt_imp', '$dSArt'] },
                    { $eq: ['$$dItm_imp', '$dItm'] }
                  ]
                }
              }
            },
            { $project: { economic: 1 } }
          ],
          as: 'economic'
        }
      },
      { $match: { 'economic': { $eq: [] } } }
      ])
      .exec(callback)
  };

EconomicImpSchema.statics
  .queryUpdateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'economics',
          let: {
            finYear_imp: '$finYear',
            dCtg_imp: '$dCtg',
            dArt_imp: '$dArt',
            dSArt_imp: '$dSArt',
            dItm_imp: '$dItm',
            name_imp: '$name'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$dCtg_imp', '$dCtg'] },
                    { $eq: ['$$dArt_imp', '$dArt'] },
                    { $eq: ['$$dSArt_imp', '$dSArt'] },
                    { $eq: ['$$dItm_imp', '$dItm'] },
                    { $ne: ['$$name_imp', '$name'] }
                  ]
                }
              }
            },
            { $project: { economic: 1 } }
          ],
          as: 'economic'
        }
      },
      { $match: { 'economic': { $ne: [] } } }
      ])
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('economic_imp', EconomicImpSchema);