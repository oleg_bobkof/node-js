'use strict';

const mongoose = require('mongoose');

var Schema = mongoose.Schema;

const arrDic = [
    'functionalClassification',
    'economicClassification',
    'programClassification',
    'departmentClassification',
    'budget',
    'bo'
];

// NSI notifications
var NSINotificationSchema = new Schema({
    dictionary: { type: String, required: true, enum: arrDic},
    operation: { type: String, required: true, enum: ['CREATE', 'UPDATE', 'DELETE']},
    objectIDs: [Schema.Types.ObjectId],
    operationDate: { type: Date, default: Date.now }
});

// Export model.
module.exports = mongoose.model('Notification', NSINotificationSchema);