'use strict';

const mongoose = require('mongoose');

var Schema = mongoose.Schema;

// справочник БО
var BOSchema = new Schema({
    finYear: { type: Number, required: true, min: 2000, max: 2100 },
    finId: { type: Number, required: true, min: 10000 },
    unp: { type: String, required: true, minLength: 9, maxLength: 9 },
    unk: { type: Number, required: true, min: 0 },
    treasuryCode: { type: Number, required: true, min: -1 },
    name: { type: String, required: true, max: 255 },
    inputDateTime: { type: Date, default: Date.now },
    updStatus: { type: Number, required: true, min: 0, max: 3, default: 1 },
});

// Index
BOSchema
    .index({ finYear: 1, finId: 1, unk: 1, unp: 1, treasuryCode: 1 }, { unique: false })
    //.index({ finYear: 1, updStatus: 1 });

// Query Helpers
BOSchema.query
    .byStatus = function (year, status) {
        return this.where({ finYear: year, updStatus: status }).select('_id').limit(100);
    };

// Statics methods
BOSchema.statics
  .queryDeleteItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'bo_imps',
          let: {
            finYear_imp: '$finYear',
            finId_imp: '$finId',
            unk_imp: '$unk',
            unp_imp: '$unp',
            treasuryCode_imp: '$treasuryCode'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$finId_imp', '$finId'] },
                    { $eq: ['$$unk_imp', '$unk'] },
                    { $eq: ['$$unp_imp', '$unp'] },
                    { $eq: ['$$treasuryCode_imp', '$treasuryCode'] }
                  ]
                }
              }
            },
            {
              $project: { bo: 1 }
            }
          ],
          as: 'bo'
        }
      },
      {
        $match: { 'bo': { $eq: [] } }
      }
      ])
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('BO', BOSchema);