'use strict';

const mongoose = require('mongoose');

var Schema = mongoose.Schema;

// экономическая классификация
var EconomicSchema = new Schema({
  finYear: { type: Number, required: true, min: 2000, max: 2100 },
  dCtg: { type: Number, required: true, min: 0 },
  dArt: { type: Number, required: true, min: 0 },
  dSArt: { type: Number, required: true, min: 0 },
  dItm: { type: Number, required: true, min: 0 },
  name: { type: String, max: 255 },
  inputDateTime: { type: Date, default: Date.now },
  updStatus: { type: Number, required: true, min: 0, max: 3, default: 1 }
});

// Index
EconomicSchema
  .index({ finYear: 1, dCtg: 1, dArt: 1, dSArt: 1, dItm: 1 }, { unique: true })
  //.index({ finYear: 1, updStatus: 1 });

// Query Helpers
EconomicSchema.query
  .byStatus = function (year, status) {
    return this.where({ finYear: year, updStatus: status }).select('_id');
  };

// Statics methods
EconomicSchema.statics
  .queryDeleteItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'economic_imps',
          let: {
            finYear_imp: '$finYear',
            dCtg_imp: '$dCtg',
            dArt_imp: '$dArt',
            dSArt_imp: '$dSArt',
            dItm_imp: '$dItm'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$dCtg_imp', '$dCtg'] },
                    { $eq: ['$$dArt_imp', '$dArt'] },
                    { $eq: ['$$dSArt_imp', '$dSArt'] },
                    { $eq: ['$$dItm_imp', '$dItm'] }
                  ]
                }
              }
            },
            {
              $project: { economic: 1 }
            }
          ],
          as: 'economic'
        }
      },
      {
        $match: { 'economic': { $eq: [] } }
      }
      ])
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('Economic', EconomicSchema);