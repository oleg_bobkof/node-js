'use strict';

const mongoose = require('mongoose');

var Schema = mongoose.Schema;

// функциональная классификация
var FunctionalSchema = new Schema({
  finYear: { type: Number, required: true, min: 2000, max: 2100 },
  dSct: { type: Number, required: true, min: 0 },
  dSSct: { type: Number, required: true, min: 0 },
  dKnd: { type: Number, required: true, min: 0 },
  dPrgr: { type: Number, required: true, min: 0 },
  name: { type: String, required: true, max: 255 },
  inputDateTime: { type: Date, default: Date.now },
  updStatus: { type: Number, required: true, min: 0, max: 3, default: 1 }
});

// Index
FunctionalSchema
  .index({ finYear: 1, dSct: 1, dSSct: 1, dKnd: 1, dPrgr: 1 }, { unique: true })
  //.index({ finYear: 1, updStatus: 1 });

// Query Helpers
FunctionalSchema.query
  .byStatus = function (year, status) {
    return this.where({ finYear: year, updStatus: status }).select('_id');
  };

// Statics methods
FunctionalSchema.statics
  .queryDeleteItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'functional_imps',
          let: {
            finYear_imp: '$finYear', 
            dSct_imp: '$dSct',
            dSSct_imp: '$dSSct',
            dKnd_imp: '$dKnd',
            dPrgr_imp: '$dPrgr'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$dSct_imp', '$dSct'] },
                    { $eq: ['$$dSSct_imp', '$dSSct'] },
                    { $eq: ['$$dKnd_imp', '$dKnd'] },
                    { $eq: ['$$dPrgr_imp', '$dPrgr'] }
                  ]
                }
              }
            },
            { $project: { functional: 1 } }
          ],
          as: 'functional'
        }
      },
      { $match: { 'functional': { $eq: [] } } }
      ])
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('Functional', FunctionalSchema);