'use strict';

const mongoose = require('mongoose');
const { extendSchema } = require('../utils/common');
const FunctionalSchema = require('./functional').schema;

var FunctionalImpSchema = extendSchema(FunctionalSchema, {});

// Index
FunctionalImpSchema
  .index({ finYear: 1, dSct: 1, dSSct: 1, dKnd: 1, dPrgr: 1 }, { unique: true })

// Statics methods
FunctionalImpSchema.statics
  .queryCreateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'functionals',
          let: {
            finYear_imp: '$finYear',
            dSct_imp: '$dSct',
            dSSct_imp: '$dSSct',
            dKnd_imp: '$dKnd',
            dPrgr_imp: '$dPrgr'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$dSct_imp', '$dSct'] },
                    { $eq: ['$$dSSct_imp', '$dSSct'] },
                    { $eq: ['$$dKnd_imp', '$dKnd'] },
                    { $eq: ['$$dPrgr_imp', '$dPrgr'] }
                  ]
                }
              }
            },
            { $project: { functional: 1 } }
          ],
          as: 'functional'
        }
      },
      { $match: { 'functional': { $eq: [] } } }
      ])
      .exec(callback)
  };

FunctionalImpSchema.statics
  .queryUpdateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'functionals',
          let: {
            finYear_imp: '$finYear',
            dSct_imp: '$dSct',
            dSSct_imp: '$dSSct',
            dKnd_imp: '$dKnd',
            dPrgr_imp: '$dPrgr',
            name_imp: '$name'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$dSct_imp', '$dSct'] },
                    { $eq: ['$$dSSct_imp', '$dSSct'] },
                    { $eq: ['$$dKnd_imp', '$dKnd'] },
                    { $eq: ['$$dPrgr_imp', '$dPrgr'] },
                    { $ne: ['$$name_imp', '$name'] }
                  ]
                }
              }
            },
            { $project: { functional: 1 } }
          ],
          as: 'functional'
        }
      },
      { $match: { 'functional': { $ne: [] } } }
      ])
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('functional_imp', FunctionalImpSchema);