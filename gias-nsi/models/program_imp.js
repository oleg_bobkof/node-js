'use strict';

const mongoose = require('mongoose');
const { extendSchema } = require('../utils/common');
const ProgramSchema = require('./program').schema;

var ProgramImpSchema = extendSchema(ProgramSchema, {});

// Unique index
ProgramImpSchema
  .index({ finYear: 1, dPro: 1, dSPro: 1 }, { unique: true })
//.index({ finYear: 1, updStatus: 1 });

// Query Helpers
ProgramImpSchema.query
  .byStatus = function (year, status) {
    return this.where({ finYear: year, updStatus: status }).select('_id');
  };

// Statics methods
ProgramImpSchema.statics
  .queryCreateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'programs',
          let: {
            finYear_imp: '$finYear',
            dPro_imp: '$dPro',
            dSPro_imp: '$dSPro'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$dPro_imp', '$dPro'] },
                    { $eq: ['$$dSPro_imp', '$dSPro'] }
                  ]
                }
              }
            },
            { $project: { program: 1 } }
          ],
          as: 'program'
        }
      },
      { $match: { 'program': { $eq: [] } } }
      ])
      .exec(callback)
  };

ProgramImpSchema.statics
  .queryUpdateItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'programs',
          let: {
            finYear_imp: '$finYear',
            dPro_imp: '$dPro',
            dSPro_imp: '$dSPro',
            name_imp: '$name'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$dPro_imp', '$dPro'] },
                    { $eq: ['$$dSPro_imp', '$dSPro'] },
                    { $ne: ['$$name_imp', '$name'] }
                  ]
                }
              }
            },
            { $project: { program: 1 } }
          ],
          as: 'program'
        }
      },
      { $match: { 'program': { $ne: [] } } }
      ])
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('program_imp', ProgramImpSchema);