'use strict';

const mongoose = require('mongoose');

var Schema = mongoose.Schema;

// программная классификация
var ProgramSchema = new Schema({
  finYear: { type: Number, required: true, min: 2000, max: 2100 },
  dPro: { type: Number, required: true, min: 0 },
  dSPro: { type: Number, required: true, min: 0 },
  name: { type: String, max: 255 },
  inputDateTime: { type: Date, default: Date.now },
  updStatus: { type: Number, required: true, min: 0, max: 3, default: 1 }
},
  { toJSON: { virtuals: false } }
);

// Unique index
ProgramSchema
  .index({ finYear: 1, dPro: 1, dSPro: 1 }, { unique: true })
//.index({ finYear: 1, updStatus: 1 });

// Query Helpers
ProgramSchema.query
  .byStatus = function (year, status) {
    return this.where({ finYear: year, updStatus: status }).select('_id');
  };

// Statics methods
ProgramSchema.statics
  .queryDeleteItems = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'program_imps',
          let: {
            finYear_imp: '$finYear',
            dPro_imp: '$dPro',
            dSPro_imp: '$dSPro'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$$finYear_imp', '$finYear'] },
                    { $eq: ['$$dPro_imp', '$dPro'] },
                    { $eq: ['$$dSPro_imp', '$dSPro'] }
                  ]
                }
              }
            },
            { $project: { program: 1 } }
          ],
          as: 'program'
        }
      },
      { $match: { 'program': { $eq: [] } } }
      ])
      .exec(callback)
  };

// Export model.
module.exports = mongoose.model('Program', ProgramSchema);