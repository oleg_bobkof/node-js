'use strict';

const mongoose = require('mongoose');

const CRUD = new Map([  // 'Признак изменения: 0-нет, 1-новая, 2-изменена, 3-удалена'
    ['READ', 0],
    ['CREATE', 1],
    ['UPDATE', 2],
    ['DELETE', 3],
]);

const models = [
    { modelname: 'Department', dictionary: 'departmentClassification' },
    { modelname: 'Functional', dictionary: 'functionalClassification' },
    { modelname: 'Economic', dictionary: 'economicClassification' },
    { modelname: 'Program', dictionary: 'programClassification' },
    { modelname: 'Budget', dictionary: 'budget' },
    { modelname: 'BO', dictionary: 'bo' },
];

function extendSchema(Schema, definition, options) {
    return new mongoose.Schema(
        Object.assign({}, Schema.obj, definition),
        options
    );
};

function getDictionary(model_name) {
    var item = models.find(obj => obj.modelname === model_name);
    return item.dictionary;
}

module.exports = {
    CRUD,
    models,
    extendSchema,
    getDictionary
};


/**
 * from - The target collection.
 * localField - The local join field.
 * foreignField - The target join field.
 * as - The name for the results.
 */
/*
{
    from: 'departments',
    let: { 
      chp_imp: '$chp', 
      finYear_imp: '$finYear',  
      name_imp: '$name'
    },
    pipeline: [{
      $match: {
        $expr: {
          $and: [
            { $eq: ['$chp', '$$chp_imp'] },
            { $eq: ['$finYear', '$$finYear_imp'] }
          ]
        }
      }},
      {$project: 
        { updStatus: {
              $cond: {
                 if: { $eq: ['$name', '$$name_imp'] },
                 then: 0,
                 else: 2
              }
           }
          
        }
      }
    ],
    as: 'department',
    
  }
  */

  /*
  // Statics methods
DepartmentImpSchema.statics
  .joinData = function (callback) {
    return this
      .aggregate([{
        $lookup: {
          from: 'departments',
          let: {
            chp_imp: '$chp',
            finYear_imp: '$finYear',
            name_imp: '$name'
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$chp', '$$chp_imp'] },
                    { $eq: ['$finYear', '$$finYear_imp'] }
                  ]
                }
              }
            },
            {
              $project:
              {
                chp: 1,
                updStatus: {
                  $cond: {
                    if: { $eq: ['$name', '$$name_imp'] },
                    then: 0,
                    else: 2
                  }
                }
              }
            }
          ],
          as: 'department'
        }
      },
      {
        $match: {
          'department': { $eq: [] } 
          // 'department': { $eq: [{chp: 17}] }
          // 'department' 
          // $expr: { 
          //   $or: [
          //     {$eq: [{ $size: "$department"}, 0] },
              
          //   ]
          // }
        }
      }
      ])
      .limit(10)
    //.exec(callback)
  }
*/