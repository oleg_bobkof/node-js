var express = require('express'),
    cookieParser = require('cookie-parser'),
    logger = require('morgan'),
    compression = require('compression'),
    helmet = require('helmet');

var { errorHandler, createHttpErr } = require('./utils/errors');
var config = require('./source/configure');

var app = express();

var debug = config.webserver.debug;
app.disable('etag');
app.use(logger(debug));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(helmet());
// Compress all routes
app.use(compression());

//app.use(express.static(path.join(__dirname, 'uploads')));
app.use(require('./routes'));

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
    var error = new createHttpErr.NotFound('The requested URL [' + [req.path] + '] was not found');
    next(error);
});

// Error handler
app.use(errorHandler);

module.exports = app;
