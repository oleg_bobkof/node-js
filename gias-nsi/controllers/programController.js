const path = require('path'),
  fs = require('fs'),
  async = require("async"),
  mssql = require('../source/db.mssql'),
  { CRUD, getDictionary } = require('../utils/common');

var Program = require('../models/program');
var ProgramImp = require('../models/program_imp');
var notification_controller = require('./notificationController');
var { createHttpErr, createErr400 } = require('../utils/errors');

// Add notification event
function addNotification(operation, items, cb) {

  let dictionary = getDictionary(Program.modelName);
  let ids = items.map(function (item) { return item._id });
  notification_controller.addNotification(dictionary, operation, ids, cb);

}

// Process programs
function processData(cb) {

  async.parallel([
    function (callback) {
      processCreate(callback);
    },
    function (callback) {
      processUpdate(callback);
    },
    function (callback) {
      processDelete(callback);
    },
  ], function (err, results) {
    if (err) {
      console.log(err.message);
      return cb(err, null);
    }
    cb(err, results);
  });

}

// --
async function processCreate(cb) {

  async.waterfall([
    function (callback) {
      ProgramImp.queryCreateItems(callback);
    },
    function (items, callback) {
      Program.insertMany(items, function (err, ins_items) {
        if (err) { callback(err); } else {
          callback(err, items, ins_items);
        }
      });
    },
    function (items, ins_items, callback) {
      addNotification('CREATE', ins_items, function (err, notification) {
        if (err) { callback(err); } else {
          callback(err, items, ins_items, notification);
        }
      });
    },
  ],
    function (err, items, ins_items, notification) {
      if (err) {
        cb(err, null);
      } else {
        // console.log(notification);
        // cb(err, `CREATE [${ins_items.length}]`);
        cb(err, notification);
      }
    }
  );

}

// -- 
async function processUpdate(cb) {

  async.waterfall([
    function (callback) {
      ProgramImp.queryUpdateItems(callback);
    },
    function (items, callback) {
      async.map(
        items,
        function (item, updCallback) {
          let [{ _id }] = item.program;
          Program.findById(_id, function (err, doc) {
            if (err) { updCallback(err); } else {
              doc.updStatus = CRUD.get('UPDATE');
              doc.name = item.name;
              doc.inputDateTime = item.inputDateTime;
              doc.save(updCallback);
            }
          });
        },
        function (err, upd_items) {
          if (err) { callback(err); } else {
            callback(err, items, upd_items);
          }
        }
      );
    },
    function (items, upd_items, callback) {
      addNotification('UPDATE', upd_items, function (err, notification) {
        if (err) { callback(err); } else {
          callback(err, items, upd_items, notification);
        }
      });
    }
  ],
    function (err, items, upd_items, notification) {
      if (err) {
        cb(err, null);
      } else {
        // console.log(notification);
        // cb(err, `UPDATE [${upd_items.length}]`);
        cb(err, notification);
      }
    }
  );

}

// --
async function processDelete(cb) {

  async.waterfall([
    function (callback) {
      Program.queryDeleteItems(callback);
    },
    function (items, callback) {
      async.map(
        items,
        function (item, delCallback) {
          let { _id } = item;
          Program.findByIdAndDelete(_id, delCallback);
        },
        function (err, del_items) {
          if (err) { callback(err); } else {
            callback(err, items, del_items);
          }
        }
      );
    },
    function (items, del_items, callback) {
      addNotification('DELETE', del_items, function (err, notification) {
        if (err) { callback(err); } else {
          callback(err, items, del_items, notification);
        }
      });
    },
  ],
    function (err, items, del_items, notification) {
      if (err) {
        console.log(err.message);
        cb(err, null);
      } else {
        // console.log(notification);
        // cb(err, `DELETE [${del_items.length}]`);
        cb(err, notification);
      }
    }
  );

}

//
function loadData(qparams, data, cb) {

  async.series({
    data_del: function (callback) {
      ProgramImp.deleteMany(qparams).exec(callback);
    },
    data_add: function (callback) {
      ProgramImp.insertMany(data, function (err, doc) {
        if (err) return callback(err);
        callback(err, { n: data.length, ok: 1, insertedCount: doc.length });
      });
    },
    notifications: function (callback) {
      processData(callback);
    }
  }, function (err, results) {
    if (err) {
      console.log(err.message);
      return cb(err, null);
    }
    cb(null, results);
  });

}

//
async function loadFromFile(qparams, cb) {

  let data;
  try {
    let file = fs.readFileSync(path.join(__dirname, '../data/') + 'programs.json', 'utf-8');
    data = JSON.parse(file);
  } catch (err) {
    cb(err, null);
    return;
  }

  if (!data.length) {
    return cb(null, data);
  }

  loadData(qparams, data, cb);

}

//
async function loadFromDB(qparams, cb) {

  mssql.bookPrograms(qparams, function (err, data) {
    if (err) { return cb(err, null); }
    if (data.length) {
      loadData(qparams, data, cb);
    } else {
      return cb(null, data);
    }
  });

}

//
function api_program_import(req, res, next) {

  let { year = new Date().getFullYear() } = req.query;
  let qparams = { finYear: year };

  async.waterfall([
    function (callback) {
      //loadFromFile(qparams, callback)
      loadFromDB(qparams, callback);
    },
    function (data, callback) {
      let { notifications = [] } = data;
      notification_controller.sendNotifications(notifications, function (err, result) {
        if (err) { callback(err); } else {
          callback(err, data, result);
        }
      });
    },
  ],
    function (err, data, result) {
      if (err) {
        return next(createErr400(-107, err));
      } else {
        console.log(data);
        console.log(result);
        res.status(200).json(result);
      }
    }
  );

}

module.exports = { api_program_import, loadFromDB, loadFromFile };