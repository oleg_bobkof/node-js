
const axios = require("axios");
const async = require("async");

var config = require('../source/configure');
var Notification = require('../models/notification');

// Create notification event
async function addNotification(dictionary, operation, ids, cb) {

  if (ids && ids.length) {

    var arr_ids = [];
    for (let i = 0; i < ids.length; i = i + 100) {
      arr_ids.push(ids.slice(i, i + 100));
    }

    async.mapSeries(
      arr_ids,
      function (items, callback) {
        let notification = new Notification({
          dictionary: dictionary,
          operation: operation,
          objectIDs: items
        });
        notification.save(function (err, notification) {
          if (err) { return callback(err); }
          callback(err, notification);
        });
      },
      function (err, notifications) {
        if (err) { cb(err, null); } else {
          cb(null, notifications);
        }
      }
    );

  } else {
    cb();
  }

}

// Make http request
function makeRequest(notification, cb) {
  let options = config.gias_scheduler.job;

  let body = {
    dictionary: notification.dictionary,
    operation: notification.operation,
    objectIDs: notification.objectIDs
  };

  options.data.data = {
    headers: "",
    params: "",
    query: "",
    body: body
  };

  axios(options)
    .then(function (response) {
      cb(null, {
        job: options.data.name,
        dictionary: notification.dictionary,
        operation: notification.operation,
        messsage: response.data
      });
    })
    .catch(function (error) {
      if (error.response) {
        cb(error.response);
      } else if (error.request) {
        cb(error.message);
      } else {
        cb(error);
      }
    });

}

// Send NSI notification
async function sendNotifications(notifications, cb) {

  async.mapSeries(
    [0, 1, 2],
    function (i, callback) {
      if (notifications[i]) {
        async.mapSeries(
          notifications[i],
          function (item, ncallback) {
            makeRequest(item, ncallback);
          },
          function (err, res) {
            if (err) { return callback(err); } else {
              callback(null, res);
            }
          }
        );
      } else { callback(); }
    },
    function (err, res) {
      if (err) { return cb(err); } else {
        cb(null, res);
      }
    }
  );

}

module.exports = { addNotification, sendNotifications, makeRequest }