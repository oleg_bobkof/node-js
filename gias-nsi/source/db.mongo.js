const mongoose = require('mongoose');
const db = mongoose.connection;

// set up mongoose connection
function initDB(dbConfig) {
    var db_url = dbConfig.uri;
    var { options } = dbConfig;

    mongoose.connect(db_url, options)
        .then(
            () => {
    mongoose.connect(db_url, options)
                console.log('MongoDB is connected: ' + db_url);
                mongoose.Promise = global.Promise;
            },
            err => {
                console.error('App starting error:', err.stack);
                process.exit(1);
            }
        );

};

module.exports = {
    initDB, db
};
