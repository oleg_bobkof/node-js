const sql = require("mssql");
const config = require('../source/configure');
var connectErr;

//Initiallising connection string
const dbConfig = config.database.mssql;
const poolPromise = new sql.ConnectionPool(dbConfig).connect().catch(err => {
  connectErr = err;
  console.log(err.message);
});

async function messageHandler(cb) {

  try {
    if (!await poolPromise) { throw (connectErr); }
  } catch (err) {
    return cb(err);
  }

  poolPromise
    .then((pool) => {
      return pool.query('select 1 as OK');
    }).then(result => {
      console.dir(result);
    }).catch(err => {
      return cb(err);
    });
}


// Book Departments
async function bookDepartments(params, cb) {
  var { finYear = 2019 } = params;
  const query = 'select YearF as finYear, chp, [name] from dbo.tf_book_Ved(@finYear)';

  try {
    if (!await poolPromise) { throw (connectErr); }
  } catch (err) {
    return cb(err);
  }

  poolPromise
    .then((pool) => {
      return pool.request()
        .input('finYear', sql.Int, finYear)
        .query(query);
    }).then(result => {
      cb(null, result.recordset);
    }).catch(err => {
      return cb(err);
    });

}

// Book Budgets
async function bookBudgets(params, cb) {
  let { finYear = 2019 } = params;
  const query = 'select YearF as finYear, finId, Parent as finIdParent, Name as nameBudget FROM dbo.tf_book_Findept(@finYear)';

  try {
    if (!await poolPromise) { throw (connectErr); }
  } catch (err) {
    return cb(err);
  }

  poolPromise
    .then((pool) => {
      return pool.request()
        .input('finYear', sql.Int, finYear)
        .query(query);
    }).then(result => {
      cb(null, result.recordset);
    }).catch(err => {
      return cb(err);
    });
}

// Book Economics
async function bookEconomics(params, cb) {
  let { finYear = 2019 } = params;
  const query = 'select YearF as finYear, dCtg, dArt, dSArt, dItm, [name] from dbo.tf_book_DEcon(@finYear)';

  try {
    if (!await poolPromise) { throw (connectErr); }
  } catch (err) {
    return cb(err);
  }

  poolPromise
    .then((pool) => {
      return pool.request()
        .input('finYear', sql.Int, finYear)
        .query(query);
    }).then(result => {
      cb(null, result.recordset);
    }).catch(err => {
      return cb(err);
    });
}

// Book Functionals
async function bookFunctionals(params, cb) {
  let { finYear = 2019 } = params;
  const query = 'select YearF as finYear, dSct, dSSct, dKnd, dPrgr, [name] from dbo.tf_book_DFunc(@finYear)';

  try {
    if (!await poolPromise) { throw (connectErr); }
  } catch (err) {
    return cb(err);
  }

  poolPromise
    .then((pool) => {
      return pool.request()
        .input('finYear', sql.Int, finYear)
        .query(query);
    }).then(result => {
      cb(null, result.recordset);
    }).catch(err => {
      return cb(err);
    });
}

// Book Programs
async function bookPrograms(params, cb) {
  let { finYear = 2019 } = params;
  const query = 'select YearF as finYear, dPro, dSPro, [name] from dbo.tf_book_DPro(@finYear)';

  try {
    if (!await poolPromise) { throw (connectErr); }
  } catch (err) {
    return cb(err);
  }

  poolPromise
    .then((pool) => {
      return pool.request()
        .input('finYear', sql.Int, finYear)
        .query(query);
    }).then(result => {
      cb(null, result.recordset);
    }).catch(err => {
      return cb(err);
    });
}

// Book BOs
async function bookBOs(params, cb) {
  let { finYear = 2019 } = params;
  const query = 'select YearF as finYear, finId, UNN as unp, Organization as unk, treasuryCode, name from dbo.tf_book_BO(@finYear)';

  try {
    if (!await poolPromise) { throw (connectErr); }
  } catch (err) {
    return cb(err);
  }

  poolPromise
    .then((pool) => {
      return pool.request()
        .input('finYear', sql.Int, finYear)
        .query(query);
    }).then(result => {
      cb(null, result.recordset);
    }).catch(err => {
      return cb(err);
    });
}

module.exports = {
  sql, poolPromise,
  messageHandler,
  bookDepartments,
  bookBudgets,
  bookEconomics,
  bookFunctionals,
  bookPrograms,
  bookBOs
};