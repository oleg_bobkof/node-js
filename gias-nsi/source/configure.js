const path = require('path')
const yaml_config = require('node-yaml-config');
// const validateSchema = require('yaml-schema-validator')

var userArgs = process.argv.slice(2);
const configFile = userArgs[0] || 'config.yaml';
const config = {};

// -- configuration sections
config.webserver = {};
// -- databases
config.database = {};
config.database.mongo = {};
config.database.mongo.uri = process.env.MONGODB_URI;
config.database.mssql = {};
// -- scheduler
config.scheduler = {};

// -- configuration
(function init() {
    try {
        var filename = path.join(__dirname, '../') + configFile;
        //var filename2 = path.join(__dirname, './config.schema.yaml');
        var appConfig = yaml_config.load(filename);
        var { webserver, gias_scheduler, database: { mongo, mssql } } = appConfig;
        console.log('Read config: ' + filename);

        if (webserver) {
            config.webserver = webserver;
        };

        if (mongo) {
            config.database.mongo = mongo;
        };

        if (mssql) {
            config.database.mssql = mssql;
        };

        if (gias_scheduler) {
            config.gias_scheduler = gias_scheduler.requests;
        };

        // validate a json OR yml file
        // validateSchema(filename, { schemaPath: filename2 })
       
    } catch (err) {
        console.error(err);
        process.exit(0);
    }
}());

module.exports = config;