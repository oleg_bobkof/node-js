'use strict';

const
  express = require('express'),
  router = express.Router(),
  gias_controller = require('../controllers/giasController');

/* GET users listing. */
router.all('/*', gias_controller.api_gias_get);

module.exports = router;