const express = require('express');
const router = express.Router();
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const path = require('path');

const swaggerDocument = YAML.load(path.join(__dirname, '../api/') + 'single.yaml');

const options = {
  explorer: true,
  customCss: '.swagger-ui .topbar { display: none }'
};

/* GET swagger UI */
router.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));
// router.use('/api-docs', swaggerUi.serve);
// router.get('/api-docs', swaggerUi.setup(swaggerDocument, options));

module.exports = router;