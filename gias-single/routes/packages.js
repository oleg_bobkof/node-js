'use strict';

const 
  express = require('express'),
  package_controller = require('../controllers/packageController'),
  router = express.Router();

// GET users listing.
router.get('/', function (req, res, next) {
  res.send('Welcome to ASFR REST API!');
});

// POST request for load package.
router.post('/package', package_controller.api_package_post);

// GET request package.
router.get('/package/:id/resend', package_controller.api_package_get);

module.exports = router;
