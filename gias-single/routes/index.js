const 
  express = require('express'),
  router = express.Router();

// Routes
router.use('/api', require('./packages'));
router.use('/api-docs', require('./api-docs'));
router.use('/api-gias', require('./gias'));

// GET home page
router.get('/', function (req, res, next) {
  res.redirect('/api-docs');
});

// GET ping -> pong
router.get('/ping', function (req, res) {
  res.status(200).json({ msg: 'pong' });
});


module.exports = router;
