const mongoose = require('mongoose');
const { extendSchema } = require('../utils/common');
const PackageINSchema = require('./packageIN').schema;
const PackageINExtSchema = extendSchema(PackageINSchema, {});

// Export model.
module.exports = mongoose.model('package_in', PackageINExtSchema);