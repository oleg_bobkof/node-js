'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Output package
const PackageOUTSchema = new Schema({
  package_output: {
    payload: { type: String },
    sign: { type: String },
    sign_algorithm: { type: String }
  },
  timestamp: { type: Date, default: Date.now },
},
  { toJSON: { virtuals: false } }
);

// Virtual for this program instance URL.
PackageOUTSchema
  .virtual('load_date')
  .get(function () {
    return this._id.getTimestamp();
  });

// Export model.
module.exports = mongoose.model('PackageOUT', PackageOUTSchema);