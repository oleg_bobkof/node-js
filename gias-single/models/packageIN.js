'use strict';

const mongoose = require('mongoose');
const MUUID = require('uuid-mongodb');

const Schema = mongoose.Schema;
const arrPackMethod = ['READ', 'CREATE', 'UPDATE', 'DELETE', 'INFO'];

// Header
const Header = new Schema({
  package_id: { type: String, default: MUUID.v4 },
  abonent_id: { type: String },
  service_path: { type: String },
  service_method: { type: String, required: true, enum: arrPackMethod }
}, 
  { _id: false }
);

// Package Input
const PackageInput = new Schema({
  path: { type: String },
  query_string: { type: String },
  method: { type: String, required: true, enum: arrPackMethod },
  payload: { type: String },
  sign: { type: String },
  sign_algorithm: { type: String },
  abonent: { type: String, required: true }
}, 
  { _id: false }
);

// Input package
const PackageINSchema = new Schema({
  header: Header,
  package_input: PackageInput, 
  request: {},
  http_status: { type: Number },
  log: {},
  error_data: {},
  timestamp: { type: Date, default: Date.now }
},
  { toJSON: { virtuals: true } }
);

// Virtual for this program instance URL.
PackageINSchema
  .virtual('load_date')
  .get(function () {
    return this._id.getTimestamp();
  });

// Export model.
module.exports = mongoose.model('package_out', PackageINSchema);