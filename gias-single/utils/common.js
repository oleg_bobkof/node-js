const mongoose = require('mongoose');
const querystring = require('querystring');

const arrHttpM = ['GET', 'POST', 'PUT', 'DELETE', ''];
const arrPackM = ['READ', 'CREATE', 'UPDATE', 'DELETE', 'INFO'];

//--
function httpMethod(method) {
  var result = '';

  for (var i = 0; i < arrPackM.length; i++) {
    if (arrPackM[i] === method) {
      result = arrHttpM[i];
      break;
    }
  }

  return result;
}

//--
function packageMethod(method) {
  var result = '';

  for (var i = 0; i < arrPackM.length; i++) {
    if (arrHttpM[i] === method) {
      result = arrPackM[i];
      break;
    }
  }

  return result;
}

//--
function jsonToBase64(data) {
  const json = JSON.stringify(data);
  return Buffer.from(json).toString('base64');
}

//--
function base64ToJson(data) {
  let json = {};
  try {
    let buffer = Buffer.from(data, 'base64').toString('utf8');
    json = JSON.parse(buffer);
  } catch (e) {
    console.log("not JSON");
  }
  return json;
}

//--
function base64ToBufer(data) {
  let json = {};
  try {
    let buffer = new Buffer.from(data, 'base64')//.toString('utf8');
    json = buffer;
  } catch (e) {
    console.log("not JSON");
  }
  return json;
}

function extendSchema(Schema, definition, options) {
  return new mongoose.Schema(
    Object.assign({}, Schema.obj, definition),
    options
  );
}

function isJsonStr(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

module.exports = {
  arrPackM, arrHttpM, isJsonStr,
  httpMethod, packageMethod,
  jsonToBase64, base64ToJson, base64ToBufer,
  extendSchema
};