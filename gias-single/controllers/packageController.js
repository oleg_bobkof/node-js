'use strict';

const
  axios = require("axios"),
  async = require("async"),
  querystring = require('querystring');

const
  utils = require('../utils/common'),
  sign = require('../utils/signature.js'),
  packageOUT = require('../models/packageOut'),
  packageIN = require('../models/packageINExt'),
  config = require('../config/configure'),
  { createErr400, createErr404, createErr500, createHttpErr, createErr } = require('../utils/errors'),
  { param, body, header, validationResult } = require('express-validator'),
  logger = require('../utils/logger');

const arrPackMethod = utils.arrPackM;

const errorFormatter = ({ location, msg, param, value }) => {
  return `${location}[${param}]: ${value} - ${msg}`;
};

const logFormatter = (id, options) => {
  return `package_in [${id}]: ${options.method} ${options.baseURL}${options.url} - `;
};

// Save input package to DB
function savePackageIN(package_in, cb) {

  package_in.save(function (err, doc) {
    if (err) { return cb(createErr400(-102, err)); }
    cb(null, doc);
  });

}

// Prepare output package
function preparePackageOUT(data) {

  let buffer = utils.jsonToBase64(data);
  let package_out = new packageOUT({ package_output: { payload: buffer } }).package_output;
  // Sign output package
  sign.singPackage(package_out);
  return package_out;

}

// Validation request
function validationReq(request) {
  // Extract the validation errors from a request.
  const errors = validationResult(request).formatWith(errorFormatter);
  if (!errors.isEmpty()) {
    let err = new createHttpErr.BadRequest(errors.array());
    err.headers = { error_code: -105 };
    throw err;
  }

}

function getErrData(obj) {
  let res = {};
  if (obj && typeof obj === "object") {
    res = obj;
  }
  return res;
}

// Make http request
function makeRequest(options, package_in, cb) {

  axios(options)
    .then(function (response) {
      package_in.http_status = response.status;
      logger.info(logFormatter(package_in.id, options), { message: response.status });
      let package_out = preparePackageOUT(response.data);
      if (response.status > 300) {
        package_in.error_data = response.data;
      }
      cb(null, package_out);
    })
    .catch(function (error) {
      package_in.log = error.message;
      logger.error(logFormatter(package_in.id, options), { message: error.message });
      if (error.response) {
        package_in.http_status = error.response.status;
        package_in.error_data = getErrData(error.response.data);
        cb(null, error.response.data);
      } else if (error.request) {
        package_in.http_status = 400;
        let err400 = createErr(400, error);
        package_in.error_data = getErrData(err400);
        cb(null, err400);
      } else {
        package_in.http_status = 500;
        let err500 = createErr(500, error);
        package_in.error_data = getErrData(err500);
        cb(null, err500);
      }
    });

}

// --
exports.api_package_post = [

  // Validate fields.
  header('service_method')
    .isLength({ min: 1 }).withMessage('empty value')
    .trim()
    .isIn(arrPackMethod).withMessage('invalid value'),
  header('package_id')
    .isUUID(),
  body('path')
    .isLength({ min: 1 }).withMessage('empty value')
    .trim(),
  body('method')
    .isLength({ min: 1 }).withMessage('empty value')
    .trim()
    .isIn(arrPackMethod).withMessage('invalid value'),
  body('abonent')
    .isLength({ min: 1 }).withMessage('empty value')
    .trim(),

  // Process request after validation and sanitization.
  (req, res, next) => {

    validationReq(req);

    // Create a packageIN object with escaped and trimmed data.
    let package_in = new packageIN({ header: req.headers, package_input: req.body, request: {} });
    let path = req.body.path || package_in.header.service_path;
    let method = req.body.method || package_in.header.service_method;
    let params = querystring.parse(req.body.query_string);
    let data = utils.base64ToJson(req.body.payload);

    if (path === '/ping' || path === 'ping') {
      let { package_input: { payload } } = package_in;
      res.status(200).json({ payload: payload, sign: null, sign_algorithm: null });
      return;
    }

    // Check signature of package
    sign.verifyPackage(package_in);

    const options = JSON.parse(JSON.stringify(config.gias_mf.request));
    options.method = utils.httpMethod(method);
    options.url = path;
    options.params = params;
    options.data = data;
    package_in.request = options;
    // console.log(options);

    async.series([
      function (callback) {
        makeRequest(options, package_in, callback);
      },
      function (callback) {
        savePackageIN(package_in, callback);
      },
    ],
      function (err, results) {
        if (err) { return next(err); }
        let { http_status = 200 } = package_in;
        res.status(http_status).send(results[0]);
      }
    );

  }

];

//
exports.api_package_get = [

  // Validate fields.
  param('id')
    .isLength({ min: 1 })
    .withMessage('empty value')
    .trim(),

  // Process request after validation and sanitization.
  (req, res, next) => {

    validationReq(req);

    packageIN.findById(req.params.id, function (err, packagein_ext) {
      if (err) { return next(createErr400(-102, err)); }

      if (packagein_ext == null) {
        let err404 = new createHttpErr.NotFound('Пакет таким идентификатором не найден');
        return next(err404);
      }

      let { package_input: { method, payload, path, query_string } } = packagein_ext;
      let params = querystring.parse(query_string);
      let data = utils.base64ToJson(payload);
      const options = JSON.parse(JSON.stringify(config.gias_mf.request));
      options.method = utils.httpMethod(method);
      options.url = path;
      options.params = params;
      options.data = data;

      if (req.path.split("/")[3] == "resend") {
        axios(options)
          .then(function (response) {
            res.status(response.status).send(response.data);
          })
          .catch(function (error) {
            if (error.response) {
              next(createErr404(error.response));
            } else if (error.request) {
              next(createErr400(400, error));
            } else {
              next(createErr500(error));
            }
          });
      } else {
        res.status(200).json(options);
      }
    });

  }

];
