'use strict';

const
  querystring = require('querystring'),
  async = require("async"),
  url = require('url'),
  axios = require("axios"),
  https = require("https");


const
  utils = require('../utils/common.js'),
  sign = require('../utils/signature.js'),
  packageOUT = require('../models/packageOut'),
  packageIN = require('../models/packageIN'),
  config = require('../config/configure'),
  { createErr400, createErr } = require('../utils/errors'),
  logger = require('../utils/logger');

const logFormatter = (id, options) => {
  return `package_out [${id}]: ${options.method} ${options.baseURL}${options.url} - `;
};

// Save input package to DB
function savePackageOUT(package_in, cb) {

  package_in.save(function (err, doc) {
    if (err) { return cb(createErr400(-102, err)); }
    cb(null, doc);
  });

}

function getErrData(obj) {
  let res = {};
  if (obj && typeof obj === "object") {
    res = obj;
  }
  return res;
}

// Make http request
function makeRequest(options, package_in, cb) {
  options.responseType = 'json';
  axios(options)
    .then(function (response) {
      package_in.http_status = response.status;
      logger.info(logFormatter(package_in.id, options), { message: response.status });
      let package_out = new packageOUT({ package_output: response.data }).package_output;
      sign.verifyPackage(package_out);
      let payload = package_out.payload || 'e30=';
      let json = utils.base64ToJson(payload) || {};
      cb(null, json);
    })
    .catch(function (error) {
      package_in.log = error.message;
      logger.error(logFormatter(package_in.id, options), { message: error.message });
      if (error.response) {
        package_in.http_status = error.response.status;
        package_in.error_data = getErrData(error.response.data);
        cb(null, error.response.data);
      } else if (error.request) {
        package_in.http_status = 400;
        let err400 = createErr(400, error);
        package_in.error_data = getErrData(err400);
        cb(null, err400);
      } else {
        package_in.http_status = 500;
        let err500 = createErr(500, error);
        package_in.error_data = getErrData(err500);
        cb(null, err500);
      }
    });

}

// Make http request 2
function makeRequestDownloads(options, package_in, cb) {
  //const config = JSON.parse(JSON.stringify(options));
  options.responseType = 'arraybuffer';
  axios(options)
    .then(function (response) {
      package_in.http_status = response.status;
      package_in.headers = response.headers;
      logger.info(logFormatter(package_in.id, options), { message: response.status });
      cb(null, response.data);
    })
    .catch(function (error) {
      package_in.log = error.message;
      logger.error(logFormatter(package_in.id, options), { message: error.message });
      if (error.response) {
        package_in.http_status = error.response.status;
        package_in.error_data = getErrData(error.response.data);
        cb(null, error.response.data);
      } else if (error.request) {
        package_in.http_status = 400;
        let err400 = createErr(400, error);
        package_in.error_data = getErrData(err400);
        cb(null, err400);
      } else {
        package_in.http_status = 500;
        let err500 = createErr(500, error);
        package_in.error_data = getErrData(err500);
        cb(null, err500);
      }
    });

}


//--
exports.api_gias_get = function (req, res, next) {

  let url_parts = url.parse(req.url, true);
  let query_string = querystring.stringify(url_parts.query);
  let method = utils.packageMethod(req.method);
  let { request, request: { headers } } = config.gias;

  let header = {
    abonent_id: headers.abonent_id,
    service_path: url_parts.pathname,
    service_method: method
  };

  let package_input = {
    path: url_parts.pathname,
    method: method,
    abonent: config.gias.request.abonent
  };

  if (query_string) {
    package_input.query_string = query_string;
  }

  let request_in = {
    method: req.method,
    url: url_parts.pathname,
    data: req.body
  };

  if (req.body.constructor === Object && Object.keys(req.body).length !== 0) {
    package_input.payload = utils.jsonToBase64(req.body);
  }

  let package_in = new packageIN({ header: header, package_input: package_input, request: request_in });
  const options = JSON.parse(JSON.stringify(request));
  const package_header = JSON.parse(JSON.stringify(package_in.header));
  options.data = package_in.package_input;
  options.headers = { ...options.headers, ...package_header };
  options.httpsAgent = new https.Agent({ rejectUnauthorized: false });

  const arrayOfStr = (url_parts.pathname || '').split('/') || [];
  const isDownloadReq = arrayOfStr.includes('contracts') && arrayOfStr.includes('documents') && (req.method === utils.arrHttpM[1]);

  if (isDownloadReq) {
    makeRequestDownloads(options, package_in,
      function (err, results) {
        if (err) { return next(err); }
        const { http_status = 200, headers = {} } = package_in;
        res
          .header(headers)
          .status(http_status)
          .send(results);
      });
  } else {
    async.series([
      function (callback) {
        makeRequest(options, package_in, callback);
      },
      function (callback) {
        savePackageOUT(package_in, callback);
      },
    ],
      function (err, results) {
        if (err) { return next(err); }
        let { http_status = 200 } = package_in;
        res.status(http_status).send(results[0]);
      }
    );
  }


};