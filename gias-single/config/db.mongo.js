const mongoose = require('mongoose');
const db = mongoose.connection;
const logger = require('../utils/logger');

const getUri = (db) => {
  return `mongodb://${db.host}:${db.port}/${db.name}`;
};

// set up mongoose connection
function initDB(dbConfig) {
  
  let db_url = dbConfig.uri;
  let { options } = dbConfig;
  mongoose.connect(db_url, options)
    .then(
      () => {
        logger.info('MongoDB is connected: ', { message: getUri(db) });
        mongoose.Promise = global.Promise;
      },
      err => {
        logger.error('App starting error: ', { message: err.stack });
        process.exit(1);
      }
    );

}

module.exports = {
  initDB, db
};
