const path = require('path');
const yaml_config = require('node-yaml-config');
const logger = require( '../utils/logger');

const
  userArgs = process.argv.slice(2);
  configFile = userArgs[0] || 'config.yaml';
  config = {};
  logFormat ='[:date[iso]] [info] => :remote-addr - :remote-user :method :url :status :response-time ms - :res[content-length]';

// Configuration sections
config.webserver = {};
config.database = {};
config.gias = {};
config.gias_mf = {};

config.webserver.port = process.env.PORT || 3020;
config.webserver.debug = 'common';
config.database.uri = process.env.MONGODB_URI;

// Configuration
(function init() {
  try {
    const filename = path.join(__dirname, '../') + configFile;
    const appConfig = yaml_config.load(filename);
    const { webserver, database, gias, gias_mf } = appConfig;
    logger.info('Read config: ', { message: filename });
    // set config
    config.webserver = (webserver) ? webserver : config.webserver;
    config.webserver.debug = logFormat;
    config.database = (database) ? database.mongo : config.database;
    config.gias = (gias) ? gias : config.gias;
    config.gias_mf = (gias_mf) ? gias_mf : config.gias_mf;
  } catch (err) {
    console.log(err);
    process.exit(0);
  }
}());

module.exports = config;
