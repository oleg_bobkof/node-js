const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
// utils
const config = require('./config/configure');
const { errorHandler, createHttpErr } = require('./utils/errors');
const app = express();
const debug = config.webserver.debug;

// view engine setup
app.disable('etag');
app.use(logger(debug));
app.use(express.json({ limit: '5mb' }));
app.use(express.urlencoded({ limit: '5mb', extended: false }));
app.use(cookieParser());
app.use(require('./routes'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  let error = new createHttpErr.NotFound('The requested URL [' + req.path + '] was not found');
  next(error);
});

// error handler
app.use(errorHandler);

module.exports = app;