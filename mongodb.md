mongo --username "admin" --password "power2019" --authenticationDatabase admin 

use admin
db.createUser(
  {
    user: "admin",
    pwd: "",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
  }
)

use admin
db.createUser(
   {
     user: "gias",
     pwd: "gias",
     roles:
       [
         { role: "readWrite", db: "gias-single" },
	        { role: "readWrite", db: "gias-mf" },
	        { role: "readWrite", db: "gias-scheduler" },
	        { role: "readWrite", db: "gias-lt" },
	        { role: "readWrite", db: "gias-scheduler-lt" },
       ]
   }
)
     
use admin
db.grantRolesToUser(
    "gias",
    [
      	{ role: "readWrite", db: "gias-scheduler-7" },
	      { role: "readWrite", db: "gias-lt-7" },
	      { role: "readWrite", db: "gias-scheduler-lt-7" },
    ]
)
