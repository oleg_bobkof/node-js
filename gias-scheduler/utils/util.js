const
  Koa = require('koa'),
  logger = require('koa-logger'),
  Router = require('koa-router'),
  bodyParser = require('koa-bodyparser');

const bootstrapKoaApp = () => {
  const app = new Koa();
  const router = new Router();
  app.use(logger());
  app.use((ctx, next) => next()
    .catch(error => {
      console.dir(error);
      ctx.body = String(error);
      ctx.status = error.status || 500;
    })
  );
  app.use(bodyParser({
    onerror(error, ctx) {
      ctx.throw(400, `cannot parse request body, ${JSON.stringify(error)}`);
    }
  }));
  app.use(router.routes());
  return { app, router };
};

const isValidDate = date => Object.prototype.toString.call(date) === '[object Date]' && !isNaN(date.getTime());

const repeatPerKey = (keys = {}) => count => (key, fn) => () => {
  if (!(key in keys)) {
    keys[key] = 0;
  }
  if (keys[key] < count) {
    fn();
    keys[key]++;
  }
};

const oncePerKey = repeatPerKey()(1);

const isJsonString = (str) => {
  try {
    var o = JSON.parse(str);
    if (o && typeof o === "object") {
      return o;
    }
  }
  catch (e) { return {} }
};

class AsyncCounter {
  constructor(countTimes) {
    let currentCount = 0;
    this.countTimes = countTimes;
    this.ready = new Promise(resolveReady => {
      this.finished = new Promise(resolveFinished => {
        const count = () => {
          currentCount++;
          if (currentCount === countTimes) {
            resolveFinished();
          }
          return currentCount;
        };
        this.count = () => this.ready.then(() => count());
        resolveReady();
      });
    });
  }
}

module.exports = { bootstrapKoaApp, isValidDate, isJsonString, oncePerKey, AsyncCounter };
