const mongoose = require('mongoose');
const mongodb = mongoose.mongo;
var db = mongoose.connection;

// set up mongoose connection
function initDB(dbConfig) {

  let db_url = dbConfig.agendaMongoUrl;
  let options = dbConfig.dboptions;
  mongoose.connect(db_url, options)
    .then(
      () => {
        console.log('MongoDB is connected: ' + db_url);
        db = db.db;
        mongoose.Promise = global.Promise;
      },
      err => {
        console.error('App starting error:', err.stack);
        process.exit(1);
      }
    );

}

module.exports = {
  initDB, db, mongodb
};