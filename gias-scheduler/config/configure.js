const path = require('path');
const yaml_config = require('node-yaml-config');

var userArgs = process.argv.slice(2);
var configFile = userArgs[0] || 'config.yaml';
var config = {};

//- configuration sections
config.webserver = {};
config.database = {};

let collection = 'agendaJobs';
let definitions = 'jobDefinitions';
let agendaMongoUrl = 'mongodb://localhost:27017/agenda';
let dboptions = { useNewUrlParser: true };
let timeout = 10000;

const settings = {
  get agendaMongoUrl() {
    return agendaMongoUrl;
  },
  set agendaMongoUrl(value) {
    agendaMongoUrl = value;
  },
  get dboptions() {
    return dboptions;
  },
  set dboptions(value) {
    dboptions = value;
  },
  get port() {
    return port;
  },
  set port(value) {
    port = value;
  },
  get collection() {
    return collection;
  },
  set collection(value) {
    collection = value;
  },
  get definitions() {
    return definitions;
  },
  set definitions(value) {
    definitions = value;
  },
  get timeout() {
    return timeout;
  },
  set timeout(value) {
    timeout = value;
  }
};

// Configuration
(function init() {
  try {
    let filename = path.join(__dirname, '../') + configFile;
    let appConfig = yaml_config.load(filename);
    let { webserver, database, definitions, scheduler } = appConfig;
    console.log('Read config: ' + filename);

    if (webserver) {
      config.webserver = webserver;
      settings.port = webserver.port;
      settings.timeout = webserver.timeout;
    }

    if (database) {
      config.database = database.mongo;
      settings.agendaMongoUrl = config.database.uri;
      settings.dboptions = config.database.options;
    }

    if (definitions) {
      config.definitions = definitions;
    }

    if (scheduler) {
      config.scheduler = scheduler;
    }

  } catch (err) {
    console.log(err);
    // process.exit(0);
  }
}());

module.exports = { settings, config };