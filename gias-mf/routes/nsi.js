const express = require('express');
const router = express.Router();

// Require our controllers.
var department_controller = require('../controllers/departmentController'); 
var economic_controller = require('../controllers/economicController');
var functional_controller = require('../controllers/functionalController');
var program_controller = require('../controllers/programController');
var budget_controller = require('../controllers/budgetController');
var bo_controller = require('../controllers/boController');
var main_controller = require('../controllers/mainController');

// -- GET nsi total 
router.get('/', main_controller.index);

// GET request for Department class.
router.get('/department_classifications', department_controller.api_department_list);
router.get('/department_classification/:id', department_controller.api_department_item);

// GET request for Economic class.
router.get('/economic_classifications', economic_controller.api_economic_list);
router.get('/economic_classification/:id', economic_controller.api_economic_item);

// GET request for Functional class.
router.get('/functional_classifications', functional_controller.api_functional_list);
router.get('/functional_classification/:id', functional_controller.api_functional_item);

// GET request for Program class.
router.get('/program_classifications', program_controller.api_program_list);
router.get('/program_classification/:id', program_controller.api_program_item);

// GET request for Budget.
router.get('/budgets', budget_controller.api_budget_list);
router.get('/budget/:id', budget_controller.api_budget_item);

// GET request for Organization.
router.get('/organizations', bo_controller.api_bo_list);
router.get('/organization/:id', bo_controller.api_bo_item);

module.exports = router;