'use strict';

var express = require('express');
var router = express.Router();
var swaggerUi = require('swagger-ui-express');
var YAML = require('yamljs');
var path = require('path')

var swaggerDocument = YAML.load(path.join(__dirname, '../api/') + 'asfr.yaml');

var options = {
    explorer: true,
    customCss: '.swagger-ui .topbar { display: none }'
};

/* GET swagger UI */
router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(swaggerDocument, options));

module.exports = router;