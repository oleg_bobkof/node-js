'use strict';

var express = require('express'),
  router = express.Router();

var contractdocController = require('../controllers/contractdocController');

// routers
router.get('/:id/contract/:fileName', contractdocController.api_contractdoc_get);
router.get('/:id/contractdoc/:fileId', contractdocController.api_contractdoc2_get);
router.get('/:id/doclinks', contractdocController.api_contractdoclinks_get);

module.exports = router;