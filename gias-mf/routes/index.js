const 
  pjson = require('../package.json'),
  express = require('express'),
  router = express.Router();

router.use('/api-docs', require('./api-docs'));
router.use('/nsi', require('./nsi'));
router.use('/gias/nsi', require('./nsi'));
router.use('/gias', require('./contracts'));
router.use('/gias', require('./reservations'));
router.use('/asfr/download', require('./download'));
router.use('/diagnostics', require('./diagnostics'));
  
// GET home page.
router.get('/', function (req, res) {
  res.redirect('/api-docs');
});

// GET ping -> pong
router.get('/ping', function (req, res) {
  res.status(200).json({
    msg: 'pong',
    name: pjson.name,
    version: pjson.version,
  });
});

module.exports = router;
