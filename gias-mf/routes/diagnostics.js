const
  express = require('express'),
  router = express.Router(),
  config = require('../config/configure'),
  { api_jobs_get } = require('../controllers/diagnosticController');


// GET ping -> pong
router.get('/config', function (req, res) {
  res.status(200).json(config);
});

router.get('/check-jobs', api_jobs_get);

module.exports = router;