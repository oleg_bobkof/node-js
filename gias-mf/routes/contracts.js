const
  express = require('express'),
  router = express.Router(),
  contract_controller = require('../controllers/contractController'),
  notification_controller = require('../controllers/notificationController'),
  contractExec_controller = require('../controllers/contractexecController');

// Resend contract
router.get('/contract/:id/resend', contract_controller.api_contract_resend);

// Resubmit contracts
router.get('/contracts/resubmit', contract_controller.api_contracts_resubmit);

// Add contract
router.post('/contract', contract_controller.api_contract_post);

// Delete contract 
router.delete('/contract/:id', contract_controller.api_contract_delete);

// Add notifications
router.post('/gias_notifications', notification_controller.api_notification_post);

// Add contract execution
router.post('/contract_execution', contractExec_controller.api_contractexec_post);

module.exports = router;