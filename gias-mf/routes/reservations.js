const
  express = require('express'),
  router = express.Router(),
  reservation_controller = require('../controllers/reservationController');

// Get reservation status
router.get('/reservation/:requestId/status', reservation_controller.api_reservation_status);
router.get('/reservation/:requestId/reply', reservation_controller.api_reservation_replay);
// Resubmit reservations
router.get('/reservations/resubmit', reservation_controller.api_reservations_resubmit);
// Resend reservation
router.get('/reservation/:requestId/resend', reservation_controller.api_reservation_resend);

// Add reservation
router.post('/reservation', reservation_controller.api_reservation_post);

// Update reservation
router.put('/reservation/:requestId/financesources', reservation_controller.api_reservation_put);


module.exports = router;