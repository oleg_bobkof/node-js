const mongoose = require('mongoose');
const db = mongoose.connection;
const logger = require('../utils/logger');
const mongodb = mongoose.mongo;

const getUri = (db) => {
  return `mongodb://${db.host}:${db.port}/${db.name}`;
};

// set up mongoose connection
function initDB(dbConfig, callback) {

  let db_url = dbConfig.uri;
  let { options } = dbConfig;
  mongoose.connect(db_url, options)
    .then(
      () => {
        logger.info('MongoDB is connected: ', { message: getUri(db) });
        mongoose.Promise = global.Promise;
        callback(null, db);
      },
      err => {
        logger.error('App starting error: ', { message: err.stack });
        callback(err);
        process.exit(1);
      }
    );

}

// сlose mongoose connection
function closeDB(callback) {
  db.close().then(
    () => {
      logger.warn('MongoDB connection closed.');
      callback();
    },
    err => {
      callback(err);
    }
  );
}

module.exports = { initDB, closeDB, db, mongodb };