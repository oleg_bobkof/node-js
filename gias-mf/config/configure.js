const path = require('path');
const yaml_config = require('node-yaml-config');
const logger = require('../utils/logger');

const userArgs = process.argv.slice(2)
const configFile = userArgs[0] || 'config.yaml';
const config = {};
const logFormat = '[:date[iso]] [info] => :remote-addr - :remote-user :method :url :status :response-time ms - :res[content-length]';

//- configuration sections
config.webserver = {};
config.database = {};
config.nginx = {};
config.nginx.requests = {};
config.gias_scheduler = {};
config.gias_scheduler.requests = {};
config.gias_scheduler.filters = [];
config.gias_single = {};
config.gias_single.requests = {};

config.webserver.port = process.env.PORT || 3010;
config.webserver.debug = 'common';
config.database.uri = process.env.MONGODB_URI;

// Configuration
(function init() {
  try {
    let filename = path.join(__dirname, '../') + configFile;
    let appConfig = yaml_config.load(filename);
    let { webserver, database, nginx, gias_single, gias_scheduler: { requests, filters } } = appConfig;
    logger.info('Read config: ', { message: filename });
    config.webserver = (webserver) ? webserver : config.webserver;
    config.webserver.debug = logFormat;
    config.database = (database) ? database.mongo : config.database;
    config.gias_scheduler.requests = (requests) ? requests : config.gias_scheduler.requests;
    config.gias_scheduler.filters = (filters) ? filters : config.gias_scheduler.filters;
    config.nginx = (nginx) ? nginx : config.nginx;
    config.gias_single = (gias_single) ? gias_single : config.gias_single;
  } catch (err) {
    console.log(err);
    process.exit(0);
  }
}());

module.exports = config;