'use strict';

const url = require('url');
const { createHttpErr, createErr400 } = require('../utils/errors');
const Department = require('../models/department');

// --
const getDepartment = (item) => (
  {
    classId: item._id,
    finYear: item.finYear,
    chp: item.chp,
    name: item.name,
    inputDateTime: item.inputDateTime
  }
);

// Get list of all Department by year.
exports.api_department_list = function (req, res, next) {

  let url_parts = url.parse(req.url, true);
  let { year = new Date().getFullYear() } = req.query;
  let qparams = { finYear: year };

  Department.find(qparams)
    .exec(function (err, list_departments) {
      if (err) { return next(createErr400(-102, err)); }
      if (list_departments.length == 0) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.search);
        return next(error);
      }

      let departments = list_departments.map(department_item => getDepartment(department_item));
      res.status(200).json(departments);
    });

};

// Get Department item by id.
exports.api_department_item = function (req, res, next) {

  let url_parts = url.parse(req.url, true);

  Department.findById(req.params.id)
    .populate('department')
    .exec(function (err, department_item) {
      if (err) { return next(createErr400(-102, err)); }
      if (department_item == null) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.path);
        return next(error);
      }

      let department = getDepartment(department_item);
      res.status(200).send(department);
    });

};