const
  async = require("async"),
  axios = require("axios"),
  logger = require('../utils/logger');

const BUCKET_NAME = 'contracts';
const CHUNKS_COLL = 'contracts.chunks';
const FILES_COLL = 'contracts.files';

const config = require('../config/configure');
const { db, mongodb } = require('../config/db.mongo');
const ContractDoc = require('../models/contractdoc');
const { createErr404, createHttpErr } = require('../utils/errors');

// Save contract document
function saveContractDoc(contractId, file_id, contractdoc, cb) {

  let { type, link: { link, name = 'unknow' } } = contractdoc;
  ContractDoc.findOne({ contractId: contractId, file_id: file_id }, function (err, doc) {
    if (err) { return cb(err); }
    if (!doc) {
      doc = new ContractDoc({
        contractId: contractId,
        file_id: file_id,
        file_type: type,
        file_name: name,
        file_link: link
      });
      doc.save(function (err, doc) {
        if (err) { return cb(err); }
        cb(null, doc);
      });
    } else { cb(null, doc); }
  });

}

// Save contract documents
function saveContractDocuments(responseArray, docs, contractId, cb) {

  async.series([
    function (callback) {
      deleteContractDocs(contractId, callback);
    },
    function (callback) {
      let bucket = new mongodb.GridFSBucket(db.db, { bucketName: BUCKET_NAME });
      async.mapSeries(
        responseArray,
        function (item, rdCb) {
          let i = responseArray.indexOf(item);
          let doc = docs[i];
          if (doc) {
            let { type, link: { name = 'unknow' } } = doc;
            logger.info(`${contractId} - contract document ${type}: ${name}`);
            let uploadStream = bucket.openUploadStream(name);
            let file_id = uploadStream.id;
            saveContractDoc(contractId, file_id, doc, function (err, doc) {
              if (err) { rdCb(err); }
              item.pipe(uploadStream);
              rdCb(null, doc);
            });
          } else {
            rdCb(null, doc);
          }
        }, function (err, results) {
          callback(err, results);
        });
    },
  ], function (err, results) {
    if (err) {
      return cb(err, null);
    }
    cb(null, results);
  });

}

// Delete contract docs
const deleteContractDocs = exports.deleteContractDocs = function (contractId, cb) {

  async.waterfall([
    function (callback) {
      ContractDoc.find({ contractId: contractId }, callback);
    },
    function (contractdocs, callback) {
      let bucket = new mongodb.GridFSBucket(db.db, { bucketName: BUCKET_NAME });
      async.map(
        contractdocs,
        function (contractdoc, delCallback) {
          let { file_id } = contractdoc;
          bucket.delete(file_id, function (err) {
            //if (err) { delCallback(err) } else {
            ContractDoc.deleteOne({ contractId: contractId, file_id: file_id }).exec(delCallback);
            //};
          });
        },
        function (err, del_items) {
          if (err) { callback(err); } else {
            callback(err, contractdocs, del_items);
          }
        }
      );
    },
  ],
    function (err) {
      if (err) { cb(err); } else { cb(); }
    }
  );

};

// Uploads contract docs
exports.loadContractDocs = function (contract, cb) {

  const type = 3;
  const { contractId, contract: { contractDocuments = [] } } = contract;
  let document = contractDocuments.find(doc => doc.type !== type && doc.link.link);

  if (!document) {
    let err = new Error(`Contract.contractDocuments with type ${type} not found`);
    return cb(err, null);
  }

  let docs = contractDocuments.filter(doc => doc.type === type && doc.link.link);
  docs.push(document);
  let options = {};
  let promiseArray = [];
  let responseArray = [];

  if (config.gias_single.requests.download) {
    options = config.gias_single.requests.download || {};
    promiseArray = docs.map(doc => {
      let uri = options.url.replace(`:contractid`, contractId);
      const data = {
        link: doc.link.link,
        name: doc.link.name
      };
      return axios.post(uri, data, options);
    });
  } else {
    if (config.nginx.requests.download) {
      options = config.nginx.requests.download || {};
      promiseArray = docs.map(doc => axios.get(doc.link.link, options)) || [];
    }
  }

  axios.all(promiseArray)
    .then(axios.spread((...args) => {
      for (let i = 0; i < args.length; i++) {
        responseArray.push(args[i].data);
      }
    }))
    .then(() => {
      saveContractDocuments(responseArray, docs, contractId, cb);
    })
    .catch((err) => {
      err.message = `Cannot download attachments - ${err.message}`;
      cb(err);
    });
  /*
  .finally(function () {
    // always executed
  });
  */
};

//
exports.api_contractdoc_get = function (req, res, next) {

  let { id, filename } = req.params;
  async.waterfall([
    function (callback) {
      ContractDoc.findOne({ contractId: id }, callback);
    },
    function (contractdocs, callback) {
      let { file_id } = contractdocs;
      let bucket = new mongodb.GridFSBucket(db.db, { bucketName: BUCKET_NAME });
      let downloadStream = bucket.openDownloadStream(file_id);
      //res.setHeader('Content-disposition', 'attachment; filename=test.jpg');
      downloadStream.on('data', (chunk) => {
        res.write(chunk);
      });
      downloadStream.on('error', (err) => {
        callback(createErr404(err));
      });
      downloadStream.on('end', () => {
        callback(null, contractdocs, null);
      });
    },
  ],
    function (err) {
      if (err) { return next(err); }
      res.end();
    }
  );

};

//
exports.api_contractdoc2_get = function (req, res, next) {

  let { id, fileId } = req.params;
  async.waterfall([
    function (callback) {
      ContractDoc.findOne({ contractId: id, file_id: fileId }, callback);
    },
    function (contractdocs, callback) {
      let bucket = new mongodb.GridFSBucket(db.db, { bucketName: BUCKET_NAME });
      let { file_id } = contractdocs;
      let downloadStream = bucket.openDownloadStream(file_id);
      downloadStream.on('data', (chunk) => {
        res.write(chunk);
      });
      downloadStream.on('error', (err) => {
        callback(createErr404(err));
      });
      downloadStream.on('end', () => {
        callback(null, contractdocs, null);
      });
    },
  ],
    function (err) {
      if (err) { return next(err); }
      res.end();
    }
  );

};

//
exports.api_contractdoclinks_get = function (req, res, next) {

  const { id } = req.params;
  ContractDoc.find({ contractId: id }, function (err, contractdocs) {
    if (err) { return next(err); }
    let doclinks = contractdocs.map(function (doc) {
      return {
        contractId: doc.contractId,
        file_id: doc.file_id,
        file_type: doc.file_type,
        file_name: doc.file_name,
        file_link: doc.file_link
      };
    }) || {};

    res.status(200).json(doclinks);
  });

};