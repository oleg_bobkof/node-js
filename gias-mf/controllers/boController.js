'use strict';

const url = require('url');
const { createHttpErr, createErr400 } = require('../utils/errors');
const BO = require('../models/bo');

// --
const getBO = (item) => (
  {
    boId: item._id,
    finYear: item.finYear,
    finId: item.finId,
    unp: item.unp,
    unk: item.unk,
    treasuryCode: item.treasuryCode,
    name: item.name,
    inputDateTime: item.inputDateTime
  }
);

// GET list of all Organization by year.
exports.api_bo_list = function (req, res, next) {

  let url_parts = url.parse(req.url, true);
  let { year = new Date().getFullYear() } = req.query;
  let qparams = { finYear: year };

  BO.find(qparams)
    .exec(function (err, list_bos) {
      if (err) { return next(createErr400(-102, err)); }
      if (list_bos.length == 0) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.search);
        return next(error);
      }

      let bos = list_bos.map(bo_item => getBO(bo_item));
      res.status(200).json(bos);
    });
};

//  GET Organization by Id.
exports.api_bo_item = function (req, res, next) {

  let url_parts = url.parse(req.url, true);

  BO.findById(req.params.id)
    .populate('bo')
    .exec(function (err, bo_item) {
      if (err) { return next(createErr400(-102, err)); }
      if (bo_item == null) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.path);
        return next(error);
      }

      let bo = getBO(bo_item);
      res.status(200).json(bo);
    });

};