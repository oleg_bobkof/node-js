const
  { body, validationResult } = require('express-validator');

const
  Notification = require('../models/notification'),
  { createHttpErr, createErr400 } = require('../utils/errors'),
  { arrPackMethod, arrDic } = require('../utils/common'),
  { addJobNotification } = require('../controllers/jobController');

const errorFormatter = ({ location, msg, param, value }) => {
  return `${location}[${param}]: ${value} - ${msg}`;
};

// Handle Notification create on POST.
exports.api_notification_post = [

  // Validate fields.
  body('dictionary')
    .isLength({ min: 1 })
    .trim().withMessage('empty value')
    .isIn(arrDic),

  body('operation')
    .isLength({ min: 1 })
    .trim().withMessage('empty value')
    .isIn(arrPackMethod),

  body('objectIDs')
    .isArray(),

  // Sanitize fields.
  body('dictionary')
    .trim().escape(),
  body('operation')
    .trim().escape(),

  // Process request after validation and sanitization.
  (req, res, next) => {

    // Extract the validation errors from a request.
    const errors = validationResult(req).formatWith(errorFormatter);

    if (!errors.isEmpty()) {
      let err = new createHttpErr.BadRequest(errors.array());
      err.headers = { error_code: -105 };
      return next(err);
    }

    // Create notification object with escaped and trimmed data
    let notification = new Notification({
      dictionary: req.body.dictionary,
      operation: req.body.operation,
      objectIDs: req.body.objectIDs
    });

    // Save notification
    notification.save(function (err, notification) {
      if (err) { return next(createErr400(-102, err)); }

      addJobNotification(notification, function (err, job) {
        if (err) { return next(createErr400(-103, err)); }
        
        res.status(200).json({});
      });

    });

  }

];
