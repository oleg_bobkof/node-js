const async = require('async');
const url = require('url');

const Department = require('../models/department');
const Economic = require('../models/economic');
const Functional = require('../models/functional');
const Program = require('../models/program');
const Budget = require('../models/budget');
const BO = require('../models/bo');

exports.index = function (req, res) {

  let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
  if (fullUrl.lastIndexOf('/', fullUrl) != (fullUrl.length - 1)) {
    fullUrl = fullUrl + '/';
  }

  let url_parts = url.parse(fullUrl, true);
  //let year = new Date().getFullYear();

  async.parallel({
    depatment_count: function (callback) {
      Department.countDocuments(callback);
    },
    economic_count: function (callback) {
      Economic.countDocuments(callback);
    },
    functional_count: function (callback) {
      Functional.countDocuments(callback);
    },
    program_count: function (callback) {
      Program.countDocuments(callback);
    },
    budget_count: function (callback) {
      Budget.countDocuments(callback);
    },
    bo_count: function (callback) {
      BO.countDocuments(callback);
    },
  }, function (err, result) {
    if (err) { return next(err); }
    res.json([
      {
        catalog: {
          id: 'functional_classifications',
          name: 'Функциональная классификация',
          url: url_parts.href + 'functional_classifications',
          count: result.functional_count
        }
      },
      {
        catalog: {
          id: 'economic_classifications',
          name: 'Экономическая классификация',
          url: url_parts.href + 'economic_classifications',
          count: result.economic_count
        }
      },
      {
        catalog: {
          id: 'program_classifications',
          name: 'Программная классификация',
          url: url_parts.href + 'program_classifications',
          count: result.program_count
        }
      },
      {
        catalog: {
          id: 'department_classifications',
          name: 'Ведомственная классификация',
          url: url_parts.href + 'department_classifications',
          count: result.depatment_count
        }
      },
      {
        catalog: {
          id: 'budgets',
          name: 'Бюджеты',
          url: url_parts.href + 'budgets',
          count: result.budget_count
        }
      },
      {
        catalog: {
          id: 'organizations',
          name: 'Бюджетные организации',
          url: url_parts.href + 'organizations',
          count: result.bo_count
        }
      }
    ]);
  });
};