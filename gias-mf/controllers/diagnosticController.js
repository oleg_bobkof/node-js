const
  async = require("async"),
  axios = require("axios");

const
  { createErr400, createErr500, createErr404 } = require('../utils/errors'),
  { getJobs } = require('../controllers/jobController'),
  config = require('../config/configure');


// Make http request
const makeRequest = (options, cb) => {

  console.log(options);
  axios(options)
    .then(function (response) {
      cb(null, response.data);
    })
    .catch(function (error) {
      if (error.response) {
        cb(createErr404(error.response));
      } else if (error.request) {
        cb(createErr400(400, error));
      } else {
        cb(createErr500(error));
      }
    });

};

const selectJobs = (cb) => {

  getJobs(function (err, jobs) {
    if (err) { return cb(err); }
    let result = [];
    let map = new Map();
    let items = jobs.filter(jobs => jobs.name.startsWith('send_'));
    for (const item of items) {
      if (!(map.has(item.url))) {
        map.set(item.url, true);
        let elem = { url: item.url };
        result.push(elem);
      }
    }
    return cb(null, result);
  });

};

const api_jobs_get = function (req, res, next) {

  selectJobs(function (err, jobs) {
    if (err) { return next(err); }

    return res.status(200).json(jobs);
  });

};

module.exports = { api_jobs_get };