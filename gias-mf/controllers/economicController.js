'use strict';

const url = require('url');
const { createHttpErr, createErr400 } = require('../utils/errors');
const Economic = require('../models/economic');

// --
const getEconomic = (item) => (
  {
    classId: item._id,
    finYear: item.finYear,
    dCtg: item.dCtg,
    dArt: item.dArt,
    dSArt: item.dSArt,
    dItm: item.dItm,
    name: item.name,
    inputDateTime: item.inputDateTime
  }
);

// GET list of all Economic by year.
exports.api_economic_list = function (req, res, next) {

  let url_parts = url.parse(req.url, true);
  let { year = new Date().getFullYear() } = req.query;
  let qparams = { finYear: year };

  Economic.find(qparams)
    .exec(function (err, list_economics) {
      if (err) { return next(createErr400(-102, err)); }
      if (list_economics.length == 0) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.search);
        return next(error);
      }

      let economics = list_economics.map(economic_item => getEconomic(economic_item));
      res.status(200).json(economics);
    });
};

// GET Economic by Id.
exports.api_economic_item = function (req, res, next) {

  let url_parts = url.parse(req.url, true);

  Economic.findById(req.params.id)
    .populate('economic')
    .exec(function (err, economic_item) {
      if (err) { return next(createErr400(-102, err)); }
      if (economic_item == null) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.path);
        return next(error);
      }

      let economic = getEconomic(economic_item);
      res.status(200).json(economic);
    });

};
