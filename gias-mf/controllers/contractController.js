const
  async = require("async"),
  logger = require('../utils/logger'),
  { body, validationResult, param } = require('express-validator');

const Contract = require('../models/contract');
const ContractLog = require('../models/contractlog');
const { loadContractDocs, deleteContractDocs } = require('../controllers/contractdocController');
const { saveContractLog, updateContractLogStatus, updateContractLogResend, listContractLogResend } = require('../controllers/contractlogController');
const { createHttpErr, createHttpError, createErr400 } = require('../utils/errors');
const { addJobContract } = require('../controllers/jobController');

const errorFormatter = ({ location, msg, param, value }) => {
  return `${location}[${param}]: ${value} - ${msg}`;
};

// Validation request
function validationReq(request) {
  // Extract the validation errors from a request.
  const errors = validationResult(request).formatWith(errorFormatter);
  if (!errors.isEmpty()) {
    let err = new Error(errors.array());
    throw createHttpError(400, err, -115);
  }

}

// Save contract
function saveContract(contract, cb) {

  let contractId = contract.contractId;

  Contract.findOne({ contractId: contractId }, function (err, doc) {
    if (err) { return cb(createErr400(-102, err)); }
    if (!doc) {
      doc = contract;
    } else {
      doc.contractId = contract.contractId;
      doc.contract = contract.contract;
      doc.loadDate = contract.loadDate;
    }
    doc.save(function (err, doc) {
      if (err) { return cb(createErr400(-102, err)); }
      cb(null, doc);
    });
  });

}

// Find contract by ID
function findContract(contractId, cb) {

  Contract.findOne({ contractId: contractId }, function (err, doc) {
    if (err) {
      let err400 = createHttpError(400, err, -102);
      return cb(err400);
    }
    // No results.
    if (doc == null) {
      let err404 = createHttpError(404, 'Договор с таким идентификатором не найден.');
      return cb(err404);
    }

    cb(null, doc);
  });

}

// Resend contract
function resendContract(contractId, cb) {

  async.waterfall([
    function (callback) {
      findContract(contractId, callback);
    },
    function (contract, callback) {
      addJobContract(contract, function (err, job) {
        if (err) { return callback(err); }
        callback(err, contract, job);
      });
    },
    function (contract, job, callback) {
      updateContractLogResend(contractId, function (err, doc) {
        if (err) { return callback(err); }
        callback(err, contract, job, doc);
      });
    },
  ],
    function (err, contract, job, doc) {
      if (err) { return cb(err); }
      cb(null, contract);
    }
  );

}

// Handle Author create on POST.
exports.api_contract_post = [

  // Validate fields.
  body('contractId')
    .isUUID(), //.withMessage('invalid value'),
  body('financeSource')
    .isArray(),
  body('financeSource').custom((value, { req }) => {
    let items = value || [];
    let financeSource = items.filter(item => item.finId && item.finId !== 90001 && item.budgetCost >= 0) || [];
    const obj = { dSct: 0, dSSct: 0, dKnd: 0, dPrgr: 0, chp: 0, dPro: 0, dSPro: 0, dCtg: 0, dArt: 0, dSArt: 0, dItm: 0 };
    let i = 0;
    financeSource.forEach(elem => {
      for (const key in obj) {
        let err_mess = `Contract validation failed: contract.financeSource.${i}: Field ${key} is required.`;
        if (elem.hasOwnProperty(key)) {
          let element = elem[key];
          if (element === null) {
            throw new Error(err_mess);
          }
        } else {
          throw new Error(err_mess);
        }
      } i++;
    });
    // Indicates the success of this synchronous custom validator
    return true;
  }),
  body('').custom((value, { req }) => {
    const docType = 3;
    const { baseContractId = null, terminationExecutionTerm = null, terminationReason = null, contractDocuments = [], financeSource = [] } = value;

    let termination = (terminationExecutionTerm > 0 && terminationReason !== null);
    if (!termination && (terminationExecutionTerm !== null | terminationReason !== null)) {
      throw new Error(`Contract validation failed: terminationExecutionTerm = ${terminationExecutionTerm} & terminationReason = ${terminationReason}`);
    }

    let docs = contractDocuments.filter(item => { return item.type !== docType; }) || [];
    let type = docs[0].type;

    if (docs.length !== 1) {
      throw new Error(`Documents validation failed: more than one attachment with type [1,2,4,5,6]`);
    } else {
      if (termination) {
        if (type < docType) {
          throw new Error(`contractDocuments validation failed: Invalid attachment type = ${type}`);
        }
      } else {
        if ((baseContractId === null && type !== 1) | (type > docType)) {
          throw new Error(`contractDocuments validation failed: Invalid attachment type = ${type}`);
        }
      }
    }

    // check contract costs [type = 1]
    if (type === 1) {
      let items = financeSource.filter(fs => { return fs.innerCost === 0 && fs.fundCost === 0 && fs.budgetCost === 0; }) || [];
      if (items.length > 0) {
        throw new Error(`Contract validation failed: contract.financeSource.innerCost|fundCost|budgetCost = 0}`);
      }
    }

    /*
    contractDocuments.forEach(item => {
      let { link: { signatureLinks: [{ signers = [] }] } } = item;
      if (signers.length < 2) {
        throw new Error(`Signature validation failed: less than two signatures - [${item.link.name}]`);
      }
    });
    */

    // Indicates the success of this synchronous custom validator
    return true;
  }),

  // Process request after validation and sanitization.
  (req, res, next) => {

    // Extract the validation errors from a request.
    const errors = validationResult(req).formatWith(errorFormatter);
    let contractlog = new ContractLog({ contractId: req.body.contractId, contract: req.body });
    let contract = new Contract({ contractId: req.body.contractId, contract: req.body });

    async.series([
      function (callback) {
        saveContractLog(contractlog, callback);
      }, // 1
      function (callback) {
        if (!errors.isEmpty()) {
          let err = new createHttpErr.BadRequest(errors.array());
          err.headers = { error_code: -105 };
          callback(err, null);
        } else {
          callback();
        }
      }, // 2
      function (callback) {
        loadContractDocs(contract, callback);
      }, // 3
      function (callback) {
        saveContract(contract, callback);
      }, // 4
      function (callback) {
        addJobContract(contract, callback);
      },
    ], function (err, results) {
      if (err) {
        contractlog.statusCode = -1;
        contractlog.log = err.message;
        saveContractLog(contractlog, function (error, doc) {
          if (error) { return next(error); }
        });
        return next(err);
      }
      logger.info(`${req.body.contractId} - contract has been added`);
      res.status(201).json(results[3]);
    });
  }

];

// Display Author delete form on DELETE.
exports.api_contract_delete = function (req, res, next) {

  async.parallel({
    docs: function (callback) {
      deleteContractDocs(req.params.id, callback);
    },
    contract: function (callback) {
      Contract.deleteOne({ 'contractId': req.params.id }).exec(callback);
    },
    contractlog: function (callback) {
      updateContractLogStatus(req.params.id, 1, callback);
    },
  }, function (err, results) {
    if (err) { return next(err); }
    let { contract = {} } = results;
    logger.info(`${req.params.id} - contract has been deleted: `, { message: JSON.stringify(contract) });
    res.status(200).json(contract);
  });

};

// Resend contract.
exports.api_contract_resend = [

  param('id').isUUID(),
  // Process request after validation and sanitization.
  (req, res, next) => {

    validationReq(req);
    const contractId = req.params.id;

    resendContract(contractId, function (err, contract) {
      if (err) { return next(err); }
      res.status(200).json(contract);
    });

  }

];

// Resend contract.
exports.api_contracts_resubmit = function (req, res, next) {

  listContractLogResend(
    function (error, contracts) {
      if (error) { return next(error); }

      async.eachSeries(contracts, function (contract, callback) {
        resendContract(contract.contractId,  callback);
      }, function (err) {
        if (err) { return next(err); }
        res.status(200).json(contracts);
      });

    });

};