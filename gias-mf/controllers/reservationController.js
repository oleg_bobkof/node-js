const
  async = require("async"),
  { param, body, validationResult } = require('express-validator');

const
  logger = require('../utils/logger'),
  Reservation = require('../models/reservation'),
  { createHttpError } = require('../utils/errors'),
  { addJobReservationLT, addJobReservationCancelLT, addJobReservationReply } = require('../controllers/jobController');

const errorFormatter = ({ location, msg, param, value }) => {
  return `${location}[${param}]: ${value} - ${msg}`;
};

// Validation request
function validationReq(request) {
  // Extract the validation errors from a request.
  const errors = validationResult(request).formatWith(errorFormatter);
  if (!errors.isEmpty()) {
    let err = new Error(errors.array());
    throw createHttpError(400, err, -115);
  }

}

// Save reservation
function saveReservation(reservation, cb) {

  let { reservationId } = reservation;
  Reservation.findOne({ reservationId: reservationId }, function (err, doc) {
    if (err) { return cb(createHttpError(400, err, -102)); }
    if (!doc) {
      doc = reservation;
      doc.resendCount = 0;
    } else {
      doc.reservationId = reservation.reservationId;
      doc.statusCode = reservation.statusCode;
      doc.statusTimestamp = reservation.statusTimestamp;
      doc.reservation = reservation.reservation;
      doc.timestamp = reservation.timestamp;
    }
    doc.save(function (err, doc) {
      if (err) { return cb(createHttpError(400, err, -102)); }
      cb(null, doc);
    });
  });

}

// Update reservation Finance Sources
function updateReservationFS(reservation_obj, cb) {

  let { reservationId, reservation: { financeSources = [] } } = reservation_obj;
  let items = financeSources.filter(item => item.processingStatus !== undefined && item.processingStatus !== null);

  async.map(
    items,
    function (item, callback) {
      Reservation.updateOne(
        { reservationId: reservationId, 'reservation.financeSources._id': item._id },
        {
          $set: {
            'reservation.financeSources.$.isSufficientlyByASFR': item.isSufficientlyByASFR,
            'reservation.financeSources.$.ASFRdatetime': item.ASFRdatetime,
            'reservation.financeSources.$.processingStatus': item.processingStatus,
            'reservation.financeSources.$.reason': item.reason,
            'reservation.financeSources.$.restPlan': item.restPlan,
            'reservation.financeSources.$.reservCost': item.reservCost
          }
        },
        callback
      );
    },
    function (err, results) {
      if (err) { return cb(err); }
      cb(null, results);
    }
  );

}

// Update reservation status
function updateReservationStatus(reservationid, statusCode, cb) {

  Reservation.updateOne({ reservationId: reservationid },
    {
      statusCode: statusCode,
      statusTimestamp: Date.now()
    }, cb);

}

// Update reservation resend
function updateReservationResend(reservationid, cb) {

  Reservation.updateOne({ reservationId: reservationid },
    //{ $set: { resendAt: Date.now(), resendCount: 0 } },
    {
      $set:
        { resendAt: Date.now() }, $inc: { resendCount: 1 }
    },
  ).exec(cb);

}

// Process reservation
function processReservation(reservationid, cb) {

  Reservation.findOne({ reservationId: reservationid, statusCode: 0 }, function (err, reservation_obj) {
    if (err) { return cb(err); }
    if (!reservation_obj) { return cb(); }

    let { reservation: { reservationId, isDuty, financeSources = [] } } = reservation_obj;
    let fs = financeSources.find(item => item.processingStatus == undefined && item.processingStatus == null);
    if (fs) { return cb(); }

    // cancel FS
    const cancelFS = financeSources.filter(item => (!item.isSufficientlyByASFR));
    const errProcessing = cancelFS.find(item => (item.processingStatus === -1));

    async.series([
      // send reply
      function (callback) {
        if (isDuty) {
          logger.info(reservationId, { message: ' - reservation has been sent' });
          addJobReservationReply(reservation_obj, callback);
        } else {
          callback();
        }
      },
      // cancel reservation 
      function (callback) {
        const isCancel = isDuty ? (cancelFS.length > 0) : (cancelFS.length > 0) && (!errProcessing);
        if (isCancel) {
          logger.info(reservationId, { message: ' - reservation has been canceled' });
          addJobReservationCancelLT(reservation_obj, callback);
        } else {
          callback();
        }
      },
      // update reservation status 
      function (callback) {
        const statusCode = ((!isDuty) && (cancelFS.length > 0) && (errProcessing)) ? -1 : 1;
        updateReservationStatus(reservationid, statusCode, callback);
      }
    ],
      function (err, results) {
        if (err) { return cb(err); }
        cb(null, results);
      }
    );

  });

}

// Find reservation by ID
function findReservation(requestid, cb) {

  Reservation.findOne({ reservationId: requestid }, function (err, reservation) {
    if (err) {
      let err400 = createHttpError(400, err, -102);
      return cb(err400);
    }
    // No results.
    if (reservation == null) {
      let err404 = createHttpError(404, 'Запрос на резеревирование с таким идентификатором в АСФР не поступал.');
      return cb(err404);
    }

    cb(null, reservation);
  });

}

// Group by Finance Sources
function groupFinanceSources(reservation_obj, cb) {

  let { reservationId, reservation: { financeSources = [] } } = reservation_obj;
  Reservation.queryGroupFS(reservationId, function (err, items) {
    if (err) { return cb(err); }

    if (financeSources.length !== items.length) {
      financeSources.length = 0;
      items.map(function (item) {
        let fs = {
          _id: item.maxId,
          budgetCost: item.sumBudgetCost.toFixed(2),
          finYear: item._id.finYear,
          dSct: item._id.dSct,
          dSSct: item._id.dSSct,
          dKnd: item._id.dKnd,
          dPrgr: item._id.dPrgr,
          chp: item._id.chp,
          dPro: item._id.dPro,
          dSPro: item._id.dSPro,
          dCtg: item._id.dCtg,
          dArt: item._id.dArt,
          dSArt: item._id.dSArt,
          dItm: item._id.dItm,
          finId: item._id.finId,
          unk: item._id.unk,
          customerId: item.maxCustomerId,
          tkId: item._id.tkId,
          UNP: item.maxUNP
        };
        financeSources.push(fs);
      });
      saveReservation(reservation_obj, cb);
    } else {
      cb(null, reservation_obj);
    }
  });

}

// Resend reservation.
function listReservationResend(cb) {

  let dt = new Date();
  dt.setHours(dt.getHours() - 1);

  const query = Reservation.find({
    statusCode: 0,
    resendCount: { $lte: 4 } //$or: [{ resendCount: { $lte: 4 } }, { resendCount: null }]
  });

  query.where('timestamp').lt(dt).exec(cb);

}

// Resend reservation 
function resendReservation(reservation_obj, cb) {

  async.series([
    function (callback) {
      addJobReservationLT(reservation_obj, callback);
    },
    function (callback) {
      updateReservationResend(reservation_obj.reservationId, callback);
    },
  ],
    function (err, result) {
      if (err) { return cb(err); }
      cb(null, result[1]);
    }
  );

}

// Status reservation return on GET 
exports.api_reservation_status = [

  param('requestId').isUUID(),
  (req, res, next) => {
    validationReq(req);

    findReservation(req.params.requestId, function (err, reservation_item) {
      if (err) { return next(err); }
      let { reservation, statusCode } = reservation_item;
      if (statusCode > 0) {
        res.status(200).json(reservation);
      } else {
        res.status(202).send('Запрос на резервирование принят АСФР, но еще находится в обработке.');
      }
    });
  }

];

// Reservation create on POST.
exports.api_reservation_post = [

  // Validate fields.
  body('purchaseGiasId')
    .isUUID(),
  body('requestId')
    .isUUID(),
  body('isDuty')
    .isBoolean(),
  body('operation')
    .isIn([1, 2]),
  body('financeSources')
    .isArray(),
  // Process request after validation and sanitization.
  (req, res, next) => {

    validationReq(req);
    const requestid = req.body.requestId;

    Reservation.findOne({ reservationId: requestid }, function (err, reservation_obj) {
      if (err) { return next(err); }

      if (!reservation_obj) {
        reservation_obj = new Reservation({ reservationId: requestid, reservation: req.body });
      } else {
        if (reservation_obj.statusCode !== -1) {
          return res.status(200).json(reservation_obj.reservation);
        }
      }

      async.series([
        function (callback) {
          saveReservation(reservation_obj, callback);
        },
        function (callback) {
          groupFinanceSources(reservation_obj, callback);
        },
        function (callback) {
          addJobReservationLT(reservation_obj, callback);
        },
        function (callback) {
          updateReservationStatus(reservation_obj.reservationId, 0, callback);
        }
      ],
        function (err) {
          if (err) { return next(err); }
          logger.info(reservation_obj.reservationId, { message: ' - reservation has been added' });
          res.status(200).json(reservation_obj.reservation);
        }
      );
    });

  }

];

// Reservation update on PUT
exports.api_reservation_put = [

  // Validate fields.
  param('requestId')
    .isUUID(),
  body('purchaseGiasId')
    .isUUID(), //.withMessage('invalid value'),
  body('requestId')
    .isUUID(),
  body('financeSources')
    .isArray(),
  body('financeSources').custom((value, { req }) => {
    // Indicates the success of this synchronous custom validator
    return true;
  }),
  (req, res, next) => {

    validationReq(req);
    let reservation = new Reservation({ reservationId: req.body.requestId, reservation: req.body });
    let { reservationId } = reservation;

    async.series([
      function (callback) {
        findReservation(reservationId, callback);
      },
      function (callback) {
        updateReservationFS(reservation, callback);
      },
      function (callback) {
        processReservation(reservationId, callback);
      },
    ],
      function (err) {
        if (err) { return next(err); }
        logger.info(reservationId, { message: ' - reservation has been update FS' });
        res.status(200).json(reservation.reservation);
      }
    );

  }
];

//
exports.api_reservation_replay = [

  param('requestId').isUUID(),
  // Process request after validation and sanitization.
  (req, res, next) => {

    validationReq(req);
    const reservationId = req.params.requestId;

    async.series([
      function (callback) {
        findReservation(reservationId, callback);
      },
      function (callback) {
        processReservation(reservationId, callback);
      },
    ],
      function (err, result) {
        if (err) { return next(err); }
        res.status(200).json(result[0]);
      }
    );

  }

];


// Resend reservation
exports.api_reservation_resend = [

  param('requestId').isUUID(),
  // Process request after validation and sanitization.
  (req, res, next) => {

    validationReq(req);
    const reservationId = req.params.requestId;

    async.waterfall([
      function (callback) {
        findReservation(reservationId, callback);
      },
      function (reservation, callback) {
        resendReservation(reservation, function (err, job) {
          if (err) { return callback(err); }
          callback(err, reservation, job);
        });
      },
    ],
      function (err, reservation, job) {
        if (err) { return next(err); }
        res.status(200).json(reservation);
      }
    );

  }

];

// Check reservations
exports.api_reservations_resubmit = function (req, res, next) {

  listReservationResend(
    function (error, reservations) {
      if (error) { return next(error); }

      async.eachSeries(reservations, function (reservation, callback) {
        resendReservation(reservation, callback);
      }, function (err) {
        if (err) { return next(err); }
        res.status(200).json(reservations);
      });

    });

};