const ContractLog = require('../models/contractlog');
const { createHttpErr, createErr400, createErr500, createErr404 } = require('../utils/errors');


// Save contract log
exports.saveContractLog = function (contract, cb) {

  let contractId = contract.contractId;
  ContractLog.findOne({ contractId: contractId }, function (err, doc) {
    if (err) { return cb(createErr400(-102, err)); }
    if (!doc) {
      doc = contract;
      doc.resendCount = 0;
    } else {
      doc.contractId = contract.contractId;
      doc.statusCode = contract.statusCode;
      doc.contract = contract.contract;
      doc.timestamp = contract.timestamp;
      doc.log = contract.log;
    }
    doc.save(function (err, doc) {
      if (err) { return cb(createErr400(-102, err)); }
      cb(null, doc);
    });
  });

};

// Update ContractLog status
exports.updateContractLogStatus = function (contractId, statusCode, cb) {

  ContractLog.updateOne({ 'contractId': contractId }, { statusCode: statusCode }).exec(cb);

};

// Update ContractLog resend
exports.updateContractLogResend = function (contractId, cb) {

  ContractLog.updateOne(
    { 'contractId': contractId },
    //{ $set: { resendAt: Date.now(), resendCount: 0 } },
    { $set: { resendAt: Date.now() }, $inc: { resendCount: 1 } },
  ).exec(cb);

};

// Resend contract.
exports.listContractLogResend = function (cb) {

  let dt = new Date();
  dt.setHours(dt.getHours() - 1);

  const query = ContractLog.find({
    statusCode: 0,
    resendCount: { $lte: 4 } //$or: [{ resendCount: { $lte: 4 } }, { resendCount: null }]
  }).select('contractId');

  query
    .where('timestamp').lt(dt)
    .exec(cb);

};