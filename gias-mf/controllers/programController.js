'use strict';

const url = require('url');
const { createHttpErr, createErr400 } = require('../utils/errors');
const Program = require('../models/program');

// --
const getProgram = (item) => (
  {
    classId: item._id,
    finYear: item.finYear,
    dPro: item.dPro,
    dSPro: item.dSPro,
    name: item.name,
    inputDateTime: item.inputDateTime
  }
);

// GET list of all Program by year.
exports.api_program_list = function (req, res, next) {

  let url_parts = url.parse(req.url, true);
  let { year = new Date().getFullYear() } = req.query;
  let qparams = { finYear: year };
  
  Program.find(qparams)
    .exec(function (err, list_programs) {
      if (err) { return next(createErr400(-102, err)); }
      if (list_programs.length == 0) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.search);
        return next(error);
      }
      
      let programs = list_programs.map(program_item => getProgram(program_item));
      res.status(200).json(programs);
    });

};

// GET Program by Id.
exports.api_program_item = function (req, res, next) {

  let url_parts = url.parse(req.url, true);

  Program.findById(req.params.id)
    .populate('program')
    .exec(function (err, program_item) {
      if (err) { return next(createErr400(-102, err)); }
      if (program_item == null) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.path);
        return next(error);
      }

      let program = getProgram(program_item);
      res.status(200).send(program);
    });

};