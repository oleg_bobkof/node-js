const
  async = require("async"),
  axios = require("axios");

const
  config = require('../config/configure'),
  logger = require('../utils/logger'),
  { createErr400, createErr500, createErr404 } = require('../utils/errors');

const
  JOB_URL_NOW = '/api/job/now',
  JOB_URL_ONCE = '/api/job/once';

const frmOptions = (options) => `job [${options.data.name}]: ${options.method} ${options.baseURL}${options.url} `;
const getFinId = (finId) => ((finId % 100 == 0) || (finId >= 90000)) ? finId : Math.floor(finId / 100);
const uniqueArray = (arr) => [...new Set(arr.map(o => JSON.stringify(o)))].map(s => JSON.parse(s));
const groupFinanceSources = (fs) => uniqueArray(fs.map(f => ({ tkId: f.tkId || 0, finId: getFinId(f.finId) || 0 })));

// Make http request
function makeRequest(options, cb) {

  axios(options)
    .then(function (response) {
      logger.info(frmOptions(options));
      cb(null, response.data);
    })
    .catch(function (error) {
      logger.error(frmOptions(options), { message: error.message });
      if (error.response) {
        cb(createErr404(error.response));
      } else if (error.request) {
        cb(createErr400(400, error));
      } else {
        cb(createErr500(error));
      }
    });

}

// Apply Rules
function applyRules(object_fs) {

  let rules = [];
  const { filters = [] } = config.gias_scheduler;
  const fs_list = groupFinanceSources(object_fs);

  for (const item of fs_list) {
    const filter = filters.find(function (f) {
      let { tkId, finId } = f.filter.rule.financeSource;
      let chkTKId = (tkId) ? item.tkId.toString().startsWith(tkId) : true;
      let chkFinId = (finId) ? item.finId.toString().startsWith(finId) : true;
      return chkTKId && chkFinId;
    });

    if (filter) {
      if (rules.indexOf(filter) === -1) {
        rules.push(filter);
      }
    }
  }
  return rules;

}

// GET all jobs
exports.getJobs = function (callback) {

  const { job } = config.gias_scheduler.requests;
  const options = JSON.parse(JSON.stringify(job));
  options.method = 'GET';
  options.data = {};
  options.url = '/api/job';
  makeRequest(options, callback);

};

// Contract job
exports.addJobContract = function (contract, cb) {

  const { job } = config.gias_scheduler.requests;
  const { financeSource = [] } = contract.contract;
  const rules = applyRules(financeSource);

  async.eachSeries(rules, function (rule, callback) {
    let { filter: { jobName } } = rule;
    let options = JSON.parse(JSON.stringify(job));
    options.data = options.data || {};
    options.data.name = jobName.replace('[name]', 'contract');
    options.data.data = {
      headers: "",
      params: "",
      query: "",
      body: contract.contract
    };
    logger.info( `contract [${contract.contract.contractId}]`);
    makeRequest(options, callback);
  }, function (err) {
    if (err) {
      cb(err);
    } else {
      cb(null, '');
    }
  });

};

// Reservations job
exports.addJobReservationLT = function (reservation_item, cb) {

  const { job } = config.gias_scheduler.requests;
  const { reservation, reservation: { financeSources = [] } } = reservation_item;
  const rules = applyRules(financeSources);
  let interval = 5;

  async.eachOfSeries(rules, function (rule, index, callback) {
    let { filter: { jobName } } = rule;
    let options = JSON.parse(JSON.stringify(job));
    options.data = options.data || {};
    options.url = JOB_URL_ONCE;
    options.data.interval = `in ${interval} seconds`;
    options.data.name = jobName.replace('[name]', 'reservation');
    options.data.data = {
      headers: "",
      params: "",
      query: "",
      body: reservation
    };
    interval++;
    makeRequest(options, callback);
  }, function (err) {
    if (err) {
      cb(err);
    } else {
      cb(null, '');
    }
  });

};

// Reservation cancel jobs
exports.addJobReservationCancelLT = function (reservation_item, cb) {

  const { job } = config.gias_scheduler.requests;
  const { reservation: { requestId, financeSources = [] } } = reservation_item;
  const rules = applyRules(financeSources);
  let interval = 45;

  async.eachSeries(rules, function (rule, callback) {
    let { filter: { jobName } } = rule;
    let options = JSON.parse(JSON.stringify(job));
    options.data = options.data || {};
    options.data.interval = `in ${interval} seconds`;
    options.url = JOB_URL_ONCE;
    options.data.name = jobName.replace('[name]', 'reservation_cancel');
    options.data.data = {
      headers: "",
      params: JSON.stringify({ reservationid: requestId }),
      query: "",
      body: {}
    };
    interval++;
    makeRequest(options, callback);
  }, function (err) {
    if (err) { return cb(err); }
    cb(null, '');
  });

};

//  Reservation Reply job
exports.addJobReservationReply = function (reservation_item, cb) {

  const { reservation, reservation: { requestId } } = reservation_item;
  const { job } = config.gias_scheduler.requests;
  const options = JSON.parse(JSON.stringify(job));
  options.data = options.data || {};
  options.url = JOB_URL_NOW;
  options.data.name = 'send_reservation_reply';
  options.data.data = {
    headers: "",
    params: JSON.stringify({ requestId: requestId }),
    query: "",
    body: reservation
  };
  makeRequest(options, cb);

};

// 
exports.addJobNotification = function (notification, cb) {

  const { objectIDs = [] } = notification;
  const { job } = config.gias_scheduler.requests;
  const options = JSON.parse(JSON.stringify(job));
  options.data = options.data || {};

  async.eachSeries(objectIDs, function (id, callback) {
    options.data.data = {
      headers: "",
      params: JSON.stringify({ contractid: id }),
      query: "",
      body: {}
    };
    makeRequest(options, callback);
  }, function (err) {
    if (err) {
      cb(err);
    } else {
      cb(null, '');
    }
  });

};

//  ContractExec Send job
exports.addJobContractExecSend = function (contractExec_item, cb) {

  const { contractExec, contractExec: { contractId } } = contractExec_item;
  const { job } = config.gias_scheduler.requests;
  const options = JSON.parse(JSON.stringify(job));
  options.data = options.data || {};
  options.url = JOB_URL_NOW;
  options.data.name = 'send_contract_execution';
  options.data.data = {
    headers: "",
    params: JSON.stringify({ contractId: contractId }),
    query: "",
    body: contractExec
  };
  makeRequest(options, cb);

};