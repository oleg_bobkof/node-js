'use strict';

const url = require('url');
const { createHttpErr, createErr400 } = require('../utils/errors');
const Budget = require('../models/budget');

// --
const getBudget = (item) => (
  {
    budgetId: item._id,
    finYear: item.finYear,
    finId: item.finId,
    finIdParent: item.finIdParent,
    nameBudget: item.nameBudget,
    inputDateTime: item.inputDateTime
  }
);

// Get list of all Budget by year.
exports.api_budget_list = function (req, res, next) {

  let url_parts = url.parse(req.url, true);
  let { year = new Date().getFullYear() } = req.query;
  let qparams = { finYear: year };

  Budget.find(qparams)
    .exec(function (err, list_budgets) {
      if (err) { return next(createErr400(-102, err)); }
      if (list_budgets.length == 0) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.search);
        return next(error);
      }

      let budgets = list_budgets.map(budget_item => getBudget(budget_item));
      res.status(200).json(budgets);
    });

};

// Get Budget item by id.
exports.api_budget_item = function (req, res, next) {

  let url_parts = url.parse(req.url, true);

  Budget.findById(req.params.id)
    .populate('budget')
    .exec(function (err, budget_item) {
      if (err) { return next(createErr400(-102, err)); }
      if (budget_item == null) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.path);
        return next(error);
      }

      let budget = getBudget(budget_item);
      res.status(200).send(budget);
    });

};

/* // Display list of all Department.
exports.budget_list = function (req, res, next) {
    const title = 'Бюджеты';

    var perPage = 20
        , page = req.query.page > 0 ? req.query.page : 1

    Budget.find()
        .limit(perPage)
        // .sort([['chp', 'ascending']])
        .skip(perPage * (page - 1))
        .exec(function (err, list_budgets) {
            if (err) { return next(err); }
            // Successful, so render.
            Budget.count().exec(function (err, count) {
                res.render('budget_list', {
                    title: title
                    , list_budgets: list_budgets
                    , page: page
                    , pages: count / perPage
                })
            })
        });
}; */