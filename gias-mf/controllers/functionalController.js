'use strict';

const url = require('url');
const { createHttpErr, createErr400 } = require('../utils/errors');
const Functional = require('../models/functional');

// --
const getFunctional = (item) => (
  {
    classId: item._id,
    finYear: item.finYear,
    dSct: item.dSct,
    dSSct: item.dSSct,
    dKnd: item.dKnd,
    dPrgr: item.dPrgr,
    name: item.name,
    inputDateTime: item.inputDateTime
  }
);

// GET list of all Functional by year.
exports.api_functional_list = function (req, res, next) {

  let url_parts = url.parse(req.url, true);
  let { year = new Date().getFullYear() } = req.query;
  let qparams = { finYear: year };
  
  Functional.find(qparams)
    .exec(function (err, list_functionals) {
      if (err) { return next(createErr400(-102, err)); }
      if (list_functionals.length == 0) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.search);
        return next(error);
      }
      let functionals = list_functionals.map(functional_item => getFunctional(functional_item));
      res.status(200).json(functionals);
    });

};

// GET Functional by Id
exports.api_functional_item = function (req, res, next) {

  let url_parts = url.parse(req.url, true);

  Functional.findById(req.params.id)
    .populate('functional')
    .exec(function (err, functional_item) {
      if (err) { return next(createErr400(-102, err)); }
      if (functional_item == null) {
        let error = new createHttpErr.NotFound('По заданным параметрам данные отсутствуют: ' + url_parts.path);
        return next(error);
      }
      let functional = getFunctional(functional_item);
      res.status(200).send(functional);
    });

};
