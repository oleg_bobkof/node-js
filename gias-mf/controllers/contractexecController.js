const
  async = require("async"),
  logger = require('../utils/logger'),
  { param, body, validationResult } = require('express-validator');

const
  ContractExec = require('../models/contractexec'),
  { createHttpErr, createErr400, createErr404 } = require('../utils/errors'),
  { addJobContractExecSend } = require('../controllers/jobController');

const errorFormatter = ({ location, msg, param, value }) => {
  return `${location}[${param}]: ${value} - ${msg}`;
};

// Validation request
function validationReq(request) {
  // Extract the validation errors from a request.
  const errors = validationResult(request).formatWith(errorFormatter);
  if (!errors.isEmpty()) {
    let err = new createHttpErr.BadRequest(errors.array());
    err.headers = { error_code: -115 };
    throw err;
  }

}

// Save contractexec
function saveContractExec(contractexec_obj, cb) {

  contractexec_obj.save(function (err, doc) {
    if (err) { return cb(createErr400(-102, err)); }
    cb(null, doc);
  });

}

// Update reservation status
function updateContractExecStatus(contractExecId, statusCode, cb) {

  ContractExec.updateOne({ contractExecId: contractExecId },
    {
      statusCode: statusCode,
      statusTimestamp: Date.now()
    }, cb);

}

// ContractExec create on POST.
exports.api_contractexec_post = [

  // Validate fields.
  body('requestId')
    .isUUID(),
  body('contractId')
    .isUUID(),
  body('PaymentDocs')
    .isArray(),

  (req, res, next) => {
    validationReq(req);
    const requestid = req.body.requestId;

    ContractExec.findOne({ contractExecId: requestid }, function (err, contractExec_obj) {
      if (err) { return next(err); }

      if (!contractExec_obj) {
        contractExec_obj = new ContractExec({ contractExecId: requestid, contractExec: req.body });
      } else {
        if (contractExec_obj.statusCode !== -1) {
          return res.status(200).json(contractExec_obj.contractExec);
        }
      }

      async.series([
        function (callback) {
          saveContractExec(contractExec_obj, callback);
        },
        function (callback) {
          addJobContractExecSend(contractExec_obj, callback);
        },
        function (callback) {
          updateContractExecStatus(contractExec_obj.contractExecId, 0, callback);
        }
      ],
        function (err) {
          if (err) { return next(err); }
          logger.info(contractExec_obj.contractExecId, { message: ' - contractExec has been added/sent' });
          res.status(200).json(contractExec_obj.contractExec);
        }
      );
    });

  }

];