'use strict';

const
  async = require("async"),
  axios = require("axios"),
  _ = require('lodash');

var { createHttpErr, createErr400, createErr500, createErr404 } = require('../utils/errors');

// Constans
const arrPackMethod = exports.arrPackMethod = ['CREATE', 'UPDATE', 'DELETE'];
const arrDic = exports.arrDic = ['contract'];

// Make http request
exports.makeRequest = function (options, cb) {

  axios(options)
    .then(function (response) {
      cb(null, response.data);
    })
    .catch(function (error) {
      if (error.response) {
        cb(createErr404(error.response));
      } else if (error.request) {
        cb(createErr400(400, error.request));
      } else {
        cb(createErr500(error));
      }
    });

};

function genrows(groups, groupKey) {
  return _.toPairs(groups)
    .map(([key, data]) => ({ [groupKey]: key, data }))
}

function gengroups(arr, iteratee, key) {
  const grouped = _.groupBy(arr, iteratee)
  return genrows(grouped, key)
}

/**
 * @typedef {Object} GroupByProp
 * @prop {String=} key Grouping key
 * @prop {String|Function} iteratee An iteratee for Lodash's groupBy
 */

/**
 * Group collection's data by multiple iteratees
 * @param data
 * @param {Array<String|GroupByProp>} props Array of group by objects or property names
 *   This parameter also can contain both property names and group by objects together
 * @returns {Array}
 */

exports.grouparray = function (data, props) {
  let result = [{ data }]

  props.forEach((prop, i) => {
    const key = prop.key || `k${i + 1}`
    const iteratee = prop.iteratee || prop

    result = _.flatten(result.map(row => {
      return gengroups(row.data, iteratee, key)
        .map(group => Object.assign({}, row, {
          [key]: group[key],
          data: group.data
        }))
    }))

  })

  return _.flatten(result)
};