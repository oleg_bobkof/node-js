const createError = require('http-errors');
const logger = require('../utils/logger');

// Error Data
const ErrorData = function (err) {
  this.errorCode = (err.headers) ? err.headers.error_code : err.statusCode;
  this.AdditionalData = {};
  this.AdditionalData.timestamp = Date.now();
  this.AdditionalData.status = err.statusCode;
  this.AdditionalData.error = err.name;
  this.AdditionalData.message = err.message;
};

// Http Error
exports.createHttpErr = createError;
exports.createHttpError = function (status, err, error_code = null) {
  let httpError = createError(status, err);
  if (error_code) {
    httpError.headers = { error_code: error_code };
  }
  return httpError;
};

// Error 500
const createErr500 = exports.createErr500 = function (error) {
  let err = createError(500, error);
  err.headers = { error_code: 500 };
  return err;
};

// Error 404
exports.createErr404 = function (error) {
  let err = createError(400, error);
  err.headers = { error_code: 400 };
  return err;
};

// Error 400
exports.createErr400 = function (code, error) {
  let err = createError(400, error);
  err.headers = { error_code: code };
  return err;
};

// Error handler
exports.errorHandler = function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error data
  if (!err.status) {
    err = createErr500(err);
  }

  let error = new ErrorData(err);
  logger.error(JSON.stringify(error));
  res.status(err.status).json(error);
};