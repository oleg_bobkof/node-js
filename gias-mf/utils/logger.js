const winston = require('winston');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, colorize, prettyPrint, printf, json } = format;

// Ignore log messages if they have { private: true }
const ignorePrivate = format((info, opts) => {
  if (info.private) { return false; }
  return info;
});

// Format message
const myFormat = printf(info => {
  return `[${info.timestamp}] [${info.level}] => ${info.message}`;
});

const container = new winston.Container();

// -- 
container.add('loggerJSON', {
  format: combine(
    label({ label: 'logger JSON' }),
    prettyPrint(),
    timestamp(),
    colorize(),
    json()
  ),
  transports: [new transports.Console()]
});

container.add('logger', {
  format: combine(
    label({ label: 'logger' }),
    //prettyPrint(),
    timestamp(),
    colorize(),
    myFormat
  ),
  transports: [new transports.Console()]
});


//
/*
const logger = createLogger({
  format: combine(
    label({ label: 'right meow!' }),
    timestamp(),
    colorize(),
    myFormat,
  ),
  transports: [new transports.Console()]
});
*/

module.exports = container.get('logger');