'use strict';

const { Schema, model } = require('mongoose');

// ContractLog
const ContractLogSchema = new Schema({
  contractId: { type: String, required: true },
  statusCode: { type: Number, required: true, default: 0 },
  statusAt: { type: Date, default: Date.now },
  resendCount: { type: Number },
  resendAt: { type: Date },
  contract: {},
  timestamp: { type: Date, default: Date.now },
  log: { type: Object }
});

// Index
ContractLogSchema
  .index({ contractId: 1 }, { unique: true });

// Export model.
module.exports = model('contract_log', ContractLogSchema);