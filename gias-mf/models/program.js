'use strict';

const { Schema, model } = require('mongoose');

// программная классификация
const ProgramSchema = new Schema({
  finYear: { type: Number, required: true, min: 2000, max: 2100 },
  dPro: { type: Number, required: true, min: 0 },
  dSPro: { type: Number, required: true, min: 0 },
  name: { type: String, required: true, max: 255 },
  inputDateTime: { type: Date, default: Date.now }
});

// Unique index
ProgramSchema
  .index({ finYear: 1, dPro: 1, dSPro: 1 }, { unique: true });

// Export model.
module.exports = model('Program', ProgramSchema);