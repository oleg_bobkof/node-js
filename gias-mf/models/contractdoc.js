'use strict';

const { Schema, model } = require('mongoose');

// Contract docs
const ContractDocSchema = new Schema({
  contractId: { type: String, required: true },
  file_id: { type: Schema.ObjectId, required: true },
  file_type: { type: Number },
  file_name: { type: String },
  file_link: { type: String }
});

// Index
ContractDocSchema
  .index({ contractId: 1, file_id: 1 }, { unique: true });

// Export model.
module.exports = model('contracts.doc', ContractDocSchema);