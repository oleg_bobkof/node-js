'use strict';

const { Schema, model } = require('mongoose');

// справочник НСИ - Бюджеты
const BudgetSchema = new Schema({
  finYear: { type: Number, required: true, min: 2000, max: 2100 },
  finId: { type: Number, required: true, min: 0 },
  finIdParent: { type: Number, required: true, min: 0 },
  nameBudget: { type: String, required: true, max: 255 },
  inputDateTime: { type: Date, default: Date.now }
});

// Index
BudgetSchema
  .index({ finYear: 1, finId: 1, finIdParent: 1 }, { unique: true });

// Export model.
module.exports = model('Budget', BudgetSchema);