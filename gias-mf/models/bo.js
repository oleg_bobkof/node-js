'use strict';

const { Schema, model } = require('mongoose');

// справочник БО
const BOSchema = new Schema({
  finYear: { type: Number, required: true, min: 2000, max: 2100 },
  finId: { type: Number, required: true, min: 10000 },
  unp: { type: String, required: true, minLength: 9, maxLength: 9 },
  unk: { type: Number, required: true, min: 0 },
  treasuryCode: { type: Number, required: true, min: -1 },
  name: { type: String, required: true, max: 255 },
  inputDateTime: { type: Date, default: Date.now }
});

// Export model.
module.exports = model('BO', BOSchema);