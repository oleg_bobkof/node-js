'use strict';

const { Schema, model } = require('mongoose');

// экономическая классификация
const EconomicSchema = new Schema({
  finYear: { type: Number, required: true, min: 2000, max: 2100 },
  dCtg: { type: Number, required: true, min: 0 },
  dArt: { type: Number, required: true, min: 0 },
  dSArt: { type: Number, required: true, min: 0 },
  dItm: { type: Number, required: true, min: 0 },
  name: { type: String, required: true, max: 255 },
  inputDateTime: { type: Date, default: Date.now }
});

// Index
EconomicSchema
  .index({ finYear: 1, dCtg: 1, dArt: 1, dSArt: 1, dItm: 1 }, { unique: true });

// Export model.
module.exports = model('Economic', EconomicSchema);