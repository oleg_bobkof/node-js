'use strict';

const { Schema, model } = require('mongoose');

// Contract Item
const ContractItem = new Schema({
  budgetCost: { type: Number, min: 0, required: true },
  fundCost: { type: Number, required: true },
  innerCost: { type: Number, required: true },
  finYear: { type: Number, min: 2019, max: 2100 },
  dSct: { type: Number, min: 0 },
  dSSct: { type: Number, min: 0 },
  dKnd: { type: Number, min: 0 },
  dPrgr: { type: Number, min: 0 },
  chp: { type: Number, min: 0 },
  dPro: { type: Number, min: 0 },
  dSPro: { type: Number, min: 0 },
  dCtg: { type: Number, min: 0 },
  dArt: { type: Number, min: 0 },
  dSArt: { type: Number, min: 0 },
  dItm: { type: Number, min: 0 },
  finId: { type: Number, min: 0 },
  unk: { type: Number, min: 0 },
  customerId: { type: String },
  tkId: { type: Number, min: -1 }
}, { _id: false });

// Contract Payment
const ContractPayment = new Schema({
  type: { type: Number, enum: [1, 2] },
  date: { type: Number },
  sum: { type: Number },
  financeSource: [ContractItem]
}, { _id: false });

// Contract Account
const ContractAccount = new Schema({
  accountProvider: { type: String, maxlength: 50 },
  bankProviderCode: { type: String, maxlength: 50 },
  bankProviderName: { type: String },
  accountProviderCurrencyCode: { type: Number }
}, { _id: false });

// Contract
const ContractSchema = new Schema({
  contractId: { type: String, required: true },
  statusCode: { type: Number, required: true, default: 0 },
  contract: {
    // ContractFlexData
    contractHistoryId: { type: String },
    contractHistoryDate: { type: Number },
    titleContract: { type: String },
    contractNum: { type: String },
    contractDate: { type: Number },
    contractPrice: { type: Number },
    contractPriceCurrencyCode: { type: String },
    executionTerm: { type: Number },
    realExecutionTerm: { type: Number },
    terminationExecutionTerm: { type: Number },
    terminationReason: { type: String },
    titleProvider: { type: String },
    addressProvider: { type: String },
    contractAccounts: [ContractAccount],
    payments: [ContractPayment],
    contractPositions: {},
    financeSource: [ContractItem],
    // ContractFixedData
    contractId: { type: String, required: true },
    etsId: { type: String, required: true },
    baseContractId: { type: String },
    planNumber: { type: String },
    purchaseIDs: {}, // [],
    unpCustomer: { type: String, minlength: 9, maxlength: 9, required: true },
    okogu: { type: String },
    unpProvider: { type: String, minlength: 9, maxlength: 9 },
    sellerOtherId: { type: String },
    countryProvider: { type: String },
    contractDocuments: {},
  },
  timestamp: { type: Date, default: Date.now }
});

// Index
ContractSchema
  .index({ contractId: 1 }, { unique: true });

// Export model.
module.exports = model('contract', ContractSchema);