'use strict';

const { Schema, model } = require('mongoose');

// функциональная классификация
const FunctionalSchema = new Schema({
  finYear: { type: Number, required: true, min: 2000, max: 2100 },
  dSct: { type: Number, required: true, min: 0 },
  dSSct: { type: Number, required: true, min: 0 },
  dKnd: { type: Number, required: true, min: 0 },
  dPrgr: { type: Number, required: true, min: 0 },
  name: { type: String, required: true, max: 255 },
  inputDateTime: { type: Date, default: Date.now }
});

// Index
FunctionalSchema
  .index({ finYear: 1, dSct: 1, dSSct: 1, dKnd: 1, dPrgr: 1 }, { unique: true });

// Export model.
module.exports = model('Functional', FunctionalSchema);