'use strict';

const { Schema, model } = require('mongoose');
const { arrPackMethod, arrDic } = require('../utils/common');

// GIAS Notifications
const NotificationSchema = new Schema({
  dictionary: { type: String, required: true, enum: arrDic },
  operation: { type: String, required: true, enum: arrPackMethod },
  objectIDs: [],
  operationDate: { type: Date, default: Date.now }
});

// Export model.
module.exports = model('notification_ext', NotificationSchema);