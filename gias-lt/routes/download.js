'use strict';

const
  express = require('express'),
  path = require('path'),
  fs = require('fs'),
  router = express.Router();

const { createErr404 } = require('../utils/errors');

router.get('/:id/contract/:fileName', function (req, res, next) {
  var id = req.params.id;
  var dir = path.join(__dirname, '/../_uploads/contractdocs/', id);
  var fileName = path.join(dir, req.params.fileName);
  fs.access(fileName, fs.F_OK, (err) => {
    if (err) { return next(createErr404(err)); }
    res.sendFile(fileName);
  });
});

module.exports = router;