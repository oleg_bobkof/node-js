const
  express = require('express'),
  router = express.Router(),
  contract_controller = require('../controllers/contractController'),
  contractexec_controller = require('../controllers/contractexecController');

// -- Routes
router.post('/contract', contract_controller.api_contract_post);
router.get('/contract/:contractid/save', contract_controller.api_contract_save);

//--
router.get('/contractexecs/process', contractexec_controller.api_contractexecs_get);
router.post('/contractexecs/process/template', contractexec_controller.api_contractexecs_post);
router.post('/contractexec/prepare', contractexec_controller.api_contractexec_prepare_post);

module.exports = router;