const 
  express = require('express'),
  router = express.Router(),
  reservation_controller = require('../controllers/reservationController');
        
// Add reservation
router.post('/reservation', reservation_controller.api_reservation_post);

// Update reservation
router.put('/reservation/:reservationid/cancel', reservation_controller.api_reservation_put);

// Save to DB
router.get('/reservation/:reservationid/save', reservation_controller.api_reservation_save);

// GET reservation
router.get('/reservation/:reservationid', reservation_controller.api_reservation_status);

module.exports = router;