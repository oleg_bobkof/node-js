'use strict';

var express = require('express'),
    swaggerUi = require('swagger-ui-express'),
    YAML = require('yamljs'),
    path = require('path'),
    router = express.Router();

var swaggerDocument = YAML.load(path.join(__dirname, '../api/') + 'asfr-lt.yaml');

var options = {
    explorer: true,
    customCss: '.swagger-ui .topbar { display: none }'
};

/* GET swagger UI */
router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(swaggerDocument, options));

module.exports = router;