const
  pjson = require('../package.json'),
  express = require('express'),
  router = express.Router();

// Routes
router.use('/api-docs', require('./api-docs'));
router.use('/asfr', require('./contracts'));
router.use('/asfr', require('./reservations'));
router.use('/asfr/download', require('./download'));

// GET home page.
router.get('/', function (req, res) {
  res.redirect('/api-docs');
});

// GET ping -> pong
router.get('/ping', function (req, res) {
  res.status(200).json({
    msg: 'pong',
    name: pjson.name,
    version: pjson.version,
  });
});

module.exports = router;
