var express = require('express'),
    path = require('path'),
    cookieParser = require('cookie-parser'),
    logger = require('morgan'),
    compression = require('compression'),
    helmet = require('helmet');

var { errorHandler, createHttpErr } = require('./utils/errors');
var config = require('./config/configure');
var app = express();

app.disable('etag');
var debug = config.webserver.debug;
app.use(logger(debug));
app.use(express.json({ limit: "5mb" }));
app.use(express.urlencoded({ limit: "5mb", extended: false }));
app.use(cookieParser());
app.use(helmet());
// Compress all routes
app.use(compression());

app.use(express.static(path.join(__dirname, 'uploads')));
app.use(require('./routes'));

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
    var error = new createHttpErr.NotFound('The requested URL [' + req.path + '] was not found');
    next(error);
});

// Error handler
app.use(errorHandler);

module.exports = app;
