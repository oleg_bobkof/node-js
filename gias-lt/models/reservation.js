'use strict';

const { Schema, model } = require('mongoose');

// Finance Source
const FinanceSource = new Schema({
  budgetCost: { type: Number, min: 0, required: true },
  finYear: { type: Number, min: 2019, max: 2100, required: true },
  dSct: { type: Number, min: 0, required: true },
  dSSct: { type: Number, min: 0, required: true },
  dKnd: { type: Number, min: 0, required: true },
  dPrgr: { type: Number, min: 0, required: true },
  chp: { type: Number, min: 0, required: true },
  dPro: { type: Number, min: 0, required: true },
  dSPro: { type: Number, min: 0, required: true },
  dCtg: { type: Number, min: 0, required: true },
  dArt: { type: Number, min: 0, required: true },
  dSArt: { type: Number, min: 0, required: true },
  dItm: { type: Number, min: 0, required: true },
  finId: { type: Number, min: 10000, max: 99999, required: true },
  unk: { type: Number, min: 0, required: true },
  customerId: { type: String },
  tkId: { type: Number, min: 100, max: 799, required: true },
  UNP: { type: String, minlength: 9, maxlength: 9 },
  isSufficientlyByASFR: { type: Number, enum: [0, 1, 2] }, // признак достаточности ассигнований в АСФР (в ответе от АСФР)  0 - аналог false 1 - аналог true 2 - средства будущих периодов
  ASFRdatetime: { type: Number },
  reason: { type: String },
  processingStatus: { type: Number, enum: [-1, 0, 1] }, // статус обработки -1 - ошибочный 0 - отправлен 1 - обработан 
  restPlan: { type: Number },  // ассигнования свободные от договорных
  reservCost: { type: Number } // сумма зарезервированных средств
}); //, { _id: false });

// Reservation
const ReservationSchema = new Schema({
  reservationId: { type: String, required: true, unique: true },
  statusCode: { type: Number, required: true, default: -1 },
  statusTimestamp: { type: Date, default: Date.now },
  reservation: {
    purchaseGiasId: { type: String, required: true },
    requestId: { type: String, required: true },
    etpId: { type: String, required: true },
    operation: { type: Number, enum: [1, 2], required: true }, // признак операции 1 – резервирование 2 – снятие остатков резервирования
    isDuty: { type: Boolean, required: true }, // Признак необходимости ожидания ответа от АСФР
    financeSources: [FinanceSource]
  },
  timestamp: { type: Date, default: Date.now }
});

// Index
ReservationSchema
  .index({ reservationId: 1 }, { unique: true });

// Query Helpers
ReservationSchema.query
  .byStatus = function (year, status) {
    return this
      .where({ finYear: year, updStatus: status })
      .select('_id');
  };

// Export model.
module.exports = model('reservation', ReservationSchema);