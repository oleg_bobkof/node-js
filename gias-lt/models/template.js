const { Schema, model } = require('mongoose');

// Filter
const FilterSchema = new Schema({
  tkId: { type: Number, min: 100, max: 900, required: true },
  finId: { type: Number, min: 100, max: 99999, required: true }
});

// Index
//FilterSchema
// .index({ tkId: 1, finId: 1 }, { unique: true });

// Template
const TemplateSchema = new Schema({
  tkId: { type: Number, min: 100, max: 799 },
  connectionId: { type: String, uppercase: true, required: true },
  connectionCheck: { type: Boolean, default: false },
  dbconfig: {
    server: { type: String, uppercase: true, required: true }, // Server to connect to. You can use 'localhost\instance' to connect to named instance.
    database: { type: String, uppercase: true, required: true }, // Database to connect to (default: dependent on server configuration).
    port: { type: Number, min: 0 }, // Port to connect to (default: 1433). Don't set when connecting to named instance.
    domain: { type: String }, // Once you set domain, driver will connect to SQL Server using domain login.
    user: { type: String, required: true }, // user name to use for authentication.    
    password: { // Password to use for authentication.
      type: String,
      trim: true,
      set: v => Buffer.from(v).toString('base64'),
      get: v => Buffer.from(v, 'base64').toString('ascii'),
      required: true
    },
    connectionTimeout: { type: Number }, // connection timeout in ms (default: 15000).
    requestTimeout: { type: Number }, // request timeout in ms (default: 15000). NOTE: msnodesqlv8 driver doesn't support timeouts < 1 second. When passed via connection string, the key must be request timeout
    stream: { type: Boolean }, //- Stream recordsets/rows instead of returning them all at once as an argument of callback (default: false) Always set to true if you plan to work with large amount of rows.
    pool: {},
    options: {}
  },
  filters: [FilterSchema],
  timestamp: { type: Date, default: Date.now }
});

// Index
TemplateSchema
  .index({ connectionId: 1 }, { unique: true });

// Statics methods
TemplateSchema.statics
  .queryFS = function (callback) {
    return this
      .aggregate([
        { $match: { connectionCheck: true } },
        { $unwind: '$filters' },
        { $group: { _id: { tkId: '$filters.tkId', finId: '$filters.finId' } } },
        { $sort: { "_id.tkId": 1 } }
      ])
      .exec(callback);
  };

// Export model.
module.exports = model('template', TemplateSchema);