'use strict';

const { Schema, model } = require('mongoose');

// Payment Document
const PaymentDoc = new Schema({
  payType: { type: Number, enum: [1, 2], required: true }, // признак платежа 1- Предоплата, 2 - По факту
  docNumber: { type: String, required: true },
  bankDate: { type: Number, required: true },
  paymentCost: {
    budgetCost: { type: Number, required: true },
    tkId: { type: Number, min: 100, max: 799, required: true },
    finYear: { type: Number, min: 2019, max: 2100, required: true },
    finId: { type: Number, min: 10000, max: 99999, required: true },
  }
}); //, { _id: false });

// Reservation
const ContractExecutionSchema = new Schema({
  contractExecId: { type: String, required: true, unique: true },
  statusCode: { type: Number, required: true, default: -1 },
  statusTimestamp: { type: Date, default: Date.now },
  contractExec: {
    requestId: { type: String, required: true },
    contractId: { type: String, required: true },
    startDate: { type: Number, required: true },
    endDate: { type: Number, required: true },
    PaymentDocs: [PaymentDoc]
  },
  timestamp: { type: Date, default: Date.now }
});

// Index
ContractExecutionSchema
  .index({ contractExecId: 1 }, { unique: true });

// Export model.
module.exports = model('contractexec', ContractExecutionSchema);