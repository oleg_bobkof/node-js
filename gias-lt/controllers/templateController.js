const
  async = require("async");

const
  config = require('../config/configure'),
  logger = require('../utils/logger'),
  Template = require('../models/template'),
  { getTemplateDB, messageHandler } = require('../config/db.mssql');

const mssqlDS = () => config.getDataSource('mssql') || [];
const frmMSSQLUri = (dbconfig) => `mssql://${dbconfig.server}:${dbconfig.port || dbconfig.options.instanceName}/${dbconfig.database}`;
const uniqueArray = (arr) => [...new Set(arr.map(o => JSON.stringify(o)))].map(s => JSON.parse(s));
const groupFinanceSources = (fs) => uniqueArray(fs.map(f => ({ tkId: f.tkId, finId: config.getFinId(f.finId) })));
const getTemplateById = (id, cb) => Template.findById(id).exec(cb);
const getTemplates = (cb) => Template.find({}).exec(cb);
const getTemplateConnId = (connectionId, cb) => Template.findOne({connectionId: connectionId}).exec(cb);

// Save template
function saveTemplate(db, callback) {

  let { database: { tkId, dbconfig } } = db;

  template = new Template({
    tkId: tkId,
    connectionId: frmMSSQLUri(dbconfig),
    dbconfig: dbconfig
  });

  template.save(function (err, doc) {
    if (err) {
      logger.error(err);
      callback(err);
    } else {
      callback(null, doc);
    }
  });

}

//
function loadTemplates(callback) {

  async.map(
    mssqlDS(),
    function (database, cb) {
      saveTemplate(database, cb);
    },
    function (err, result) {
      if (err) {
        callback(null, null);
        logger.error('Error parsing config file - [databases]');
      } else {
        callback(null, result);
      }
    }
  );

}

//
function prepareTemplate(template, callback) {

  const mess = `[${template.tkId}] ${template.connectionId} - `;
  getTemplateDB(template.dbconfig, function (err, filters) {
    if (err) {
      logger.error(mess, { message: err });
      return callback(err);
    }

    logger.info(mess, { message: 'OK' });
    template.connectionCheck = true;
    template.filters = filters;
    template.save(function (err, doc) {
      if (err) { return callback(err); }
      config.addRecipients(doc);
      callback(null, doc);
    });
  });

}

//
function processTemplates(callback) {

  Template.find({}, function (err, items) {
    if (err) { return callback(err); }

    async.map(
      items,
      function (item, cb) {
        prepareTemplate(item, cb);
      },
      function (err, result) {
        if (err) {
          callback(err);
        } else {
          callback(null, result);
        }
      }
    );
  });

}

//
function checkDBs() {

  async.map(
    mssqlDS(),
    function (db, callback) {
      let { database: { dbconfig } } = db;
      messageHandler(dbconfig, callback);
    },
    function (err, result) {
      if (err) {
        logger.error('Checking the mssql databases failed: ', { message: err.message });
      } else {
        logger.info('Done checking mssql databases TK');
      }
    }
  );

}

// Check all templates
const checkTemplates = (cb) => {
  async.series([
    (callback) => Template.deleteMany({}, callback),
    (callback) => loadTemplates(callback),
    (callback) => processTemplates(callback)
  ],
    (err) => {
      if (err) {
        logger.error('Checking the mssql databases TK - failed');
        cb(err);
      } else {
        logger.info('Done checking mssql databases TK');
        cb();
      }
    }
  );
};

// Filter Finance Sources
const filterFinanceSources = (object_fs) => {

  let arr = [];
  config.recipients.forEach(function (filter) {
    let fs_list = object_fs.filter(elem => filter.tkId === elem.tkId && filter.finId === config.getFinId(elem.finId));
    Array.prototype.push.apply(arr, fs_list);
  });
  return arr;

};

// Filter Finance Sources by Template
const applyTemplate = (object_fs, template) => {

  let arr = [];
  template.filters.forEach(function (filter) {
    let fs_list = object_fs.filter(elem => filter.tkId === elem.tkId && filter.finId === config.getFinId(elem.finId));
    Array.prototype.push.apply(arr, fs_list);
  });
  return arr;

};

//
const applyTemplates = (object_fs) => {

  let classification = [];
  let map = new Map();
  const fs_list = groupFinanceSources(object_fs);
  for (const item of fs_list) {
    const fs = config.recipients.find(elem => item.tkId == elem.tkId && item.finId == elem.finId);
    if (fs) {
      if (!map.has(fs.id)) {
        map.set(fs.id, true);
        classification.push({ id: fs.id });
      }
    }
  }
  return classification;

};

//
module.exports = { checkTemplates, checkDBs, groupFinanceSources, applyTemplate, applyTemplates, 
                    getTemplateById, getTemplateConnId, getTemplates, filterFinanceSources };