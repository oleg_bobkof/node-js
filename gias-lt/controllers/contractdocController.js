const
  async = require("async"),
  axios = require("axios");

const ContractDoc = require('../models/contractdoc');
const { db, mongodb } = require('../config/db.mongo');
const { createErr404 } = require('../utils/errors');
const { makeRequest } = require('../utils/common');
const config = require('../config/configure');

const BUCKET_NAME = 'contracts';
const CHUNKS_COLL = 'contracts.chunks';
const FILES_COLL = 'contracts.files';

// Delete contract docs
const deleteContractDocs = exports.deleteContractDocs = function (contractId, cb) {

  async.waterfall([
    function (callback) {
      ContractDoc.find({ contractId: contractId }, callback);
    },
    function (contractdocs, callback) {
      let bucket = new mongodb.GridFSBucket(db.db, { bucketName: BUCKET_NAME });
      async.map(
        contractdocs,
        function (contractdoc, delCallback) {
          let { file_id } = contractdoc;
          bucket.delete(file_id, function (err) {
            //if (err) { delCallback(err) } else {
            ContractDoc.deleteOne({ contractId: contractId, file_id: file_id }).exec(delCallback);
            //};
          });
        },
        function (err, del_items) {
          if (err) { callback(err); } else {
            callback(err, contractdocs, del_items);
          }
        }
      );
    },
  ],
    function (err) {
      if (err) { cb(err); } else { cb(); }
    }
  );

};

// Save contract document
function saveContractDoc(contractId, file_id, contractdoc, cb) {

  let { file_type, file_name, file_link } = contractdoc;
  ContractDoc.findOne({ contractId: contractId, file_id: file_id }, function (err, doc) {
    if (err) { return cb(err); }
    if (!doc) {
      doc = new ContractDoc({
        contractId: contractId,
        file_id: file_id,
        file_type: file_type,
        file_name: file_name,
        file_link: file_link
      });
      doc.save(function (err, doc) {
        if (err) { return cb(err); }
        cb(null, doc);
      });
    } else { cb(null, doc); }
  });

}

// Save contract documents
function saveContractDocuments(responseArray, contractId, cb) {

  async.series([
    function (callback) {
      deleteContractDocs(contractId, callback);
    },
    function (callback) {
      let bucket = new mongodb.GridFSBucket(db.db, { bucketName: BUCKET_NAME });
      async.mapSeries(
        responseArray,
        function (item, rdCb) {
          let contractdoc = item.contractdoc;
          let uploadStream = bucket.openUploadStream(contractdoc.file_name);
          let file_id = uploadStream.id;
          saveContractDoc(contractId, file_id, contractdoc, function (err, doc) {
            if (err) { rdCb(err); }
            item.pipe(uploadStream);
            rdCb(null, doc);
          });
        }, function (err, results) {
          // TO DO
          callback(err, results);
        });
    },
  ], function (err, results) {
    if (err) {
      return cb(err, null);
    }
    cb(null, results);
  });

}

// Uploads contract docs
exports.loadContractDocs = function (contract, cb) {

  const { contractId } = contract;
  const { request = {} } = config.gias_mf;
  const options = JSON.parse(JSON.stringify(request));
  options.method = 'GET';

  async.waterfall([
    function (callback) {
      options.responseType = 'json';
      options.url = `/asfr/download/${contractId}/doclinks`;
      makeRequest(options, callback);
    },
    function (contractdocs, callback) {
      options.responseType = 'stream';
      let responseArray = [];
      let promiseArray = contractdocs.map(doc => axios.get(`/asfr/download/${contractId}/contractdoc/${doc.file_id}`, options)) || [];
      axios.all(promiseArray)
        .then(axios.spread((...args) => {
          for (let i = 0; i < args.length; i++) {
            responseArray.push(args[i].data);
            responseArray[i].contractdoc = contractdocs[i];
          }
        }))
        .then(function () {
          saveContractDocuments(responseArray, contractId, callback);
        })
        .catch(function (err) {
          err.message = `${err.message} - ${link}`;
          callback(err);
        });
    },
  ],
    function (err) {
      if (err) { cb(err); } else { cb(); }
    }
  );

};

//
exports.contractData = function (contractId, cb) {

  ContractDoc.findOne({ contractId: contractId }, function (err, contractdoc) {
    if (err) { return cb(err); }
    let { file_id } = contractdoc;
    contractDataToBase64(file_id, function (err, data) {
      if (err) { cb(err); } else {
        cb(null, data);
      }
    });
  });

};

//
function contractDataToBase64(file_id, cb) {

  let bucket = new mongodb.GridFSBucket(db.db, { bucketName: BUCKET_NAME });
  let dataStream = bucket.openDownloadStream(file_id);
  // let dataStream = downloadStream(file_id);
  let chunks = [];

  dataStream.on('data', (chunk) => {
    chunks.push(chunk);
  });

  dataStream.on('error', (err) => {
    console.log(err);
    cb(err);
  });

  dataStream.on('end', () => {
    let data = Buffer.concat(chunks).toString('base64');
    cb(null, data);
  });

}

//
exports.contractDataListToBase64 = function (contractId, cb) {

  async.waterfall([
    function (callback) {
      ContractDoc.find({ contractId: contractId }, callback);
    },
    function (contractdocs, callback) {
      async.mapSeries(
        contractdocs,
        function (contractdoc, cb_item) {
          let { file_id, file_type, file_link, file_name } = contractdoc;
          contractDataToBase64(file_id, function (err, data) {
            if (err) { return cb_item(err); }
            let contractData = {
              filetype: file_type,
              filename: file_name,
              filelink: file_link,
              data: data
            };
            cb_item(null, contractData);
          });
        },
        function (err, contractData) {
          if (err) { callback(err); } else {
            callback(err, contractdocs, contractData);
          }
        }
      );
    },
  ],
    function (err, contractdocs, contractData) {
      if (err) { return cb(err); }
      cb(null, contractData);
    }
  );

};

//
exports.api_contractdoc_get = function (req, res, next) {

  let { id, filename } = req.params;
  async.waterfall([
    function (callback) {
      ContractDoc.findOne({ contractId: id }, callback);
    },
    function (contractdocs, callback) {
      let bucket = new mongodb.GridFSBucket(db.db, { bucketName: BUCKET_NAME });
      let { file_id } = contractdocs;
      let downloadStream = bucket.openDownloadStream(file_id);
      downloadStream.on('data', (chunk) => {
        res.write(chunk);
      });
      downloadStream.on('error', (err) => {
        callback(createErr404(err));
      });
      downloadStream.on('end', () => {
        callback(null, contractdocs, null);
      });
    },
  ],
    function (err) {
      if (err) { return next(err); }
      res.end();
    }
  );

};