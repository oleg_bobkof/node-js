const
  axios = require("axios"),
  config = require('../config/configure'),
  logger = require('../utils/logger'),
  { createErr400, createErr500, createErr404 } = require('../utils/errors');

const
  JOB_URL_NOW = '/api/job/now',
  JOB_URL_ONCE = '/api/job/once';

const frmOptions = (options) => {
  return `job [${options.data.name}]: ${options.method} ${options.baseURL}${options.url} `;
};

// Make http request
function makeRequest(options, cb) {

  axios(options)
    .then(function (response) {
      logger.info(frmOptions(options));
      cb(null, response.data);
    })
    .catch(function (error) {
      logger.error(frmOptions(options), { message: error.message });
      if (error.response) {
        cb(createErr404(error.response));
      } else if (error.request) {
        error.message = `${error.message} - ${error.request._currentUrl}`;
        cb(createErr400(400, error));
      } else {
        cb(createErr500(error));
      }
    });

}

// Contract - make http request
exports.addJobSaveContract = function (contractId, cb) {

  const { job = {} } = config.gias_scheduler.requests[0];
  const options = JSON.parse(JSON.stringify(job));
  options.data = options.data || {};
  options.data.data = {
    headers: "",
    params: JSON.stringify({ contractid: contractId }),
    query: "",
    body: {}
  };

  makeRequest(options, cb);

};

// #1
exports.addJobDeleteContract = function (contractId, cb) {

  const { job = {} } = config.gias_scheduler.requests[1];
  const options = JSON.parse(JSON.stringify(job));
  options.data = options.data || {};
  options.data.data = {
    headers: "",
    params: JSON.stringify({ contractid: contractId }),
    query: "",
    body: {}
  };

  makeRequest(options, cb);

};

// #2
exports.addJobReservationLT = function (reservationId, cb) {

  const { job = {} } = config.gias_scheduler.requests[2];
  const options = JSON.parse(JSON.stringify(job));
  options.data = options.data || {};
  options.data.data = {
    headers: "",
    params: JSON.stringify({ reservationid: reservationId }),
    query: "",
    body: {}
  };

  makeRequest(options, cb);

};

// #3
exports.addJobReservationReply = function (reservation_item, cb) {

  const { reservation, reservation: { requestId } } = reservation_item;
  const { job = {} } = config.gias_scheduler.requests[3];
  const options = JSON.parse(JSON.stringify(job));
  options.data = options.data || {};
  options.data.data = {
    headers: "",
    params: JSON.stringify({ reservationid: requestId }),
    query: "",
    body: reservation
  };

  makeRequest(options, cb);

};

// #4 - send_contractexec
exports.addJobContractExecSend = function (contractexec_obj, cb) {

  const { contractExec, contractExec: { requestId } } = contractexec_obj;
  const { job = {} } = config.gias_scheduler.requests[4];
  const options = JSON.parse(JSON.stringify(job));
  options.data = options.data || {};
  options.data.data = {
    headers: "",
    params: JSON.stringify({ contractexecid: requestId }),
    query: "",
    body: contractExec
  };

  makeRequest(options, cb);

};

// #5 - save_contractexec
exports.addJobContractExecProcess = function (param_obj, cb) {

  const { job = {} } = config.gias_scheduler.requests[5];
  const options = JSON.parse(JSON.stringify(job));
  options.data = options.data || {};
  options.data.data = {
    headers: "",
    params: {},
    query: "",
    body: param_obj || {}
  };

  makeRequest(options, cb);

};

// #6 - prepare_contractexec
exports.addJobContractExecPrepare = function (param_obj, cb) {

  const { job = {} } = config.gias_scheduler.requests[6];
  const options = JSON.parse(JSON.stringify(job));
  
  // const interval = 5;   
  // options.data.interval = `in ${interval} seconds`;
  // options.url = JOB_URL_ONCE;

  options.data = options.data || {};
  options.data.data = {
    headers: "",
    params: {},
    query: "",
    body: param_obj || {}
  };

  logger.info(JSON.stringify(options.data.data.body));
  makeRequest(options, cb);

};