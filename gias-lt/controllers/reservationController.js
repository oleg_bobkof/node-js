const
  async = require("async"),
  { param, body, validationResult } = require('express-validator');

const
  Reservation = require('../models/reservation'),
  { saveReservationToDB, cancelReservationToDB } = require('../config/db.mssql'),
  { createHttpErr, createErr400, createErr404 } = require('../utils/errors'),
  { addJobReservationLT, addJobReservationReply } = require('../controllers/jobController'),
  { applyTemplate, applyTemplates, getTemplateById, filterFinanceSources } = require('../controllers/templateController');

const errorFormatter = ({ location, msg, param, value }) => {
  return `${location}[${param}]: ${value} - ${msg}`;
};

// Validation request
function validationReq(request) {
  // Extract the validation errors from a request.
  const errors = validationResult(request).formatWith(errorFormatter);
  if (!errors.isEmpty()) {
    let err = new createHttpErr.BadRequest(errors.array());
    err.headers = { error_code: -115 };
    throw err;
  }

}

// Less than one minute
function lessOneMinute(timestamp) {
  let dt = new Date();
  let statusDate = new Date(timestamp);
  statusDate.setMinutes(statusDate.getMinutes() + 1);
  return (dt < statusDate);
}

// Save reservation
function saveReservation(reservation_obj, cb) {

  reservation_obj.save(function (err, doc) {
    if (err) { return cb(createErr400(-102, err)); }
    cb(null, doc);
  });

}

// Find reservation by ID
function findReservation(requestid, cb) {

  Reservation.findOne({ reservationId: requestid }, function (err, reservation) {
    if (err) {
      let err400 = createErr400(-102, err);
      return cb(err400);
    }
    // No results.
    if (reservation == null) {
      let err404 = new createHttpErr.NotFound(`Reservation requestid = ${requestid} isn't found.`);
      return cb(err404);
    }

    cb(null, reservation);
  });

}

// Update reservation status
function updateReservationStatus(reservationid, statusCode, cb) {

  Reservation.updateOne({ reservationId: reservationid },
    {
      statusCode: statusCode,
      statusTimestamp: Date.now()
    }, cb);

}

// Clear Finance Sources
function clearReservationFS(financeSources, template, dt) {

  let fs_list = applyTemplate(financeSources, template);
  fs_list.forEach(function (fs) {
    fs.isSufficientlyByASFR = false;
    fs.ASFRdatetime = dt;
    fs.processingStatus = -1;
  });

}

// Check reservation 
function cheсkReservation(reservation, template, dt, cb) {

  saveReservationToDB(template, reservation, function (err, recordset) {
    if (err) { return cb(err); }
    const { financeSources = [] } = reservation;
    if (recordset) {
      clearReservationFS(financeSources, template, dt);
      // apply result
      recordset.forEach(function (item) {
        let fs = financeSources.find(elem => item._id == elem._id);
        if (fs) {
          fs.isSufficientlyByASFR = item.isSufficientlyByASFR;
          fs.ASFRdatetime = dt;
          fs.processingStatus = 0;
          // TO DO: new fields 
          fs.reason = item.AdditionalInfo;
          fs.restPlan = item.RestPlan;
          fs.reservCost = item.ReservSum;
        }
      });
    }
    cb(null, reservation);
  });

}

// Process reservation
function processReservation(reservation_obj, cb) {

  const { reservation, reservation: { financeSources = [] } } = reservation_obj;
  const items = applyTemplates(financeSources);
  const dt = Date.now();
  
  async.series([
    function (callback) {
      async.mapSeries(
        items,
        function (template_link, cb_item) {
          getTemplateById(template_link.id, function (err, template) {
            if (err) { return cb_item(err); }
            cheсkReservation(reservation, template, dt, cb_item);
          });
        },
        function (err, result) {
          if (err) { return callback(err); }
          callback(null, result);
        }
      );
    },
    function (callback) {
      if (items.length === 0) {
        callback(null, reservation_obj);
      } else {
        reservation_obj.statusCode = 1;
        reservation_obj.statusTimestamp = dt;
        saveReservation(reservation_obj, callback);  
      }
    }
  ],
    function (err, result) {
      if (err) { return cb(err); }
      cb(null, result[1]);
    }
  );

}

// Cancel reservation
function cancelReservation(reservation_obj, cb) {

  const { reservationId, reservation } = reservation_obj;
  const items = applyTemplates(reservation.financeSources);

  async.series([
    function (callback) {
      async.mapSeries(
        items,
        function (template_link, cb_item) {
         
          getTemplateById(template_link.id, function (err, template) {
            if (err) { return cb_item(err); }
            cancelReservationToDB(template, reservation, cb_item);
          });
        },
        function (err, recordset) {
          if (err) { return callback(err); }
          let fs_items = recordset[0];
          callback(null, fs_items);
        }
      );
    },
    function (callback) {
      updateReservationStatus(reservationId, -2, callback);
    }
  ],
    function (err, results) {
      if (err) { return cb(err); }
      cb(err, results);
    }
  );

}

// Handle Author create on POST.
exports.api_reservation_post = [

  // Validate fields.
  body('purchaseGiasId')
    .isUUID(), //.withMessage('invalid value'),
  body('requestId')
    .isUUID(),
  body('financeSources')
    .isArray(),
  body('financeSources').custom((value, { req }) => {
    // Indicates the success of this synchronous custom validator
    return true;
  }),
  // Process request after validation and sanitization.
  (req, res, next) => {

    validationReq(req);
    const requestid = req.body.requestId;

    Reservation.findOne({ reservationId: requestid }, function (err, reservation_obj) {
      if (err) { return next(err); }

      if (!reservation_obj) {
        reservation_obj = new Reservation({ reservationId: requestid, reservation: req.body });
      } else {
        if (reservation_obj.statusCode !== -1) {
          return res.status(200).json(reservation_obj.reservation);
        } else {
          if (lessOneMinute(reservation_obj.statusTimestamp)) {
            return res.status(200).json(reservation_obj.reservation);
          }
        }
      }

      async.series([
        function (callback) {
          saveReservation(reservation_obj, callback);
        },
        function (callback) {
          addJobReservationLT(reservation_obj.reservationId, callback);
        },
      ],
        function (err) {
          if (err) { return next(err); }
          res.status(200).json(reservation_obj.reservation);
        }
      );
    });
  }

];

//
exports.api_reservation_put = [

  param('reservationid').isUUID(),
  (req, res, next) => {

    validationReq(req);
    const reservationId = req.params.reservationid;

    async.waterfall([
      function (callback) {
        findReservation(reservationId, callback);
      },
      function (reservation_obj, callback) {
        cancelReservation(reservation_obj, function (err, doc) {
          if (err) { return callback(err); }
          callback(err, reservation_obj, doc);
        });
      }
    ],
      function (err, reservation_obj) {
        if (err) { return next(err); }
        res.status(200).json(reservation_obj.reservation);
      }
    );

  }

];

// Status reservation return on GET 
exports.api_reservation_save = [

  param('reservationid').isUUID(),
  (req, res, next) => {

    validationReq(req);
    const reservationId = req.params.reservationid;

    async.waterfall([
      function (callback) {
        findReservation(reservationId, callback);
      },
      function (reservation_obj, callback) {
        processReservation(reservation_obj, function (err, doc) {
          if (err) { return callback(err); }
          callback(err, reservation_obj, doc);
        });
      },
      function (reservation_obj, doc, callback) {
        addJobReservationReply(doc, function (err, job) {
          if (err) { return callback(err); }
          callback(err, reservation_obj, doc, job);
        });
      },
    ],
      function (err, reservation_obj, doc, job) {
        if (err) { return next(err); }
        res.status(200).json(doc);
      }
    );
  }

];

// Status reservation return on GET 
exports.api_reservation_status = [

  param('reservationid').isUUID(),
  (req, res, next) => {
    validationReq(req);

    findReservation(req.params.reservationid, function (err, reservation_obj) {
      if (err) { return next(err); }
      let { reservation, statusCode } = reservation_obj;
      if (statusCode === -1) {
        res.status(202).send('Запрос на резервирование принят ТК, но еще находится в обработке.');
      } else {
        res.status(200).json(reservation);
      }
    });
  }

];