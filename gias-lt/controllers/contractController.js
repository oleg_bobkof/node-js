'use strict';

const
  async = require("async");

const Contract = require('../models/contract');
const { body, validationResult } = require('express-validator');
const { addContractToDB, addContractDocToDB, markContractToDB } = require('../config/db.mssql');
const { createHttpErr, createErr400, createErr404 } = require('../utils/errors');
const { addJobSaveContract, addJobDeleteContract } = require('../controllers/jobController');
const { loadContractDocs, contractDataListToBase64 } = require('../controllers/contractdocController');
const { applyTemplates, getTemplateById } = require('../controllers/templateController');

const errorFormatter = ({ location, msg, param, value }) => {
  return `${location}[${param}]: ${value} - ${msg}`;
};

// Save contract
function saveContract(contract, cb) {

  let contractId = contract.contractId;
  Contract.findOne({ contractId: contractId }, function (err, doc) {
    if (err) { return cb(createErr400(-102, err)); }
    if (!doc) {
      doc = contract;
    } else {
      doc.contractId = contract.contractId;
      doc.contract = contract.contract;
      doc.statusCode = -1;
      doc.statusTimestamp = Date.now();
    }
    doc.save(function (err, doc) {
      if (err) { return cb(createErr400(-102, err)); }
      cb(null, doc);
    });
  });

}

// Update contract status
function updateContractStatus(contractId, statusCode, cb) {

  Contract.updateOne({ contractId: contractId },
    {
      statusCode: statusCode,
      statusTimestamp: Date.now()
    }, cb);

}

// New Contract Header
function getContractAED(contract_obj) {

  let c = new Contract({ contractId: contract_obj.contractId, contract: contract_obj });
  c.contract.financeSource = undefined;
  c.contract.payments = undefined;
  c.contract.contractPositions = undefined;
  c.contract.contractDocuments = undefined;
  return c.contract;

}

//
function saveContractAED(contract, cb) {

  const { contractId, contract: { financeSource = [] } } = contract;
  const items = applyTemplates(financeSource);
  if (items.length == 0) { return cb(); }

  async.waterfall([
    function (callback) {
      contractDataListToBase64(contractId, callback);
    },
    function (contractdatas, callback) {
      async.mapSeries(
        items,
        function (template_link, cb_item) {
          getTemplateById(template_link.id, function (err, template) {
            if (err) { return cb_item(err); }
            saveContractToDB(contract, contractdatas, template, cb_item);
          });
        },
        function (err, contractaed) {
          if (err) { callback(err); } else {
            callback(err, contractdatas, contractaed);
          }
        }
      );
    },
  ],
    function (err, contractdatas, contractaed) {
      if (err) { return cb(err); }
      cb(null, contractaed);
    }
  );

}

// Add Contract
function saveContractToDB(contract_obj, contractdatas, template, cb) {

  if (!template) { return cb(null, null); }
  let { contract } = contract_obj;

  async.waterfall([
    function (callback) {
      let contractAED = getContractAED(contract);
      addContractToDB(template, contract, contractAED, callback);
    },
    function (result, callback) {
      let { contractDocuments = [] } = contract;
      async.map(
        contractdatas,
        function (contractdata, saveCallback) {
          let { returnValue = 0 } = result;
          if (returnValue != 0) {
            let contractdoc = contractDocuments.find(doc => doc.link.link === contractdata.filelink) || {};
            addContractDocToDB(template, returnValue, contractdoc, contractdata, saveCallback);
          } else {
            saveCallback();
          }
        },
        function (err, docaed) {
          if (err) { callback(err); } else {
            callback(err, result, docaed);
          }
        }
      );
    },
    // Mark contract
    function (result, docaed, callback) {
      let { returnValue = 0 } = result;
      markContractToDB(template, returnValue, function (err, mark) {
        if (err) { callback(err); } else {
          callback(err, result, docaed, mark);
        }
      });
    },
  ],
    function (err) {
      if (err) { return cb(err); }
      cb(null, null);
    }
  );

}

// Handle Contract create on POST.
exports.api_contract_post = [

  // Validate fields.
  body('contractId')
    .isUUID(), //.withMessage('invalid value'),

  // Process request after validation and sanitization.
  (req, res, next) => {

    // Extract the validation errors from a request.
    const errors = validationResult(req).formatWith(errorFormatter);
    if (!errors.isEmpty()) {
      var err = new createHttpErr.BadRequest(errors.array());
      err.headers = { error_code: -105 };
      return next(err);
    }

    let contractId = req.body.contractId;
    let contract = new Contract({ contractId: contractId, contract: req.body });

    async.series([
      function (callback) {
        saveContract(contract, callback);
      },
      function (callback) {
        loadContractDocs(contract, callback);
      },
      function (callback) {
        addJobSaveContract(contractId, callback);
      }
    ],
      function (err) {
        if (err) { return next(err); }
        res.status(201).json(contract);
      }
    );
  }

];

// Save contract on GET 
exports.api_contract_save = function (req, res, next) {

  const contractId = req.params.contractid;

  Contract.findOne({ contractId: contractId }, function (err, contract) {
    if (err) { return next(err); }

    if (!contract) {
      let error = createErr404(`Contract ${contractId} isn't found.`);
      return next(error);
    }

    async.series([
      function (callback) {
        saveContractAED(contract, callback);
      },
      function (callback) {
        addJobDeleteContract(contractId, callback);
      },
      function (callback) {
        updateContractStatus(contractId, 1, callback);
      }
    ],
      function (err) {
        if (err) { return next(err); }
        res.status(200).json(contract.contract);
      }
    );
  });

};