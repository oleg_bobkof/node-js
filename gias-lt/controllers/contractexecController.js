const
  uuidv4 = require('uuid/v4'),
  async = require("async"),
  logger = require('../utils/logger'),
  { query, body, validationResult } = require('express-validator');

const
  ContractExec = require('../models/contractexec'),
  { getContractExecList, getContractExecFromDB } = require('../config/db.mssql'),
  { createHttpErr, createErr400, createErr404 } = require('../utils/errors'),
  { addJobContractExecSend, addJobContractExecProcess, addJobContractExecPrepare } = require('../controllers/jobController'),
  { getTemplates, getTemplateConnId } = require('../controllers/templateController');

const errorFormatter = ({ location, msg, param, value }) => {
  return `${location}[${param}]: ${value} - ${msg}`;
};

// Validation request
function validationReq(request) {
  // Extract the validation errors from a request.
  const errors = validationResult(request).formatWith(errorFormatter);
  if (!errors.isEmpty()) {
    let err = new createHttpErr.BadRequest(errors.array());
    err.headers = { error_code: -115 };
    throw err;
  }

}

// Save contract execution
function saveContractExec(contractexec_obj, cb) {

  contractexec_obj.save(function (err, doc) {
    if (err) { return cb(createErr400(-102, err)); }
    cb(null, doc);
  });

}

// Update contract execution status
function updateContractExecStatus(contractExecId, statusCode, cb) {

  ContractExec.updateOne({ contractExecId: contractExecId },
    {
      statusCode: statusCode,
      statusTimestamp: Date.now()
    }, cb);

}

// Prepare contract execution
function prepareContractExec(template, ContrId, startDate, endDate, cb) {


  getContractExecFromDB(template, ContrId, startDate, endDate, function (err, contractPayDocs) {
    if (err) { return cb(err); }
    if (!(contractPayDocs[0])) { return cb(); }

    const uuid = uuidv4();
    const { contractId } = contractPayDocs[0];
    const items = contractPayDocs.map(pd => ({
      payType: pd.payType,
      docNumber: pd.DocNumber,
      bankDate: pd.bankDate,
      paymentCost: {
        budgetCost: pd.budgetCost,
        tkId: pd.tkId,
        finYear: pd.FinYear,
        finId: pd.finId,
      }
    }));

    const contractexec_obj = new ContractExec({
      contractExecId: uuid,
      contractExec: {
        requestId: uuid,
        contractId: contractId,
        startDate: startDate,
        endDate: endDate,
        PaymentDocs: items
      }
    });

    async.series([
      function (callback) {
        saveContractExec(contractexec_obj, callback);
      },
      function (callback) {
        addJobContractExecSend(contractexec_obj, callback);
      },
      function (callback) {
        updateContractExecStatus(contractexec_obj.contractExecId, 0, callback);
      }
    ],
      function (err, result) {
        if (err) { return cb(err); }
        cb(null, result[0]);
      }
    );

  });

}

// Process contract execution
function processContractExec(template, startDate, endDate, cb) {

  async.waterfall([
    function (callback) {
      getContractExecList(template, startDate, endDate, callback);
    },
    function (contractIDs, callback) {
      async.mapSeries(
        contractIDs,
        function (contract, cb_item) {
          const { ContrId = 0 } = contract;
          const param_obj = {
            contrId: ContrId,
            startDate: startDate.getTime(),
            endDate: endDate.getTime(),
            template: {
              tkId: template.tkId,
              connectionId: template.connectionId
            }
          };
          addJobContractExecPrepare(param_obj, cb_item);
        },
        function (err, result) {
          if (err) { return callback(err); }
          callback(null, result, contractIDs);
        }
      );
    },
  ],
    function (err, contracts, contractIDs) {
      if (err) { return cb(err); }
      cb(null, contracts);
    }
  );

}

// Check contract execution on GET 
exports.api_contractexecs_get = [

  query('startDate')
    .toInt(),
  query('endDate')
    .toInt(),
  query('forDays')
    .toInt(),

  (req, res, next) => {

    validationReq(req);
    let { startDate, endDate, forDays = 7 } = req.query;

    if (!startDate) {
      startDate = new Date();
      startDate.setDate(startDate.getDate() - forDays);
      startDate.setHours(0, 0, 0, 0);
    } else {
      startDate = new Date(startDate);
    }

    if (!endDate) {
      endDate = new Date();
      // endDate.setHours(12, 0, 0, 0);
      endDate.setHours(0, 0, 0, 0);
    } else {
      endDate = new Date(endDate);
    }

    async.waterfall([
      function (callback) {
        getTemplates(callback);
      },
      function (templates, callback) {
        async.mapSeries(
          templates,
          function (template, cb) {
            let param_obj = {
              startDate: startDate.getTime(),
              endDate: endDate.getTime(),
              template: {
                tkId: template.tkId,
                connectionId: template.connectionId
              }
            };
            addJobContractExecProcess(param_obj, cb);
          },
          function (err, result) {
            if (err) { return callback(err); }
            callback(null, templates, result);
          }
        );
      },
    ],
      function (err, contracts, templates) {
        if (err) { return next(err); }
        res.status(200).json(templates);
      }
    );

  }

];


// Check contract execution on POST
exports.api_contractexecs_post = [

  body('startDate')
    .toInt(),
  body('endDate')
    .toInt(),
  body('template.connectionId')
    .isString(),

  (req, res, next) => {

    validationReq(req);
    let { startDate, endDate, template: { connectionId } } = req.body;

    async.waterfall([
      function (callback) {
        getTemplateConnId(connectionId, callback);
      },
      function (template_obj, callback) {
        processContractExec(template_obj, new Date(startDate), new Date(endDate), function (err, contractexecs) {
          if (err) { return callback(err); }
          callback(null, template_obj, contractexecs);
        });
      },
    ],
      function (err, templates, contractexecs) {
        if (err) { return next(err); }
        res.status(200).json(contractexecs);
      }
    );
  }

];

// 
exports.api_contractexec_prepare_post = [

  body('contrId')
    .toInt(),
  body('startDate')
    .toInt(),
  body('endDate')
    .toInt(),
  body('template.connectionId')
    .isString(),

  (req, res, next) => {

    validationReq(req);
    const { contrId, startDate, endDate, template: { connectionId } } = req.body;

    async.waterfall([
      function (callback) {
        getTemplateConnId(connectionId, callback);
      },
      function (template_obj, callback) {
        prepareContractExec(template_obj, contrId, new Date(startDate), new Date(endDate), function (err, contractexec) {
          if (err) { return callback(err); }
          callback(null, template_obj, contractexec);
        });
      },
    ],
      function (err, templates, contractexec) {
        if (err) { return next(err); }
        res.status(200).json(contractexec);
      }
    );

  }

];
