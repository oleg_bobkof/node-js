SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gias_AddContractDocuments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gias_AddContractDocuments]
GO
-- Version 2; Date 06.10.2019; LastEditor Oleg
CREATE PROCEDURE [dbo].[gias_AddContractDocuments]
    @masterId int,
    @fileType smallint,	
    @contractDocuments ntext,
    @contractData ntext
AS
BEGIN

    set nocount on
    declare @result smallint
    set  @result = 0
     
    if exists(select 1 from giasContracts where [Id] = @masterId  )
    begin    
        insert into giasContractsDocuments( 
           masterId, fileType, contractDocuments, contractData)
        values (
          @masterId, @fileType, @contractDocuments, @contractData)
     
        set @result = @@identity
    end

    RETURN @result

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[gias_AddContractDocuments]  TO [public]
GO

