if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gias_AddContract]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gias_AddContract]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- Version 9; Date 10.12.2019; LastEditor Oleg
CREATE PROCEDURE [dbo].[gias_AddContract]
    @contractId uniqueidentifier,
    @baseContractId uniqueidentifier,
    @terminationExecutionTerm datetime,
    @contract ntext,
    @financeSource ntext,
    @payments ntext,
    @contractPositions ntext,
    @contractDocuments ntext,
    @contractData ntext,
    @contractXML image = null,
    @contractJSON image = null
AS
BEGIN
    set nocount on 

    declare 
      @result int,
      @len int,
      @startPos smallint,
      @stepId smallint,
      @ptrval varbinary(16),
      @dataXML varchar (8000)

    set @result = 0 
     
    IF NOT EXISTS(
        SELECT 1 FROM
            giasContracts
        WHERE 
           (contractId = @contractId and @terminationExecutionTerm is null and terminationExecutionTerm is null ) 
	        or
           (contractId = @contractId  and @terminationExecutionTerm is not null and terminationExecutionTerm is not null ))
     BEGIN 
         INSERT INTO giasContracts(
             [contractId], [terminationExecutionTerm], 
             [contract], financeSource, payments, contractPositions, contractDocuments, contractData, contractXML, contractJSON)
         VALUES(
             @contractId, @terminationExecutionTerm, 
             @contract, @financeSource, @payments, @contractPositions, @contractDocuments, @contractData, '', '')

         set @result =  scope_identity()
     END

     -- update contractXML
     IF (@result <> 0 and @contractXML is not null)
     BEGIN
         set @startPos = 1 
         set @stepId = 8000
         set @len = DATALENGTH(@contractXML)
         select @ptrval = TEXTPTR(contractXML) from giasContracts where [Id] = @result
         set @dataXML = CAST(SUBSTRING(@contractXML, @startPos, @stepId) as varchar(8000)) 

         WRITETEXT giasContracts.contractXML @ptrval @dataXML
   
          while (@len > @stepId and @startPos <= @len) 
          begin
            set @startPos = @startPos + @stepId
            set @dataXML = CAST(SUBSTRING(@contractXML, @startPos, @stepId) as varchar(8000)) 

            UPDATETEXT giasContracts.contractXML @ptrval null 0 @dataXML
          end
     END
		
     -- update contractJSON
     IF (@result <> 0 and @contractJSON is not null)
     BEGIN
         set @startPos = 1 
         set @stepId = 8000
         set @len = DATALENGTH(@contractJSON)
         select @ptrval = TEXTPTR(contractJSON) from giasContracts where [Id] = @result
         set @dataXML = CAST(SUBSTRING(@contractJSON, @startPos, @stepId) as varchar(8000)) 

         WRITETEXT giasContracts.contractJSON @ptrval @dataXML
   
          while (@len > @stepId and @startPos <= @len) 
          begin
            set @startPos = @startPos + @stepId
            set @dataXML = CAST(SUBSTRING(@contractJSON, @startPos, @stepId) as varchar(8000)) 

            UPDATETEXT giasContracts.contractJSON @ptrval null 0 @dataXML
          end
     END
	
     RETURN @result
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO