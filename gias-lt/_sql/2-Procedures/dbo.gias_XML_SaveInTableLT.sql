SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gias_XML_SaveInTableLT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gias_XML_SaveInTableLT]
GO


-- Version 1; Date 18.12.2019; LastEditor nastya
CREATE  procedure dbo.gias_XML_SaveInTableLT (@data ntext, @Id int) 
as 

set nocount on

 
declare @GIASLTContr table
 (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[ContrId] [varchar] (50) NOT NULL ,
	[etsId] [varchar] (50) NULL ,
	[baseContrId] [varchar] (50) NULL ,
	[unpCustomer] [varchar] (9) NULL ,
	[okogu] [varchar] (50) NULL ,
	[titleContract] [varchar] (255) NOT NULL ,
	[contractNum] [varchar] (50) NOT NULL ,
	[contractDate] [datetime] NOT NULL ,
	[contractPrice] [money] NOT NULL ,
	[contractPriceCurrencyCode] [int] NOT NULL ,
	[executionTerm] [datetime] NULL ,
	[realExecutionTerm] [datetime] NULL ,
	[terminationExecutionTerm] [datetime] NULL ,
	[terminationReason] [varchar] (255) NULL ,
	[titleProvider] [varchar] (255) NOT NULL ,
	[addressProvider] [varchar] (255) NULL ,
	[unpProvider] [varchar] (9) NULL ,
	[sellerOtherId] [varchar] (50) NULL ,
	[countryProvider] [varchar] (50) NOT NULL ,
	[accountProvider] [varchar] (50) NOT NULL ,
	[bankProviderCode] [varchar] (50) NOT NULL ,
	[bankProviderName] [varchar] (255) NULL 
)


declare @GIASLTContrPurchase table 
(
		[purchaseID] [varchar] (50) NOT NULL 
)


declare @GIASLTContrItems table
(
  [Id] int IDENTITY (1, 1) NOT NULL,
  finYear int NULL,
  dSct smallint NULL,
  dSSct smallint NULL,
  dKnd smallint NULL,
  dPrgr smallint NULL,
  chp smallint NULL,
  dPro smallint NULL,
  dSPro smallint NULL,
  dCtg smallint NULL,
  dArt smallint NULL,
  dSArt smallint NULL,
  dItm smallint NULL,
  finId int NULL,
  unk int NULL,
  customerId uniqueidentifier NULL,
  tkId int  NULL,
  budgetCost money NULL,
  fundCost money NULL,
  innerCost money NULL 
)


declare @GIASLTContrPayItems table
(
  [Id] int IDENTITY (1, 1) NOT NULL,
	[type] smallint Null,
	[date] datetime Null,
	[sum] money null,
  finYear int NULL,
  dSct smallint NULL,
  dSSct smallint NULL,
  dKnd smallint NULL,
  dPrgr smallint NULL,
  chp smallint NULL,
  dPro smallint NULL,
  dSPro smallint NULL,
  dCtg smallint NULL,
  dArt smallint NULL,
  dSArt smallint NULL,
  dItm smallint NULL,
  finId int NULL,
  unk int NULL,
  customerId uniqueidentifier NULL,
  tkId int  NULL,
  budgetCost money NULL,
  fundCost money NULL,
  innerCost money NULL 
) 


declare @GIASLTContrAcc table 
(
	[accountProvider] [varchar] (34) NOT NULL,
	[bankProviderCode] [varchar] (11) NOT NULL,
	[bankProviderName] [varchar] (255) NOT NULL,
	[accountProviderCurrencyCode] [int] NULL 
)



declare
	@FinId int,
	@UNK int,
	@IsUnion int,
	@FinId_union int,
	@FinId_union_gias int,
	@FinId_insert int,
	@TimeZone int,
	@date datetime,
	@ErrorCode int,
	@LtcontractId int,
	@TypePay int,
	@DatePay datetime,
	@SumPay money,
	@LTContrPayId int,
	@SeveralTK int,
	@SumBk money

 


set @FinId_union = 90000
set @Date = '1970-01-01 00:00:00'
set @IsUnion = 0
set @FinId_union_gias = 90001
set @TimeZone = 3

if exists
					(
						select 
							1
						from
							FinDept F
							join TreasurysAccounts A on
								F.FinId = A.FinId
							join Treasurys T on
								A.TreasuryCode = T.TreasuryCode and
								T.Active = 1
							where 
	 							Budget = 5
						)

set @IsUnion = 1 

--  financeSource>
declare
  @hr int,
	@idxml int

set  @hr = -1
set @idxml = -1

-- ����������  XML
	exec @hr = sp_xml_preparedocument @idxml output, @data

-- 
insert into @GIASLTContrItems
	( 
  [budgetCost], [fundCost], [innerCost], [finId], [unk], [customerId], [tkId], [finYear], 
	[dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], [dSPro], [dCtg], [dArt], [dSArt], [dItm]) 
select
  budgetCost, 
	fundCost, 
	innerCost, 
	cast(NullIf([finId], '') as int) as finId, 
	cast(NullIf([unk], '') as int) as unk, 
	NullIf([customerId], '') as customerId, 
	cast(NullIf([tkId], '') as int) as tkId, 
	cast(NullIf([finYear], '') as int) as finYear, 
	cast(NullIf([dSct], '') as smallint) as dSct, 
	cast(NullIf([dSSct], '') as smallint) as dSSct, 
	cast(NullIf([dKnd], '') as smallint) as dKnd, 
	cast(NullIf([dPrgr], '') as smallint) as dPrgr, 
	cast(NullIf([chp], '')as smallInt) as chp, 
	cast(NullIf([dPro], '') as smallint) as dPro, 
	cast(NullIf([dSPro], '') as smallint) as dSPro, 
	cast(NullIf([dCtg], '') as smallint) as dCtg, 
	cast(NullIf([dArt], '') as smallint) as dArt, 
	cast(NullIf([dSArt], '') as smallint) as dSArt, 
	cast(NullIf([dItm], '') as smallint) as dItm
from
  openxml (@idxml, '/contract/financeSource')
    with (
			[budgetCost] money './budgetCost', 
			[fundCost] money './fundCost', 
			[innerCost] money './innerCost', 
			[finId] varchar(50) './finId', 
			[unk] varchar(50) './unk', 
			[customerId] varchar(50) './customerId', 
			[tkId] varchar(50) './tkId', 
			[finYear] varchar(50) './finYear', 
			[dSct] varchar(50) './dSct', 
			[dSSct] varchar(50) './dSSct', 
			[dKnd] varchar(50) './dKnd', 
			[dPrgr] varchar(50) './dPrgr', 
			[chp] varchar(50) './chp', 
			[dPro] varchar(50) './dSSct', 
			[dSPro] varchar(50) './dSPro', 
			[dCtg] varchar(50) './dCtg', 
			[dArt] varchar(50) './dArt', 
			[dSArt] varchar(50) './dSArt', 
			[dItm] varchar(50) './dItm'
    ) I
		join Treasurys T on
		(T.treasuryCode = I.TKId) or
		(
			(T.treasuryCode/100 = I.tkId/100) and
			(	(T.TreasuryCode % 100 = 1 and I.TKId % 100 = 0) or
				(I.TkId % 100 = 1 and T.TreasuryCode % 100 = 0)
			)
		)
	where
		BudgetCost <> 0 and
		(	(@IsUnion = 0 and	FinId in (select FinId from TreasurysAccounts TA with (nolock))) or
		(@IsUnion = 1 and @FinId_union_gias = FinId)	) and
		T.Active = 1 

insert into @GIASLTContrPayItems
	( 
  [type], [date], [sum], [budgetCost], [fundCost], [innerCost], [finId], [unk], [customerId], [tkId], [finYear], 
	[dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], [dSPro], [dCtg], [dArt], [dSArt], [dItm]) 

select
  cast(NullIf([type], '') as smallint) as type, 
	cast(NullIf([date], '') as datetime) as [date], 
	[sum],	
  budgetCost, 
	fundCost, 
	innerCost, 
	cast(NullIf([finId], '') as int) as finId, 
	cast(NullIf([unk], '') as int) as unk, 
	NullIf([customerId], '') as customerId, 
	cast(NullIf([tkId], '') as int) as tkId, 
	cast(NullIf([finYear], '') as int) as finYear, 
	cast(NullIf([dSct], '') as smallint) as dSct, 
	cast(NullIf([dSSct], '') as smallint) as dSSct, 
	cast(NullIf([dKnd], '') as smallint) as dKnd, 
	cast(NullIf([dPrgr], '') as smallint) as dPrgr, 
	cast(NullIf([chp], '')as smallInt) as chp, 
	cast(NullIf([dPro], '') as smallint) as dPro, 
	cast(NullIf([dSPro], '') as smallint) as dSPro, 
	cast(NullIf([dCtg], '') as smallint) as dCtg, 
	cast(NullIf([dArt], '') as smallint) as dArt, 
	cast(NullIf([dSArt], '') as smallint) as dSArt, 
	cast(NullIf([dItm], '') as smallint) as dItm
from
  openxml (@idxml, '/contract/payments/financeSource')
    with (
			[type] varchar(50) '/contract/payments/type',
			[date] varchar(50) '/contract/payments/date',
			[sum] money '/contract/payments/sum',
			[budgetCost] money './budgetCost', 
			[fundCost] money './fundCost', 
			[innerCost] money './innerCost', 
			[finId] varchar(50) './finId', 
			[unk] varchar(50) './unk', 
			[customerId] varchar(50) './customerId', 
			[tkId] varchar(50) './tkId', 
			[finYear] varchar(50) './finYear', 
			[dSct] varchar(50) './dSct', 
			[dSSct] varchar(50) './dSSct', 
			[dKnd] varchar(50) './dKnd', 
			[dPrgr] varchar(50) './dPrgr', 
			[chp] varchar(50) './chp', 
			[dPro] varchar(50) './dSSct', 
			[dSPro] varchar(50) './dSPro', 
			[dCtg] varchar(50) './dCtg', 
			[dArt] varchar(50) './dArt', 
			[dSArt] varchar(50) './dSArt', 
			[dItm] varchar(50) './dItm'
    )



INSERT INTO @GIASLTContr([contrId], [etsId], [baseContrId], [unpCustomer],
                              [okogu], [titleContract], [contractNum], [contractDate],
                              [contractPrice], [contractPriceCurrencyCode], [executionTerm],
                              [realExecutionTerm], [terminationExecutionTerm], [terminationReason],
                              [titleProvider], [addressProvider], [unpProvider], [sellerOtherId],
                              [countryProvider], [accountProvider], [bankProviderCode], [bankProviderName])

select
	NullIf([GIASContrId],'') as contrId, 
	NullIf([etsId], '') as etsId, 
	NullIf([baseGIASContrId], '') as baseContrId, 
	NullIf([unpCustomer], '') as unpCustomer, 
	NullIf([okogu], '') as okogu, 
	NullIf([titleContract],'') as titleContract, 
	NullIf([contractNum], '') as contractNum, 
	case when contractDate = '' then null  else dateadd(hh,@TimeZone,dateadd(ss, cast(contractDate as bigint)/1000, @Date)) end as  contractDate,
	contractPrice, 
	cast(NullIf([contractPriceCurrencyCode], '') as int) as contractPriceCurrencyCode, 
	case when executionTerm = '' then null  else dateadd(hh,@TimeZone,dateadd(ss, cast(executionTerm as bigint)/1000, @Date)) end as  executionTerm,
	case when realExecutionTerm = '' then null  else dateadd(hh,@TimeZone,dateadd(ss, cast(realExecutionTerm as bigint)/1000, @Date)) end as  realExecutionTerm,
	case when terminationExecutionTerm = '' then null  else dateadd(hh,@TimeZone,dateadd(ss, cast(terminationExecutionTerm as bigint)/1000, @Date)) end as  terminationExecutionTerm,
	NullIf([terminationReason], '') as terminationReason, 
	NullIf([titleProvider], '') as titleProvider, 
 	NullIf([addressProvider], '') as addressProvider, 
	NullIf([unpProvider], '') as unpProvider, 
	NullIf([sellerOtherId], '') as sellerOtherId,
 	NullIf([countryProvider], '') as countryProvider, 
	NullIf([accountProvider], '') as accountProvider, 
	NullIf([bankProviderCode], '') as bankProviderCode,
 	NullIf([bankProviderName], '') as bankProviderName
from
	openXML(@idXML, '/contract' )
		with
		(
			[GIASContrId] [varchar] (50) './contractId',
			[etsId] [varchar] (50) './etsID' ,
			[baseGIASContrId] [varchar] (50) './baseContractId' ,
			[unpCustomer] [varchar] (9) './unpCustomer' ,
			[okogu] [varchar] (50) './okogu',
			[titleContract] [varchar] (255) './titleContract',
			[contractNum] [varchar] (50) './contractNum',
			[contractDate] [varchar] (50)'./contractDate' ,
			[contractPrice] money './contractPrice',
			[contractPriceCurrencyCode] [varchar] (50) './contractPriceCurrencyCode' ,
			[executionTerm] [varchar] (50) './executionTerm',
			[realExecutionTerm] [varchar] (50) './realExecutionTerm' ,
			[terminationExecutionTerm] [varchar] (50) './terminationExecutionTerm' ,
			[terminationReason] [varchar] (255) './terminationReason',
			[titleProvider] [varchar] (255) './titleProvider' ,
			[addressProvider] [varchar] (255) './addressProvider' ,
			[unpProvider] [varchar] (9) './unpProvider',
			[sellerOtherId] [varchar] (50) './sellerOtherId',
			[countryProvider] [varchar] (50) './countryProvider',
			[accountProvider] [varchar] (50) './accountProvider',
			[bankProviderCode] [varchar] (50) './bankProviderCode',
			[bankProviderName] [varchar] (255) './bankProviderName'
)


--purchaseIDs
insert into @GIASLTContrPurchase(PurchaseId)
select 
		NullIf(purchaseIDs, '') as purchaseIDs 
from
	openXML(@idXML, '/contract')
		with
		(
			purchaseIDs varchar(50) './purchaseIDs'	
		)

--contractAccounts
INSERT INTO @GIASLTContrAcc([accountProvider], [bankProviderCode], [bankProviderName], [accountProviderCurrencyCode])
select
	NullIf([accountProvider], '') as accountProvider, 
	NullIf([bankProviderCode], '') as bankProviderCode, 
	NullIf([bankProviderName], '') as BankProviderName, 
	cast(NullIf([accountProviderCurrencyCode], '') as int) as accountProviderCurrencyCode
from
	openXML(@idXML, 'contract/contractAccounts')
		with
		(
			[accountProvider] [varchar] (34) './accountProvider' ,
			[bankProviderCode] [varchar] (11) './bankProviderCoder',
			[bankProviderName] [varchar] (255) './bankProviderName',
			[accountProviderCurrencyCode] [varchar] (50) './accountProviderCurrencyCode' 
		)

-- ������ XML
if  @hr = 0 
  exec sp_xml_removedocument @idxml

if not exists(
							select 
								1
							from
								@GIASLTContrItems
						)
begin
	--raiserror('�������� ������������ ��� ������� ��', 16, -1) 
	return 1020
end


declare lstContr cursor for
select 
	case when @IsUnion = 1 and @FinId_union_gias = FinId then @FinId_union else FinId end as FinId_insert, max(UNK) as UNK, FinId, sum(budgetCost) as budgetCost
from
	@GIASLTContrItems r4
group by
	FinId

open lstContr

fetch next from lstContr into @FinId_Insert, @UNK, @FinId, @SumBk
while @@fetch_status = 0
begin

if exists( 
					select 
						1 
					from 
						GIASLTContr 
					where
						IdGiasContracts = @Id and
						FinId = @FinId_insert )

delete from c
from 
	GiasLtContr C
where
	IdGiasContracts = @Id and
	FinId = @FinId_insert
 



INSERT INTO [GIASLTContr]([GIASContrId], [etsId], [baseGIASContrId], 
													[unpCustomer], [okogu], [titleContract], [contractNum], 
													[contractDate], [BeginDate], [contractPrice], [contractPriceCurrencyCode], 
													[executionTerm], [realExecutionTerm], [terminationExecutionTerm], 
													[terminationReason], [titleProvider], [addressProvider], [unpProvider], 
													[sellerOtherId], [countryProvider], [accountProvider], [bankProviderCode], 
													[bankProviderName], [DateChangeState], 
													[DateLoadFromGIAS], [LastUser], 
													[IdGiasContracts], [StateCode], [FinId], [UNK])
select 
	[ContrId], [etsId], [baseContrId], 
	[unpCustomer], [okogu], [titleContract], [contractNum], 
	[contractDate], [contractDate] as BeginDate, [contractPrice], [contractPriceCurrencyCode], 
	[executionTerm], [realExecutionTerm], [terminationExecutionTerm], 
	[terminationReason], [titleProvider], [addressProvider], [unpProvider], 
	[sellerOtherId], [countryProvider], [accountProvider], [bankProviderCode], 
	[bankProviderName], getDate() as DateChangeState, getDate() as DateLoadFromGIAS,
	system_user as LastUser,@Id as idGiasContracts,
  -1 as StateCode,
	@FinId_insert,
	@UNK 
from
	@GiasLtContr C


if @@error <> 0 
	set @ErrorCode = 1
set @LTContractId = scope_identity()


--financeSource
INSERT INTO [dbo].[GIASLTContrItems]([MasterId], [budgetCost], [fundCost], [innerCost], 
																		[finId], [unk], [customerId], [tkId], [finYear], 
																		[dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], 
																		[dSPro], [dCtg], [dArt], [dSArt], [dItm])
select 
	@LTContractId as MasterID,
	[budgetCost], [fundCost], [innerCost], 
	@FinId_insert, [unk], [customerId], [tkId], [finYear], 
	[dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], 
	[dSPro], [dCtg], [dArt], [dSArt], [dItm]
from
	@GIASLTContrItems I
where
	FinId = @FinId and
	[dSct] is not Null and
	[dSSct] is not Null and
	[dKnd] is not null and
	[dPrgr] is not null and
	[chp] is not null and
	[dPro] is not null and
	[dSPro] is not null and
	[dCtg] is not null and
	[dArt] is not null and
	[dSArt] is not NULL and
	[dItm] is NOT NULL

if @@error <> 0 set @ErrorCode = 1


--purchaseIDs
insert into GIASLTContrPurchase(MasterID, PurchaseId)
select 
	@LTContractID, purchaseID 
from
	@GIASLTContrPurchase

if @@error <> 0 set @ErrorCode = 1

--contractAccounts
INSERT INTO [GIASLTContrAcc]([MasterId], [accountProvider], [bankProviderCode], [bankProviderName], [accountProviderCurrencyCode])
select
	@LTContractId as [MasterId],[accountProvider], [bankProviderCode], [bankProviderName], [accountProviderCurrencyCode]
from
	@GIASLTContrAcc

if @@error <> 0 set @ErrorCode = 1


--payments
declare lstPay cursor for
select
	type, [date], sum(budgetCost) as budgetCost
from
	@GIASLTContrPayItems P 
where
	FinId = @FinID
group by type, date

open lstPay

fetch next from lstPay into @TypePay, @DatePay, @SumPay
while @@fetch_status = 0
begin
	Insert into GiasLTContrPay(MasterId, type, date, sum)
	select @LTContractId as MasterId, @TypePay, @DatePay, @SumPay

	if @@error <> 0 set @ErrorCode = 1

	set @LTContrPayId = scope_identity()


	INSERT INTO [dbo].[GIASLTContrPayItems]([MasterId], [budgetCost], [fundCost], [innerCost], 
																				[finId], [unk], [customerId], [tkId], [finYear], 
																				[dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], 
																				[dSPro], [dCtg], [dArt], [dSArt], [dItm])
	select
		@LTContrPayId AS MasterID, [budgetCost], [fundCost], [innerCost], 
		@FinId_insert, [unk], [customerId], [tkId], [finYear], 
		[dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], 
		[dSPro], [dCtg], [dArt], [dSArt], [dItm]
	from
		 @GIASLTContrPayItems
	where
			type = @typePay and
			date = @datePay and
			FinId = @FinID and
			[dSct] is not Null and
			[dSSct] is not Null and
			[dKnd] is not null and
			[dPrgr] is not null and
			[chp] is not null and
			[dPro] is not null and
			[dSPro] is not null and
			[dCtg] is not null and
			[dArt] is not null and
			[dSArt] is not NULL and
			[dItm] is NOT NULL

	if @@error <> 0 set @ErrorCode = 1



fetch next from lstpay into @TypePay, @DatePay, @SumPay
end

close lstpay	
deallocate lstpay

fetch next from lstContr into @FinId_Insert, @UNK, @FinId, @SumBk
end
close lstContr	
deallocate lstContr

if @ErrorCode = 1
	return -1
else
	return 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

