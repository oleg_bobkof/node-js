SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gias_SaveInTableLT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gias_SaveInTableLT]
GO

CREATE procedure gias_SaveInTableLT
as

set nocount on
declare 
	@json nvarchar(4000),
	@parent_Id_m int,
	@date datetime,
	@contractId varchar(50),
	@LtcontractId int,
	@terminationExecutionterm datetime,
	@LTContrPayId int,
	@ErrorCode int,
	@TypePay int,
	@DatePay datetime,
	@SumPay money,
	@FinanceSource_id int,
	@Id int
				

declare @result table
(	
	 element_id INT  NOT NULL, 
   parent_ID INT,
   Object_ID INT,
   NAME VARCHAR(2000),
   StringValue VARCHAR(5000) NOT NULL,
   ValueType VARCHAR(10) NOT null
)

set @Date = '1970-01-01 00:00:00'

update giasContracts
	set
		Status = 2,
		spid = @@Spid
	where
		Status = 0

declare lst cursor for
select 
	contract, [Id]
from
	giascontracts
where
	status = 2 and
	spid = @@Spid
open lst

fetch next from lst into @Json, @Id
while @@fetch_status = 0
begin
BEGIN TRANSACTION
	delete from @result

	insert into @result(element_id, parent_id, object_id, Name, stringValue, valueType)
	select
		element_id, parent_id, object_id, Name, stringValue, valueType
	from
		dbo.parseJSon(@Json)

	select 
		@Parent_id_m = max(Parent_Id) 
	from 
		@result

	INSERT INTO [dbo].[GIASLTContr]([GIASContrId], [etsId], [baseGIASContrId], [unpCustomer], [okogu], 
																[titleContract], [contractNum], [contractDate], [BeginDate], [contractPrice], 
																[contractPriceCurrencyCode], [executionTerm], [realExecutionTerm], [terminationExecutionTerm], 
																[terminationReason], [titleProvider], [addressProvider], [unpProvider], [sellerOtherId], [countryProvider], 
																[accountProvider], [bankProviderCode], [bankProviderName], [DateChangeState], 
																[DateLoadFromGIAS], [LastUser])

	select 
		max(case when NAme = 'contractId' then StringValue else Null end) as contractd , 
		max(case when NAme = 'etsID' then StringValue else Null end) as etsID,
		max(case when NAme = 'baseContractId' then case when StringValue = 'Null' then null else StringValue end else Null end) as baseContractId,
		--max(case when NAme = 'planNumber' then cast(StringValue as varchar(50)) else Null end) as planNumber,
		max(case when NAme = 'unpCustomer' then StringValue else Null end) as unpCustomer,
		max(case when NAme = 'okogu' then StringValue else Null end) as okogu,
		max(case when NAme = 'titleContract' then StringValue else Null end) as titleContract,
		max(case when NAme = 'contractNum' then StringValue else Null end) as contractNum,
		max(case when NAme = 'contractDate' then	dateadd(ss, cast(StringValue as bigint)/1000, @Date) else Null end) as contractDate,
		max(case when NAme = 'contractDate' then dateadd(ss, cast(StringValue as bigint)/1000, @Date) else Null end) as contractDate,
		max(case when NAme = 'contractPrice' then cast(StringValue as money) else Null end) as contractPrice,
		max(case when NAme = 'contractPriceCurrencyCode' then cast(StringValue as int) else Null end) as contractPriceCurrencyCode,
		max(case when NAme = 'executionTerm' then  case when StringValue = 'null' then null else dateAdd(ss, cast(StringValue as bigint)/1000, @date) end else Null end) as executionTerm,
		max(case when NAme = 'realexecutionTerm' then case when StringValue = 'Null' then null else dateAdd(ss, cast(StringValue as bigint)/1000, @date) end else Null end) as realexecutionTerm,
		max(case when NAme = 'terminationExecutionTerm' then case when StringValue = 'null' then null else dateadd(ss, cast(StringValue as bigint)/1000, @Date) end else Null end) as terminationExecutionTerm,
		max(case when NAme = 'terminationReason' then case when StringValue = 'Null' then null else StringValue end else Null end) as terminationReason,
		max(case when NAme = 'titleProvider' then StringValue else Null end) as titleProvider,
		max(case when NAme = 'addressProvider' then StringValue else Null end) as addressProvider,
		max(case when NAme = 'unpProvider' then StringValue else Null end) as unpProvider,
		max(case when NAme = 'sellerOtherId' then case when StringValue = 'null' then null else StringValue end else Null end) as sellerOtherId,
		max(case when NAme = 'countryProvider' then StringValue else Null end) as countryProvider,
		max(case when NAme = 'accountProvider' then StringValue else Null end) as accountProvider,
		max(case when NAme = 'bankProviderCode' then StringValue else Null end) as bankProviderCode,
		max(case when NAme = 'bankProviderName' then case when StringValue = 'null' then null else StringValue end else Null end) as bankProviderName,
		getDate(),
		getDate(),
		system_user
	from 
		@result r
	where 
		Parent_id  = @Parent_id_m 
	
	if @@error <> 0 set @ErrorCode = 1

	set @LTContractId = scope_identity()

--financeSource
	INSERT INTO [dbo].[GIASLTContrItems]([MasterId], [budgetCost], [fundCost], [innerCost], 
																		[finId], [unk], [customerId], [tkId], [finYear], 
																		[dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], 
																		[dSPro], [dCtg], [dArt], [dSArt], [dItm])
	select 
	*
	from
		(
			select
				@LTContractId as MasterID,
				max(case when r4.NAme = 'budgetCost' then cast(r4.StringValue as money) else Null end) as budgetCost, 
				max(case when r4.NAme = 'fundCost' then cast(r4.StringValue as money) else Null end) as fundCost,
				max(case when r4.NAme = 'innerCost' then cast(r4.StringValue as money) else Null end) as innerCost,
				max(case when r4.NAme = 'finId' then cast(r4.StringValue as int) else Null end) as finId,
				max(case when r4.NAme = 'unk' then cast(r4.StringValue as int) else Null end) as unk,
				max(case when r4.NAme = 'customerId' then r4.StringValue else Null end) as customerId,
				max(case when r4.NAme = 'tkId' then cast(r4.StringValue as int) else Null end) as tkId,
				max(case when r4.NAme = 'finYear' then cast(r4.StringValue as int) else Null end) as finYear,
				max(case when r4.NAme = 'dSct' then cast(r4.StringValue as smallint) else Null end) as dSct,
				max(case when r4.NAme = 'dSSct' then cast(r4.StringValue as smallint) else Null end) as dSSct,
				max(case when r4.NAme = 'dKnd' then cast(r4.StringValue as smallint) else Null end) as dKnd,
				max(case when r4.NAme = 'dPrgr' then cast(r4.StringValue as smallint) else Null end) as dPrgr,
				max(case when r4.NAme = 'chp' then cast(r4.StringValue as smallint) else Null end) as chp,
				max(case when r4.NAme = 'dPro' then cast(r4.StringValue as smallint) else Null end) as dPro,
				max(case when r4.NAme = 'dSPro' then cast(r4.StringValue as smallint) else Null end) as dSPro,
				max(case when r4.NAme = 'dCtg' then cast(r4.StringValue as smallint) else Null end) as dCtg,
				max(case when r4.NAme = 'dArt' then cast(r4.StringValue as smallint) else Null end) as dArt,
				max(case when r4.NAme = 'dSArt' then cast(r4.StringValue as smallint) else Null end) as dSArt,
				max(case when r4.NAme = 'dItm' then cast(r4.StringValue as smallint) else Null end) as dItm
			from
				@result r --array
				join @result r1 on --object
					r.Object_id = r1.Parent_Id
				join @result r4 on --object
					r1.Object_id = r4.Parent_Id 
				where
					r.Name = 'financeSource' and
					r.Parent_id  = @Parent_Id_m 
				group by 
					r4.parent_id
		) I
	where
		FinId in (select FinId from TreasurysAccounts TA with (nolock))

	if @@error <> 0 set @ErrorCode = 1


	Update c
	set
		C.FinId = I.FinId,
		C.UNK = I.UNK
	from
		GIASLTContr C
		join (
				select 
					MasterId, FinId, UNK 
				from
					GIASLTContrItems I
				) I on C.Id = I.MasterId
	where
		C.Id = @LTContractId


--purchaseIDs
	insert into GIASLTContrPurchase(MasterID, PurchaseId)
	select 
		@LTContractID, cast(r1.StringValue as varchar(50)) as purchaseIDs 	
	from 
		@result R
		join @result r1 on
			r.Object_id = r1.Parent_id
	where 
		R.Name = 'purchaseIDs'

	if @@error <> 0 set @ErrorCode = 1
	
--payments						
	declare lstPay cursor for
	select 
		max(case when r2.NAme = 'type' then case when r2.stringValue = 'null' then null else cast(r2.StringValue as int) end else Null end) as type, 
		max(case when r2.NAme = 'date' then dateadd(ss, cast(r2.StringValue as bigint)/1000, @Date) else null end) as [date], 
 		max(case when r2.NAme = 'sum' then cast(r2.StringValue as money) else Null end) as [sum],
		max(case when r2.NAme = 'financeSource' then cast(r2.StringValue as int) else Null end) as [financeSource]
	from
		@result r
		join @result r1 on
			r.Object_id = r1.Parent_id
		join @result r2 on
			r1.Object_id = r2.Parent_Id
		
	where
		r.Name = 'payments' and
		exists(
						select 
							1
						from
							@result r3 -- object
							join @result r4 on
								r4.Parent_id = r3.Object_id
						where
							r3.Parent_id = r2.Object_Id and  
							r4.Name = 'finId' and
							cast(r4.StringValue as int) in (select FinId from TreasurysAccounts TA with (nolock))
					)							
	group by 
		r2.parent_id

	open lstPay

	fetch next from lstPay into @TypePay, @DatePay, @SumPay, @FinanceSource_id
	while @@fetch_status = 0
	begin
		if exists(select 
			*
		from
			(
				select
					@LTContrPayId AS MasterID, 
					max(case when r4.NAme = 'budgetCost' then cast(r4.StringValue as money) else Null end) as budgetCost, 
					max(case when r4.NAme = 'fundCost' then cast(r4.StringValue as money) else Null end) as fundCost,
					max(case when r4.NAme = 'innerCost' then cast(r4.StringValue as money) else Null end) as innerCost,
					max(case when r4.NAme = 'finId' then cast(r4.StringValue as int) else Null end) as finId,
					max(case when r4.NAme = 'unk' then cast(r4.StringValue as int) else Null end) as unk,
					max(case when r4.NAme = 'customerId' then r4.StringValue else Null end) as customerId,
					max(case when r4.NAme = 'tkId' then cast(r4.StringValue as int) else Null end) as tkId,
					max(case when r4.NAme = 'finYear' then cast(r4.StringValue as int) else Null end) as finYear,

					max(case when r4.NAme = 'dSct' then cast(r4.StringValue as smallint) else Null end) as dSct,
					max(case when r4.NAme = 'dSSct' then cast(r4.StringValue as smallint) else Null end) as dSSct,

					max(case when r4.NAme = 'dKnd' then cast(r4.StringValue as smallint) else Null end) as dKnd,
					max(case when r4.NAme = 'dPrgr' then cast(r4.StringValue as smallint) else Null end) as dPrgr,
					max(case when r4.NAme = 'chp' then cast(r4.StringValue as smallint) else Null end) as chp,
					max(case when r4.NAme = 'dPro' then cast(r4.StringValue as smallint) else Null end) as dPro,
					max(case when r4.NAme = 'dSPro' then cast(r4.StringValue as smallint) else Null end) as dSPro,
					max(case when r4.NAme = 'dCtg' then cast(r4.StringValue as smallint) else Null end) as dCtg,
					max(case when r4.NAme = 'dArt' then cast(r4.StringValue as smallint) else Null end) as dArt,
					max(case when r4.NAme = 'dSArt' then cast(r4.StringValue as smallint) else Null end) as dSArt,
					max(case when r4.NAme = 'dItm' then cast(r4.StringValue as smallint) else Null end) as dItm
		
				from
					@result r --array
					join @result r1 on --object
						r.Object_id = r1.Parent_id
					join @result r2 on --field+array
						r1.Object_id = r2.Parent_Id
					join @result r3 on -- object
						r3.Parent_id = r2.Object_Id
					join @result r4 on
						r4.Parent_id = r3.Object_id	
				where
					r.Name = 'payments' and
					r.ValueType = 'array' and
					r2.Object_Id = @FinanceSource_id
				group by 
					r4.parent_id
			) P
	where FinId in (select FinId from TreasurysAccounts TA with (nolock))) 
		Insert into GiasLTContrPay(MasterId, type, date, sum)
		select @LTContractId as MasterId, @TypePay, @DatePay, @SumPay

		if @@error <> 0 set @ErrorCode = 1

		set @LTContrPayId = scope_identity()


		INSERT INTO [dbo].[GIASLTContrPayItems]([MasterId], [budgetCost], [fundCost], [innerCost], 
																				[finId], [unk], [customerId], [tkId], [finYear], 
																				[dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], 
																				[dSPro], [dCtg], [dArt], [dSArt], [dItm])

		select 
			*
		from
			(
				select
					@LTContrPayId AS MasterID, 
					max(case when r4.NAme = 'budgetCost' then cast(r4.StringValue as money) else Null end) as budgetCost, 
					max(case when r4.NAme = 'fundCost' then cast(r4.StringValue as money) else Null end) as fundCost,
					max(case when r4.NAme = 'innerCost' then cast(r4.StringValue as money) else Null end) as innerCost,
					max(case when r4.NAme = 'finId' then cast(r4.StringValue as int) else Null end) as finId,
					max(case when r4.NAme = 'unk' then cast(r4.StringValue as int) else Null end) as unk,
					max(case when r4.NAme = 'customerId' then r4.StringValue else Null end) as customerId,
					max(case when r4.NAme = 'tkId' then cast(r4.StringValue as int) else Null end) as tkId,
					max(case when r4.NAme = 'finYear' then cast(r4.StringValue as int) else Null end) as finYear,

					max(case when r4.NAme = 'dSct' then cast(r4.StringValue as smallint) else Null end) as dSct,
					max(case when r4.NAme = 'dSSct' then cast(r4.StringValue as smallint) else Null end) as dSSct,

					max(case when r4.NAme = 'dKnd' then cast(r4.StringValue as smallint) else Null end) as dKnd,
					max(case when r4.NAme = 'dPrgr' then cast(r4.StringValue as smallint) else Null end) as dPrgr,
					max(case when r4.NAme = 'chp' then cast(r4.StringValue as smallint) else Null end) as chp,
					max(case when r4.NAme = 'dPro' then cast(r4.StringValue as smallint) else Null end) as dPro,
					max(case when r4.NAme = 'dSPro' then cast(r4.StringValue as smallint) else Null end) as dSPro,
					max(case when r4.NAme = 'dCtg' then cast(r4.StringValue as smallint) else Null end) as dCtg,
					max(case when r4.NAme = 'dArt' then cast(r4.StringValue as smallint) else Null end) as dArt,
					max(case when r4.NAme = 'dSArt' then cast(r4.StringValue as smallint) else Null end) as dSArt,
					max(case when r4.NAme = 'dItm' then cast(r4.StringValue as smallint) else Null end) as dItm
		
				from
					@result r --array
					join @result r1 on --object
						r.Object_id = r1.Parent_id
					join @result r2 on --field+array
						r1.Object_id = r2.Parent_Id
					join @result r3 on -- object
						r3.Parent_id = r2.Object_Id
					join @result r4 on
						r4.Parent_id = r3.Object_id	
				where
					r.Name = 'payments' and
					r.ValueType = 'array' and
					r2.Object_Id = @FinanceSource_id
				group by 
					r4.parent_id
			) P
	where FinId in (select FinId from TreasurysAccounts TA with (nolock))


	if @@error <> 0 set @ErrorCode = 1

	fetch next from lstpay into @TypePay, @DatePay, @SumPay, @FinanceSource_id
	end
	close lstpay	
	deallocate lstpay

if @ErrorCode = 1
	begin
	ROLLBACK
	Update G 
		set 
			Status = 0,
			spid = null
		from
			giasContracts G
		where
			id = @ID
	end
else
	begin
		COMMIT
		Update G 
		set 
			Status = 1,
			spid = null
		from
			giasContracts G
		where
			id = @ID
	end

fetch next from lst into @Json, @Id
end
close lst
deallocate lst














	
 



 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

