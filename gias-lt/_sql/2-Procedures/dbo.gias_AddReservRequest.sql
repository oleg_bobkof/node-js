SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gias_AddReservRequest]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gias_AddReservRequest]
GO
-- Version 3; Date 27.03.2020; LastEditor Oleg
CREATE procedure gias_AddReservRequest(
	@requestId uniqueidentifier, @data text, @dateRequest datetime)
as
set nocount on

declare
  	@hr int,
	@idxml int,
	@dateLastRequest datetime

declare @giasReserv table
(
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[purchaseGiasId] [uniqueidentifier] NOT NULL ,
	[requestId] [uniqueidentifier] NOT NULL ,
	[etpId] [varchar] (50) NOT NULL ,
	[operation] [smallint] NOT NULL ,
	[isDuty] [bit] NOT NULL ,
	[isSufficientlyByASFR] bit  null DEFAULT (0),
	[DateRequest] datetime not null
)

declare @giasReservDetail table
(
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[budgetCost] [money] NOT NULL ,
	[canBeReservedByASFR] [money] null  DEFAULT (0),
	[finYear] [int] NOT NULL ,
	[dSct] [smallint] NOT NULL ,
	[dSSct] [smallint] NOT NULL ,
	[dKnd] [smallint] NOT NULL ,
	[dPrgr] [smallint] NOT NULL ,
	[chp] [smallint] NOT NULL ,
	[dPro] [smallint] NOT NULL ,
	[dSPro] [smallint] NOT NULL ,
	[dCtg] [smallint] NOT NULL ,
	[dArt] [smallint] NOT NULL ,
	[dSArt] [smallint] NOT NULL ,
	[dItm] [smallint] NOT NULL ,
	[finId] [int] NOT NULL ,
	[unk] [int] NOT NULL ,
	[customerId] [uniqueidentifier] NULL,
	[tkId] [int] NOT NULL ,
	[UNP] [varchar] (9) not null,
	[setId] [int] null,
	[isSufficientlyByASFR] [bit] null DEFAULT (0),
	[uid] [varchar] (50) not null 
	
)


declare 
	@Id int


if exists(select 1	from GiasReservRequest where requestId = @requestId)
goto EndProc


exec @hr = sp_xml_preparedocument @idxml output, @data

if @hr  = 0
begin

insert into @GiasReserv([purchaseGiasId], [requestId], [etpId], [operation], [isDuty], dateRequest)
select 
	R.[purchaseGiasId], 
	R.[requestId], 
	R.[etpId], 
	R.[operation], 
	R.[isDuty],
	@DateRequest
from
	dbo.gias_xml_Reserv(@idxml, @data) R


insert into @giasReservDetail([budgetCost], [finYear], [dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], [dSPro], [dCtg], [dArt], [dSArt], [dItm], [finId], [unk], [customerId], [tkId], [UNP], [SetId], [uid])
select 
	I.[budgetCost], 
	I.[finYear], 
	I.[dSct], 
	I.[dSSct], 
	I.[dKnd], 
	I.[dPrgr], 
	I.[chp], 
	I.[dPro], 
	I.[dSPro], 
	I.[dCtg], 
	I.[dArt], 
	I.[dSArt], 
	I.[dItm], 
	I.[finId], 
	I.[unk], 
	I.[customerId], 
	I.[tkId], 
	I.[UNP],
	dbo.fUtils_YearToSetId(I.FinYear) as SetId,
	I.uid
FROM
	dbo.gias_xml_ReservDetail(@idxml, @data) I 
	join Treasurys T on
		(T.treasuryCode = I.TKId) or
		(
			(T.treasuryCode/100 = I.tkId/100) and
			(	(T.TreasuryCode % 100 = 1 and I.TKId % 100 = 0) or
				(I.TkId % 100 = 1 and T.TreasuryCode % 100 = 0) or
				(IsNull(T.TreasuryCodedForGias,-1) = I.TKId)
			)
		)
where
	FinId in (select FinId from TreasurysAccounts TA with (nolock)) and
	T.Active = 1 

exec sp_xml_removedocument @idxml
end


select
	@dateLastRequest = IsNull(max(DateRequest),0)
from
	GiasReservRequest
	 

if exists (select 1 from
		@giasReserv R
		join @GiasReservDetail D on
			1 = 1
	where
		R.Operation = 1 and DateRequest > @dateLastRequest)
begin

Update D
set
	isSufficientlyByASFR = case when (IsNull(R.RestPlan,0) - IsNull(t.BudgetCost,0) - D.BudgetCost > 0) then 1 else case when isDuty = 0 and IsNull(R.RestPlan,0) - IsNull(t.BudgetCost,0) > 0 then 1 else 0 end end, 
	canBeReservedByASFR =case when (IsNull(R.RestPlan,0) - IsNull(t.BudgetCost,0) - D.BudgetCost > 0) then D.BudgetCost else case when isDuty = 0 and IsNull(R.RestPlan,0) - IsNull(t.BudgetCost,0) > 0 then IsNull(R.RestPlan,0) - IsNull(t.BudgetCost,0) else 0 end end  	 
from
	@giasReservDetail D 
	left join RPlan_Rest R on
		R.SetId = D.SetId and
		R.FinId = D.FinId and
		R.Organization = D.Unk and 
		R.Chapter = D.chp and 
		R.[Section] = D.dSct and
		R.SubSection = D.dSSct and
		R.[View] = D.dKnd and
		R.Paragraph = D.dPrgr and
		R.Program = D.dPro and
		R.SubProgram = D.dSPro and 
		R.Article = D.dArt and
		R.SubArticle = D.dSArt and 
		R.Item = D.dItm
	join @giasReserv G on
		1 = 1	
	left join 
						(
							select
								sum(I.[budgetCost]) budgetCost,  
								I.[dSct], 
								I.[dSSct], 
								I.[dKnd], 
								I.[dPrgr], 
								I.[chp], 
								I.[dPro], 
								I.[dSPro], 
								I.[dCtg], 
								I.[dArt], 
								I.[dSArt], 
								I.[dItm], 
								I.[finId], 
								I.[unk], 		
								SetId 
							from
								GiasLtReserv I
								join @giasReserv R on
									1 = 1				
							where
								I.purchaseGiasId <> R.purchaseGiasId  
							group by
								I.[dSct], I.[dSSct], I.[dKnd], I.[dPrgr], I.[chp], I.[dPro], I.[dSPro], I.[dCtg], I.[dArt], I.[dSArt], I.[dItm], I.[finId], I.[unk], I.SetId 				
						) T on
										D.SetId = T.SetId and
										D.FinId = T.FinId and
										D.Unk = T.Unk and 
										D.chp = T.chp and 
										D.dSct = T.dSct and
										D.dSSct = T.dSSct and
										D.[dKnd] = T.dKnd and
										D.dPrgr = T.dPrgr and
										D.dPro = T.dPro and
										D.dSPro = T.dSPro and 
										D.dArt = T.dArt and
										D.dSArt = T.dSArt and 
										D.dItm = T.dItm


if not exists (select 1 from @giasReservDetail where isSufficientlyByASFR = 0 )
begin

Update R
set
	isSufficientlyByASFR = 1
from
	@GiasReserv R

end	 

end



insert into GiasReservRequest([purchaseGiasId], [requestId], [etpId], [operation], [isDuty], [isSufficientlyByASFR], [status], DateRequest)
select 
	R.[purchaseGiasId], 
	R.[requestId], 
	R.[etpId], 
	R.[operation], 
	R.[isDuty], 
	isSufficientlyByASFR,
	case when (	(DateRequest > @dateLastRequest) and (	(isSufficientlyByASFR = 1) or (Operation = 2) )) then 0 else -1 end,
	DateRequest
from
	@GiasReserv R

set @Id = scope_Identity() 


insert into giasReservRequestDet([masterId], [budgetCost], [dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], [dSPro], [dCtg], [dArt], [dSArt], [dItm], [finId], [unk], [customerId], [tkId], [UNP], [setId], [canBeReservedByASFR], [isSufficientlyByASFR], [uid])
select 
	@Id as MasterId,
	I.[budgetCost],  
	I.[dSct], 
	I.[dSSct], 
	I.[dKnd], 
	I.[dPrgr], 
	I.[chp], 
	I.[dPro], 
	I.[dSPro], 
	I.[dCtg], 
	I.[dArt], 
	I.[dSArt], 
	I.[dItm], 
	I.[finId], 
	I.[unk], 
	I.[customerId], 
	I.[tkId], 
	I.[UNP],
	I.[SetId],
	I.[canBeReservedByASFR],
	I.[isSufficientlyByASFR],
	I.uid
FROM
	@giasReservDetail I


EndProc:
	select
			I.[uid] as _id,
			D.[Year] as finYear,  
			I.[dSct], 
			I.[dSSct], 
			I.[dKnd], 
			I.[dPrgr], 
			I.[chp], 
			I.[dPro], 
			I.[dSPro], 
			I.[dCtg], 
			I.[dArt], 
			I.[dSArt], 
			I.[dItm], 
			I.[finId], 
			I.[unk], 
			I.[customerId], 
			I.[tkId], 
			I.[UNP],
			I.[isSufficientlyByASFR]
	from
		giasReservRequestDet I
		join giasReservRequest R on
			I.MasterId = R.Id
		left join DataSet D on
			I.SetId = D.SetId
	where
		R.requestId = @requestId 
	
	return
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

