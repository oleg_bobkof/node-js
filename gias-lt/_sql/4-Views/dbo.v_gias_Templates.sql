SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[v_gias_Templates]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[v_gias_Templates]
GO
-- Version 1; Date 04.01.2020; LastEditor Oleg
CREATE VIEW dbo.v_gias_Templates
AS

SELECT DISTINCT 
    TA.TreasuryCode AS tkId, TA.finId
FROM         
    TreasurysAccounts TA JOIN Treasurys T ON 
        TA.TreasuryCode = T.TreasuryCode
WHERE    
    T.ACTIVE = 1

UNION

SELECT 
    tkId, finId 
FROM
    GiasTemplates


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[v_gias_Templates]  TO [public]
GO

