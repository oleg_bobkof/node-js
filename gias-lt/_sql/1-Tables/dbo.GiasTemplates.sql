if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GiasTemplates]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[GiasTemplates]
GO

CREATE TABLE [dbo].[GiasTemplates] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[tkId] [smallint] NOT NULL ,
	[finId] [int] NOT NULL ,
	CONSTRAINT [PK_GiasTemplates] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO

 CREATE  UNIQUE  INDEX [IX_GiasTemplates] ON [dbo].[GiasTemplates]([tkId], [finId]) ON [PRIMARY]
GO

GRANT  REFERENCES ,  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[GiasTemplates]  TO [public]
GO


