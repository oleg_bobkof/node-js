if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LT_DBAlloc]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[LT_DBAlloc]
GO

CREATE TABLE [dbo].[LT_DBAlloc] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[ServerName] [varchar] (250) COLLATE Cyrillic_General_CI_AS NULL ,
	[DBName] [varchar] (250) COLLATE Cyrillic_General_CI_AS NULL ,
	[Year] [int] NULL ,
	[SetId] [int] NULL ,
	[TypeKZ] [varchar] (50) COLLATE Cyrillic_General_CI_AS NULL ,
	[Budget] [varchar] (50) COLLATE Cyrillic_General_CI_AS NULL ,
	[CodeBudget] [int] NULL ,
	[FinId] [int] NULL ,
	[Notes] [varchar] (50) COLLATE Cyrillic_General_CI_AS NULL ,
	[TreasuryCode] [int] NULL 
) ON [PRIMARY]
GO


