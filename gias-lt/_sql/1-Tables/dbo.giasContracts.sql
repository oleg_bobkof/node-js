if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_giasContractsDocuments_giasContracts]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[giasContractsDocuments] DROP CONSTRAINT FK_giasContractsDocuments_giasContracts
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[giasContracts]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[giasContracts]
GO

CREATE TABLE [dbo].[giasContracts] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[contractId] [uniqueidentifier] NOT NULL ,
	[terminationExecutionTerm] [datetime] NULL ,
	[contract] [ntext] COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[financeSource] [ntext] COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[payments] [ntext] COLLATE Cyrillic_General_CI_AS NULL ,
	[contractPositions] [ntext] COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[contractDocuments] [ntext] COLLATE Cyrillic_General_CI_AS NULL ,
	[contractData] [ntext] COLLATE Cyrillic_General_CI_AS NULL ,
	[contractXML] [ntext] COLLATE Cyrillic_General_CI_AS NULL ,
	[contractJSON] [ntext] COLLATE Cyrillic_General_CI_AS NULL ,
	[status] [smallint] NOT NULL ,
	[loadDate] [datetime] NOT NULL ,
	[spid] [smallint] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[giasContracts] WITH NOCHECK ADD 
	CONSTRAINT [PK_giasContracts] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[giasContracts] ADD 
	CONSTRAINT [DF_giasContracts_status] DEFAULT ((-2)) FOR [status],
	CONSTRAINT [DF_giasContracts_loadDate] DEFAULT (getdate()) FOR [loadDate]
GO

CREATE  INDEX [ix_contractId] ON [dbo].[giasContracts]([contractId]) ON [PRIMARY]
GO

GRANT  REFERENCES ,  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[giasContracts]  TO [public]
GO