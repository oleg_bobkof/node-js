if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[giasContractsDocuments]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[giasContractsDocuments]
GO

CREATE TABLE [dbo].[giasContractsDocuments] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[masterId] [int] NOT NULL ,
	[fileType] [smallint] NOT NULL ,
	[contractDocuments] [ntext] COLLATE Cyrillic_General_CI_AS NULL ,
	[contractData] [ntext] COLLATE Cyrillic_General_CI_AS NULL ,
	CONSTRAINT [PK_giasContractsDocuments] PRIMARY KEY  CLUSTERED 
	(
		[id]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_giasContractsDocuments_giasContracts] FOREIGN KEY 
	(
		[masterId]
	) REFERENCES [dbo].[giasContracts] (
		[Id]
	) ON DELETE CASCADE 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

 CREATE  INDEX [IX_giasCD_masterid] ON [dbo].[giasContractsDocuments]([masterId]) ON [PRIMARY]
GO

GRANT  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[giasContractsDocuments]  TO [public]
GO


