if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GIASLTContrPay]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[GIASLTContrPay]
GO

CREATE TABLE [dbo].[GIASLTContrPay] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[MasterId] [int] NOT NULL ,
	[type] [int] NULL ,
	[date] [datetime] NOT NULL ,
	[sum] [money] NOT NULL ,
	CONSTRAINT [PK_GIASLTContrPay] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_GIASLTContrPay_GIASLTContr] FOREIGN KEY 
	(
		[MasterId]
	) REFERENCES [dbo].[GIASLTContr] (
		[Id]
	) ON DELETE CASCADE 
) ON [PRIMARY]
GO


exec sp_addextendedproperty N'MS_Description', '������ �������� �� �������� ����', N'user', N'dbo', N'table', N'GIASLTContrPay'

GO

exec sp_addextendedproperty N'MS_Description', '������������� ������ ������� ��������', N'user', N'dbo', N'table', N'GIASLTContrPay', N'column', N'Id'
GO
exec sp_addextendedproperty N'MS_Description', '������ �� ������ � ������� GIASLTContr', N'user', N'dbo', N'table', N'GIASLTContrPay', N'column', N'MasterId'
GO
exec sp_addextendedproperty N'MS_Description', '��� �������: 1-���������, 2-������', N'user', N'dbo', N'table', N'GIASLTContrPay', N'column', N'type'
GO
exec sp_addextendedproperty N'MS_Description', '���� �������', N'user', N'dbo', N'table', N'GIASLTContrPay', N'column', N'date'
GO
exec sp_addextendedproperty N'MS_Description', '������ �������', N'user', N'dbo', N'table', N'GIASLTContrPay', N'column', N'sum'


GO


