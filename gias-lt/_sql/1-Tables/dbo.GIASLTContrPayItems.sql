if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GIASLTContrPayItems]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[GIASLTContrPayItems]
GO

CREATE TABLE [dbo].[GIASLTContrPayItems] (
	[MasterId] [int] NOT NULL ,
	[budgetCost] [money] NOT NULL ,
	[fundCost] [money] NOT NULL ,
	[innerCost] [money] NOT NULL ,
	[finId] [int] NOT NULL ,
	[unk] [int] NOT NULL ,
	[customerId] [varchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[tkId] [int] NOT NULL ,
	[finYear] [int] NOT NULL ,
	[dSct] [smallint] NOT NULL ,
	[dSSct] [smallint] NOT NULL ,
	[dKnd] [smallint] NOT NULL ,
	[dPrgr] [smallint] NOT NULL ,
	[chp] [smallint] NOT NULL ,
	[dPro] [smallint] NOT NULL ,
	[dSPro] [smallint] NOT NULL ,
	[dCtg] [smallint] NOT NULL ,
	[dArt] [smallint] NOT NULL ,
	[dSArt] [smallint] NOT NULL ,
	[dItm] [smallint] NOT NULL ,
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	CONSTRAINT [PK_GIASLTContrPayItems] PRIMARY KEY  CLUSTERED 
	(
		[id]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_GIASLTContrPayItems_GIASLTContrPay] FOREIGN KEY 
	(
		[MasterId]
	) REFERENCES [dbo].[GIASLTContrPay] (
		[Id]
	) ON DELETE CASCADE 
) ON [PRIMARY]
GO


exec sp_addextendedproperty N'MS_Description', '������� �� ��������� ������������� ������� �� �������� ����', N'user', N'dbo', N'table', N'GIASLTContrPayItems'

GO

exec sp_addextendedproperty N'MS_Description', '������ �� ������ ������� ��������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'MasterId'
GO
exec sp_addextendedproperty N'MS_Description', '��������� ��������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'budgetCost'
GO
exec sp_addextendedproperty N'MS_Description', '�������� ��������������� ������������ ������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'fundCost'
GO
exec sp_addextendedproperty N'MS_Description', '����������� ��������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'innerCost'
GO
exec sp_addextendedproperty N'MS_Description', '��� ������� � ������������ � �������� ������������ �������� ���������� �������� �� 16 ������� 2005 �. �410 ��� ����������� ����������� ����� �������.', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'finId'
GO
exec sp_addextendedproperty N'MS_Description', '��� ���������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'unk'
GO
exec sp_addextendedproperty N'MS_Description', '��� ���������������� ��������� � ����', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'customerId'
GO
exec sp_addextendedproperty N'MS_Description', '��� ���������������� ������������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'tkId'
GO
exec sp_addextendedproperty N'MS_Description', '��� ��������������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'finYear'
GO
exec sp_addextendedproperty N'MS_Description', '��� �������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'dSct'
GO
exec sp_addextendedproperty N'MS_Description', '��� ����������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'dSSct'
GO
exec sp_addextendedproperty N'MS_Description', '��� ����', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'dKnd'
GO
exec sp_addextendedproperty N'MS_Description', '��� ���������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'dPrgr'
GO
exec sp_addextendedproperty N'MS_Description', '��� �����', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'chp'
GO
exec sp_addextendedproperty N'MS_Description', '��� ���������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'dPro'
GO
exec sp_addextendedproperty N'MS_Description', '��� ������������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'dSPro'
GO
exec sp_addextendedproperty N'MS_Description', '��� ���������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'dCtg'
GO
exec sp_addextendedproperty N'MS_Description', '��� ������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'dArt'
GO
exec sp_addextendedproperty N'MS_Description', '��� ���������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'dSArt'
GO
exec sp_addextendedproperty N'MS_Description', '��� ��������', N'user', N'dbo', N'table', N'GIASLTContrPayItems', N'column', N'dItm'


GO


