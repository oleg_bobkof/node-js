if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GIASLTContrPurchase]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[GIASLTContrPurchase]
GO

CREATE TABLE [dbo].[GIASLTContrPurchase] (
	[MasterId] [int] NOT NULL ,
	[purchaseID] [varchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	CONSTRAINT [PK_GIASLTContrPurchase] PRIMARY KEY  CLUSTERED 
	(
		[MasterId],
		[purchaseID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


exec sp_addextendedproperty N'MS_Description', '������ ��������������� ���� ��������������� �������', N'user', N'dbo', N'table', N'GIASLTContrPurchase'

GO

exec sp_addextendedproperty N'MS_Description', '������ �� ������ � ������� GIASLTContr', N'user', N'dbo', N'table', N'GIASLTContrPurchase', N'column', N'MasterId'
GO
exec sp_addextendedproperty N'MS_Description', '������������� ���� ��������������� �������', N'user', N'dbo', N'table', N'GIASLTContrPurchase', N'column', N'purchaseID'


GO


