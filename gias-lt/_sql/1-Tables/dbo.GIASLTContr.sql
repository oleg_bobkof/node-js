if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GIASLTContr]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[GIASLTContr]
GO

CREATE TABLE [dbo].[GIASLTContr] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[GIASContrId] [varchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[etsId] [varchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[baseGIASContrId] [varchar] (50) COLLATE Cyrillic_General_CI_AS NULL ,
	[unpCustomer] [varchar] (9) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[okogu] [varchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[titleContract] [varchar] (255) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[contractNum] [varchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[contractDate] [datetime] NOT NULL ,
	[BeginDate] [datetime] NOT NULL ,
	[contractPrice] [money] NOT NULL ,
	[contractPriceCurrencyCode] [int] NOT NULL ,
	[executionTerm] [datetime] NULL ,
	[realExecutionTerm] [datetime] NULL ,
	[terminationExecutionTerm] [datetime] NULL ,
	[terminationReason] [varchar] (255) COLLATE Cyrillic_General_CI_AS NULL ,
	[titleProvider] [varchar] (255) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[addressProvider] [varchar] (255) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[unpProvider] [varchar] (9) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[sellerOtherId] [varchar] (50) COLLATE Cyrillic_General_CI_AS NULL ,
	[countryProvider] [varchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[accountProvider] [varchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[bankProviderCode] [varchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL ,
	[bankProviderName] [varchar] (255) COLLATE Cyrillic_General_CI_AS NULL ,
	[StateCode] [int] NULL CONSTRAINT [DF__GIASLTCon__State__44586380] DEFAULT (0),
	[ReasonOfRefusal] [varchar] (500) COLLATE Cyrillic_General_CI_AS NULL ,
	[DateChangeState] [datetime] NULL ,
	[LTSysNum] [int] NULL ,
	[UNK] [int] NULL ,
	[FinId] [int] NULL ,
	[DateLoadFromGIAS] [datetime] NULL CONSTRAINT [DF__GIASLTCon__DateL__454C87B9] DEFAULT (getdate()),
	[LastUser] [sysname] NOT NULL ,
	CONSTRAINT [PK_GIASLTContr] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


exec sp_addextendedproperty N'MS_Description', '�������� ����', N'user', N'dbo', N'table', N'GIASLTContr'

GO

exec sp_addextendedproperty N'MS_Description', '������������� ������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'Id'
GO
exec sp_addextendedproperty N'MS_Description', '������������� ���� �������� ��� ��������������� ����������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'GIASContrId'
GO
exec sp_addextendedproperty N'MS_Description', '��� �������� �� ���', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'etsId'
GO
exec sp_addextendedproperty N'MS_Description', '������������� ���� �������� (����������� ��� ��������������� ����������)', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'baseGIASContrId'
GO
exec sp_addextendedproperty N'MS_Description', '��� ���������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'unpCustomer'
GO
exec sp_addextendedproperty N'MS_Description', '��� ������������� ��������������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'okogu'
GO
exec sp_addextendedproperty N'MS_Description', '������������ �������� ��������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'titleContract'
GO
exec sp_addextendedproperty N'MS_Description', '����� �������� ��� ��������������� ����������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'contractNum'
GO
exec sp_addextendedproperty N'MS_Description', '���� �������� ��� ��������������� ����������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'contractDate'
GO
exec sp_addextendedproperty N'MS_Description', '���� ������ �������� �������� ��� ��������������� ���������� - ������������� ��������� contractDate �� JSON', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'BeginDate'
GO
exec sp_addextendedproperty N'MS_Description', '���� ��������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'contractPrice'
GO
exec sp_addextendedproperty N'MS_Description', '��� ������, ������������ ��� �������� ���� ��������(933)', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'contractPriceCurrencyCode'
GO
exec sp_addextendedproperty N'MS_Description', '���� ���������� ��������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'executionTerm'
GO
exec sp_addextendedproperty N'MS_Description', '����������� ���� ���������� ��������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'realExecutionTerm'
GO
exec sp_addextendedproperty N'MS_Description', '���� ����������� ������������ �� ���������� ��������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'terminationExecutionTerm'
GO
exec sp_addextendedproperty N'MS_Description', '��������� ����������� ������������ �� ��������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'terminationReason'
GO
exec sp_addextendedproperty N'MS_Description', '������������ ����������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'titleProvider'
GO
exec sp_addextendedproperty N'MS_Description', '����� ���������� ����������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'addressProvider'
GO
exec sp_addextendedproperty N'MS_Description', '��� ����������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'unpProvider'
GO
exec sp_addextendedproperty N'MS_Description', '����������������� ��� ���������� (��� ������������ ���������� ��������)', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'sellerOtherId'
GO
exec sp_addextendedproperty N'MS_Description', '������ ����������� ����������("����� 2" ��� ������, �������� ���� 017-99)', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'countryProvider'
GO
exec sp_addextendedproperty N'MS_Description', '��������� ���� ����������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'accountProvider'
GO
exec sp_addextendedproperty N'MS_Description', '��� ����� ����������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'bankProviderCode'
GO
exec sp_addextendedproperty N'MS_Description', '������������ ����� ����������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'bankProviderName'
GO
exec sp_addextendedproperty N'MS_Description', '��������� ��������: ������� ������������, ������, �������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'StateCode'
GO
exec sp_addextendedproperty N'MS_Description', '������� ������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'ReasonOfRefusal'
GO
exec sp_addextendedproperty N'MS_Description', '���� � ����� ������������ ��������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'DateChangeState'
GO
exec sp_addextendedproperty N'MS_Description', '����� �������� � LT', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'LTSysNum'
GO
exec sp_addextendedproperty N'MS_Description', '���', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'UNK'
GO
exec sp_addextendedproperty N'MS_Description', '��� �������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'FinId'
GO
exec sp_addextendedproperty N'MS_Description', '���� ����� ����������� �������� ���� � LT', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'DateLoadFromGIAS'
GO
exec sp_addextendedproperty N'MS_Description', '����� ������������, �������� ��������� ���������', N'user', N'dbo', N'table', N'GIASLTContr', N'column', N'LastUser'


GO


