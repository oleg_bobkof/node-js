SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gias_xml_ReservDetail]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[gias_xml_ReservDetail]
GO
-- Version 2; Date 23.03.2020; LastEditor Oleg
CREATE function dbo.gias_xml_ReservDetail(
	@idXMl int, @data ntext = null)
returns @table table
(
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[budgetCost] [money] NOT NULL ,
	[finYear] [int] NOT NULL ,
	[dSct] [smallint] NOT NULL ,
	[dSSct] [smallint] NOT NULL ,
	[dKnd] [smallint] NOT NULL ,
	[dPrgr] [smallint] NOT NULL ,
	[chp] [smallint] NOT NULL ,
	[dPro] [smallint] NOT NULL ,
	[dSPro] [smallint] NOT NULL ,
	[dCtg] [smallint] NOT NULL ,
	[dArt] [smallint] NOT NULL ,
	[dSArt] [smallint] NOT NULL ,
	[dItm] [smallint] NOT NULL ,
	[finId] [int] NOT NULL ,
	[unk] [int] NOT NULL ,
	[customerId] [uniqueidentifier] NULL,
	[tkId] [int] NOT NULL ,
	[UNP] [varchar] (9) not null,
  	[uid] [varchar] (50) not null
)

as
begin

insert into @table ([budgetCost], [finYear], [dSct], [dSSct], [dKnd], [dPrgr], [chp], [dPro], [dSPro], [dCtg], [dArt], [dSArt], [dItm], [finId], [unk], [customerId], [tkId], [UNP], [uid]) 
select 
	[budgetCost], 
	[finYear], 
	[dSct], 
	[dSSct], 
	[dKnd], 
	[dPrgr], 
	[chp], 
	[dPro], 
	[dSPro], 
	[dCtg], 
	[dArt], 
	[dSArt], 
	[dItm], 
	[finId], 
	[unk], 
	case [customerId] when '' then null else [customerId] end, 
	[tkId], 
	[UNP], 
	[uid]
FROM 
  openxml (@idxml, '/reservation/financeSources')
    with (
	[budgetCost] money './budgetCost', 
	[finYear] int  './finYear',  
	[dSct] smallint './dSct', 
	[dSSct] smallint './dSSct', 
	[dKnd] smallint './dKnd', 
	[dPrgr] smallint './dPrgr', 
	[chp] smallint './chp', 
	[dPro] smallint './dPro', 
	[dSPro] smallint './dSPro', 
	[dCtg] smallint './dCtg', 
	[dArt] smallint './dArt', 
	[dSArt] smallint './dSArt', 
	[dItm] smallint './dItm',
	[finId] int './finId', 
	[unk] int './unk', 
	[customerId] varchar(50) './customerId', 
	[tkId] int './tkId', 
	[unp] varchar(9) './UNP',
	[uid] varchar(50) './_id'
    )

return

end















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[gias_xml_ReservDetail]  TO [public]
GO

