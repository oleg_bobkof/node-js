declare 
  @contract nvarchar(4000),
  @financeSource nvarchar(4000),
  @payments nvarchar(4000),
  @contractPositions nvarchar(4000),
  @contractDocuments nvarchar(4000)
  -- @contractXML nvarchar(4000)

select top 1 
  @contract = contract,
  @financeSource = financeSource,
  @payments = payments,
  @contractPositions = contractPositions,
  @contractDocuments = contractDocuments
--  @contractXML = contractXML
-- delete 
from giasContracts

select * from dbo.parseJSON(@contract)
select * from dbo.parseJSON(@financeSource)
select * from dbo.parseJSON(@payments)
select * from dbo.parseJSON(@contractPositions)
select * from dbo.parseJSON(@contractDocuments)
