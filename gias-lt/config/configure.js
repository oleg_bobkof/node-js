const path = require('path');
const yaml_config = require('node-yaml-config');
const logger = require('../utils/logger');

const userArgs = process.argv.slice(2);
const configFile = userArgs[0] || 'config.yaml';
const logFormat = '[:date[iso]] [info] => :remote-addr - :remote-user :method :url :status :response-time ms - :res[content-length]';
const uniqueArray = arr => [...new Set(arr.map(o => JSON.stringify(o)))].map(s => JSON.parse(s));

const config = {
  getFinId: (finId) => ((finId % 100 == 0) || (finId >= 90000)) ? finId : Math.floor(finId / 100),
  getDataSource: function (type) {
    let { databases = [] } = this;
    return databases.filter(db => db.database.type === type);
  },
  addRecipients: function (template) {
    let { _id, filters = [] } = template;
    let items = uniqueArray(filters.map(f => ({ id: _id, tkId: f.tkId, finId: this.getFinId(f.finId) })));
    Array.prototype.push.apply(this.recipients, items);
    return this.recipients;
  }
};

// Web server
config.webserver = {};
config.webserver.port = process.env.PORT || 4010;
config.webserver.debug = 'dev';

// Databases
config.databases = [];
config.gias_mf = {};
config.gias_scheduler = {};

// Recipients
config.recipients = [];

// Configuration
(function init() {
  try {
    let filename = path.join(__dirname, '../') + configFile;
    let appConfig = yaml_config.load(filename);
    let { webserver, gias_mf, gias_scheduler, databases = [] } = appConfig;
    logger.info('Read config: ', { message: filename });
    config.webserver = (webserver) ? webserver : config.webserver;
    config.webserver.debug = logFormat;
    config.gias_mf = (gias_mf) ? gias_mf : config.gias_mf;
    config.gias_scheduler = (gias_scheduler) ? gias_scheduler : config.gias_scheduler;
    config.databases = (databases) ? databases : config.databases;
  } catch (err) {
    console.log(err);
    process.exit(0);
  }
}());

module.exports = config;