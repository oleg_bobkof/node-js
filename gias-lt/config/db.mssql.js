const
  sql = require("mssql"),
  logger = require('../utils/logger'),
  { json2xml, utf2win } = require('../utils/common');


//
function messageHandler(dbconfig, cb) {

  const dbConn = new sql.ConnectionPool(dbconfig);
  dbConn.connect().then(function () {
    let request = new sql.Request(dbConn);
    request.query("select db_name() as OK").then(function (result) {
      cb(null, result.recordset[0]);
      dbConn.close();
    }).catch(function (err) {
      cb(err);
      dbConn.close();
    });
  }).catch(function (err) {
    cb(err);
  });

}

// 
function getTemplateDB(dbconfig, cb) {

  const dbConn = new sql.ConnectionPool(dbconfig);
  dbConn.connect().then(function () {
    let request = new sql.Request(dbConn);
    request.query("select tkId, finId from v_gias_Templates").then(function (result) {
      cb(null, result.recordset);
      dbConn.close();
    }).catch(function (err) {
      logger.error(err);
      cb(err);
      dbConn.close();
    });
  }).catch(function (err) {
    logger.error(err);
    cb(err);
  });

}

/*
  Contracts
*/

// Add Contract -> gias_AddContract
function addContractToDB(template, contract, contractAED, cb) {

  if (!template) { return cb(null, null); }
  const { tkId, connectionId, dbconfig = {} } = template;
  const { contractId, baseContractId, terminationExecutionTerm, titleContract = '' } = contract;
  date = (terminationExecutionTerm) ? new Date(terminationExecutionTerm) : null;

  const dbConn = new sql.ConnectionPool(dbconfig);
  dbConn.connect().then(function () {
    let request = new sql.Request(dbConn);
    logger.info(`[${tkId}] ${connectionId} - gias_AddContract: `, { message: contractId });
    let bufXML = null;
    let bufJSON = null;

    try {
      contract.titleContract = titleContract.slice(0, 299);
      contractAED.titleContract = titleContract.slice(0, 299);
      let contractXML = json2xml({ contract: contract });
      bufXML = utf2win(contractXML);
      bufJSON = utf2win(JSON.stringify(contract));
    } catch (err) {
      cb(err);
    }

    request.input('contractId', sql.UniqueIdentifier, contractId);
    // request.input('contract', sql.NText, JSON.stringify(contractAED));
    request.input('contract', sql.NText, '');
    request.input('baseContractId', sql.UniqueIdentifier, baseContractId);
    request.input('terminationExecutionTerm', sql.DateTime, date);
    request.input('financeSource', sql.NText, '');
    request.input('payments', sql.NText, '');
    request.input('contractPositions', sql.NText, '');
    request.input('contractDocuments', sql.NText, null);
    request.input('contractData', sql.Text, null);
    request.input('contractXML', sql.Image, bufXML);
    request.input('contractJSON', sql.Image, bufJSON)
      .execute('gias_AddContract').then(function (result) {
        let { returnValue = 0 } = result;
        logger.info(`[${tkId}] ${connectionId} - gias_AddContract: `, { message: returnValue.toString() });
        cb(null, result);
        dbConn.close();
      }).catch(function (err) {
        cb(err);
        logger.error(err);
        dbConn.close();
      });
  }).catch(function (err) {
    cb(err);
    logger.error(err);
  });

}

// Add Contract Documents
function addContractDocToDB(template, masterid, contractdoc, contractdata, cb) {

  if (!(template) || (masterid === 0)) { return cb(null, null); }
  const { tkId, connectionId, dbconfig = {} } = template;
  const { type = 0, link: { name = '' } } = contractdoc;

  const dbConn = new sql.ConnectionPool(dbconfig);
  dbConn.connect().then(function () {
    const request = new sql.Request(dbConn);
    logger.info(`[${tkId}] ${connectionId} - gias_AddContractDocuments: `, { message: masterid.toString() + ' ' + name });
    request.input('masterId', sql.Int, masterid);
    request.input('fileType', sql.SmallInt, type);
    request.input('contractDocuments', sql.NText, JSON.stringify(contractdoc));
    request.input('contractData', sql.Text, JSON.stringify(contractdata))
      .execute('gias_AddContractDocuments').then(function (result) {
        cb(null, result);
        dbConn.close();
      }).catch(function (err) {
        cb(err);
        logger.error(err);
        dbConn.close();
      });
  }).catch(function (err) {
    cb(err);
    logger.error(err);
  });

}

// Add End Contract -> gias_AddEnd @Id
function markContractToDB(template, masterid, cb) {

  if (!(template) || (masterid === 0)) { return cb(null, null); }
  const { tkId, connectionId, dbconfig = {} } = template;

  const dbConn = new sql.ConnectionPool(dbconfig);
  dbConn.connect().then(function () {
    const request = new sql.Request(dbConn);
    logger.info(`[${tkId}] ${connectionId} - gias_AddEnd: `, { message: masterid.toString() });
    request.input('Id', sql.Int, masterid)
      .execute('gias_AddEnd').then(function (result) {
        cb(null, result);
        dbConn.close();
      }).catch(function (err) {
        logger.error(err);
        cb(err);
        dbConn.close();
      });
  }).catch(function (err) {
    logger.error(err);
    cb(err);
  });

}

/*
  Reservation
*/

// Save Reservation -> gias_AddReservRequest(@requestId uniqueidentifier, @data text, @dateRequest datetime)
function saveReservationToDB(template, reservation, cb) {

  if (!template) { return cb(null, null); }
  const { tkId, connectionId, dbconfig = {} } = template;
  const { requestId } = reservation;
  const dt = new Date();
  dt.setHours(dt.getHours() + 3);

  const dbConn = new sql.ConnectionPool(dbconfig);
  dbConn.connect().then(function () {
    const request = new sql.Request(dbConn);
    const reservationTK = JSON.parse(JSON.stringify(reservation));
    const { financeSources = [] } = reservationTK;
    financeSources.forEach((fs) => fs.reason = null);
    const reservationXML = json2xml({ reservation: reservationTK });
    // let bufXML = utf2win(reservationXML);
    logger.info(`[${tkId}] ${connectionId} - gias_AddReservRequest: `, { message: requestId });
    request.input('requestId', sql.UniqueIdentifier, requestId);
    request.input('data', sql.Text, reservationXML);
    request.input('dateRequest', sql.DateTime, dt)
      .execute('gias_AddReservRequest').then(function (result) {
        cb(null, result.recordset);
        dbConn.close();
      }).catch(function (err) {
        cb(err);
        logger.error(err);
        dbConn.close();
      });
  }).catch(function (err) {
    cb(err);
    logger.error(err);
  });

}

// Cancel Reservation -> gias_CancelReservRequest(@requestId [uniqueidentifier])
function cancelReservationToDB(template, reservation, cb) {

  if (!template) { return cb(null, null); }
  const { tkId, connectionId, dbconfig = {} } = template;
  const { requestId } = reservation;

  const dbConn = new sql.ConnectionPool(dbconfig);
  dbConn.connect().then(function () {
    const request = new sql.Request(dbConn);
    logger.info(`[${tkId}] ${connectionId} - gias_CancelReservRequest: `, { message: requestId });
    request.input('requestId', sql.UniqueIdentifier, requestId)
      .execute('gias_CancelReservRequest').then(function (result) {
        cb(null, result);
        dbConn.close();
      }).catch(function (err) {
        cb(err);
        logger.error(err);
        dbConn.close();
      });
  }).catch(function (err) {
    cb(err);
    logger.error(err);
  });

}

/*
  Contract Execution
*/

// Get Contracts Execution List -> gias_GetExecutionList
function getContractExecList(template, startDate, endDate, cb) {

  if (!template) { return cb(null, null); }
  const { tkId, connectionId, dbconfig = {} } = template;

  const dbConn = new sql.ConnectionPool(dbconfig);
  dbConn.connect().then(function () {
    const request = new sql.Request(dbConn);
    request.input('startDate', sql.DateTime, startDate);
    request.input('endDate', sql.DateTime, endDate)
      .execute('gias_GetExecutionList').then(function (result) {
        cb(null, result.recordset);
        dbConn.close();
      }).catch(function (err) {
        cb(err);
        logger.error(err);
        dbConn.close();
      });
  }).catch(function (err) {
    cb(err);
    logger.error(err);
  });

}

//
function getContractExecFromDB(template, contrId, startDate, endDate, cb) {

  if (!template) { return cb(null, null); }
  const { tkId, connectionId, dbconfig = {} } = template;

  const dbConn = new sql.ConnectionPool(dbconfig);
  dbConn.connect().then(function () {
    const request = new sql.Request(dbConn);
    logger.info(`[${tkId}] ${connectionId} - gias_GetExecution: ${startDate.toISOString()}, ${endDate.toISOString()}, ${contrId}`);
    request.input('startDate', sql.DateTime, startDate);
    request.input('endDate', sql.DateTime, endDate);
    request.input('id', sql.Int, contrId)
      .execute('gias_GetExecution').then(function (result) {
        cb(null, result.recordset);
        dbConn.close();
      }).catch(function (err) {
        cb(err);
        logger.error(err);
        dbConn.close();
      });
  }).catch(function (err) {
    cb(err);
    logger.error(err);
  });

}

//////////////////////////////////////////
module.exports = {
  sql,
  getTemplateDB, messageHandler,
  addContractToDB, addContractDocToDB, markContractToDB,
  saveReservationToDB, cancelReservationToDB,
  getContractExecList, getContractExecFromDB
};
