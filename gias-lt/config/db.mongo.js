const mongoose = require('mongoose');
const logger = require('../utils/logger');
const config = require('../config/configure');
const mongodb = mongoose.mongo;
const db = mongoose.connection;

// Format Uri
const getUri = (db) => `mongodb://${db.host}:${db.port}/${db.name}`;

// Set up mongoose connection
function initDB(callback) {

  const { database: { dbconfig: { uri, options } } } = config.getDataSource('mongo')[0];
  mongoose.connect(uri, options).then(
    () => {
      mongoose.Promise = global.Promise;
      logger.info('MongoDB is connected: ', { message: getUri(db) });
      callback(null, db);
    },
    err => {
      logger.error('App starting error: ', { message: err });
      callback(err);
      process.exit(1);
    }
  );

}

//
function closeDB(callback) {
  db.close().then(
    () => {
      logger.warn('MongoDB connection closed.');
      callback();
    },
    err => {
      callback(err);
    }
  );
}

module.exports = { initDB, closeDB, db, mongodb };
