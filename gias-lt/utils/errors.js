const createError = exports.createHttpErr = require('http-errors');
const logger = require('../utils/logger');

const ErrorData = function (err) {
  let error_code = err.statusCode;

  if (err.headers) {
    error_code = err.headers.error_code;
  }

  this.errorCode = error_code;
  this.AdditionalData = {};
  this.AdditionalData.timestamp = Math.floor(new Date() / 1000);
  this.AdditionalData.status = err.statusCode;
  this.AdditionalData.error = err.name;
  this.AdditionalData.message = err.message;
};

// Error 500
const createErr500 = exports.createErr500 = function (error) {
  let err = createError(500, error);
  err.headers = { error_code: 500 };
  return err;
};

// Error 404
exports.createErr404 = function (error) {
  let err = createError(400, error);
  err.headers = { error_code: 400 };
  return err;
};

// Error 400
exports.createErr400 = function (code, error) {
  let err = createError(400, error);
  err.headers = { error_code: code };
  return err;
};

// Error handler
exports.errorHandler = function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error data
  if (!err.status) {
    err = createErr500(err);
  }

  let error = new ErrorData(err);
  logger.error(JSON.stringify(error));
  res.status(err.status).json(error);
};